"""Test the forward chain of the SWB"""

import logging
import unittest
from pathlib import Path

import xarray as xr

import stereoid.oceans.run_scenario as run_scenario
import stereoid.utils.config as st_config
import stereoid.utils.tools as tools


class TestFoward(unittest.TestCase):
    def setUp(self):
        self.root = Path(__file__).parent.parent
        self.config = st_config.parse(self.root / "PAR" / "user_defaults.cfg")

    def open_expected_and_produced(self, expected_fname, produced_fname):
        expected_nrcs_path = (
            self.root / "tests/Test Data/Expected Results" / expected_fname
        )
        expected_ds = xr.open_dataset(expected_nrcs_path)
        produced_nrcs_path = (
            self.root
            / "tests/Test Results/OceanE2E/Scenarios/California"
            / produced_fname
        )
        produced_ds = xr.open_dataset(produced_nrcs_path)
        return expected_ds, produced_ds

    def test_forward_l1(self):
        swb_param_path = self.config["par"] / "params_SWB_California_test.py"
        p = tools.load_python_file(swb_param_path)
        main_logger = logging.getLogger()
        main_logger.handlers = []
        handler = logging.StreamHandler()
        handler.setLevel(logging.DEBUG)
        main_logger.addHandler(handler)
        main_logger.setLevel(logging.INFO)
        run_scenario.run_stereoid_fwd(p)
        expected_nrcs_ds, produced_nrcs_ds = self.open_expected_and_produced(
            "nrcs_L1_All_obs_nonoise_California_R13_S01_350_05.nc",
            "nrcs_L1_All_obs_nonoise_California_R13_S01_350_05.nc",
        )
        xr.testing.assert_allclose(expected_nrcs_ds, produced_nrcs_ds)
        expected_dop_ds, produced_dop_ds = self.open_expected_and_produced(
            "dop_L1_All_obs_nonoise_California_R13_S01_350_05.nc",
            "dop_L1_All_obs_nonoise_California_R13_S01_350_05.nc",
        )
        xr.testing.assert_allclose(expected_dop_ds, produced_dop_ds)
        expected_imacs_ds, produced_imacs_ds = self.open_expected_and_produced(
            "imacs_L1_All_obs_California_R13_S01_350_05.nc",
            "imacs_L1_All_obs_California_R13_S01_350_05.nc",
        )
        xr.testing.assert_allclose(expected_imacs_ds, produced_imacs_ds)
        expected_cut_off_ds, produced_cut_off_ds = self.open_expected_and_produced(
            "cut_off_L1_All_obs_California_R13_S01_350_05.nc",
            "cut_off_L1_All_obs_California_R13_S01_350_05.nc",
        )
        xr.testing.assert_allclose(expected_cut_off_ds, produced_cut_off_ds)
