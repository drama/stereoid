from pathlib import Path

import stereoid.utils.config as st_config


# Set this to the directory of your data and model results
paths = st_config.parse(Path(__file__).parent.parent / "PAR" / "user_defaults.cfg")
# Main project path
path = paths["main"]
# Path to PAR file
pardir = paths["par"]
resdir = path / "tests" / "Test Results"
scns_dir = path / "tests" / "Test Data" / "Ocean/Scenarios"

progress_bar = True
noise = False

# -- Satellite parameters
# distance between satellites
dau = 350e3  # should be consistent with the PAR file
incident_angle = 31
# Mode for SAR: either WM or IWS
mode = "IWS"
run_id = f"ocean_{mode}"
parfile = pardir / f"Harmony_{run_id}.cfg"
# some additional information
rx_ipc_name = "tud_2020_tripple_ati"
rx_cpc_name = "tud_2020_tripple"
az_res_dct = {"WM": 5, "IWS": 20}
az_res = az_res_dct[mode]
b_ati = 9
rot_angle = -14

# polarizations
txpol = "V"
rxpolbase = "mM"

# Add or not the current Doppler_
add_current_Doppler = True

# -- Dynamical model parameters
# paths for ocean model
model_run = "California"
scn_dir = scns_dir / model_run
scn_file = scn_dir / "ocean_lionel.mat"
sizeaz = [300e3, 320e3]
sizer = [240e3, 250e3]

# -- Wave model parameters
# paths to SWAN wave spectra
run = "R13"
swan_dir = scn_dir / "SWAN"
swan_file = swan_dir / f"specCal_{run}.nc"
read_from_netcdf = True
swan_as_nc = True
# Minimum Wave length
lambda_min = 0.005
# Maximum Wave length
lambda_max = 500
# Number of frequencies single side (total 2*n_k -1)
n_k = 128
# Fetch for short waves
fetch = 100e3

# -- SAR spectrum parameters
# Maximum wavelength in SAR spectra (inherent scene size)
SAR_spectra_lambda_max = 2000
# Spatial sampling of spectra
spec_samp = [5, 5]  # in case of 1000 m resolution, 10 = each 10 km one spectrum
# Number of looks (should be coupled to SAR_spectra_lambda_max and spec_samp)
SAR_spectra_looks = 25
spec_type_doppler_backscatter = "SWAN_noneq"
spec_type_imacs_cutoff = "SWAN_noneq"
# Separating wave number for merging the long and short wave spectra.
k_l = None
# Setting to True adds random noise to the spectra
SAR_spectra_noise = False

# -- Output parameters
# output paths
sc_run = "S01"
main_out = resdir / "OceanE2E" / "Scenarios" / model_run
main_out.mkdir(parents=True, exist_ok=True)
pattern = f"{model_run}_{run}_{sc_run}_{int(dau/1000):03d}"
sigma_file = main_out / f"Backscatter_{pattern}.nc"
Doppler_file = main_out / f"Doppler_{pattern}.nc"
spectra_file = main_out / f"Spectra_{pattern}_{spec_samp[0]:02d}.nc"
obs_nonoise_file = main_out / f"All_obs_nonoise_{pattern}_{spec_samp[0]:02d}"
obs_file = main_out / f"All_obs_{pattern}_{spec_samp[0]:02d}"
