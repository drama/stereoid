import os
# -- Set this to the directory of your data and model results
import stereoid.utils.config as st_config
import pathlib
paths = st_config.parse(pathlib.Path('/home/lgaultier/src/stereoid/PAR/user_defaults.cfg'),
                        section="Paths")
# Path to PAR file
pardir = paths["par"]
path = paths["par"]
data = paths["data"]

lut_directory = os.path.join(data, 'lut')
lut_pattern = 'lut_20220616_companion400km_all.nc'
parfile = '/home/lgaultier/src/stereoid/PAR/Hrmny_2021_1.cfg'
incident = 31
dau = 400E3

data_directory = os.path.join(data, 'SWAN')
_pattern = 'All_obs_California_T01_S01_add_doppler_SWAN_adjusted_RIM_10k_p_400_x_150_390_y_150_600_rot_-14_'
data_pattern = f'L1_{_pattern}.nc'
data_pattern = {'nrcs': f'nrcs_L1_{_pat}_10.nc',
                'dop': f'dop_L1_{_pat}_10.nc',
                'imacs': f'imacs_L1_{_pat}_10.nc',
                'cut_off': f'cut_off_L1_{_pat}_11.nc',
                }
dx = 1E3
subsample = [10, 10]
sigma_imacs = [8E-6, 8E-6] #0.04E-3
sigma_dir0 = [10, 10]
sigma_norm0 = [2.8, 0.8]
pyramid_factor = [10, 1]
threshold_cost = 40
threshold_nrcs = 1
half_filter_nrcs = 5
weight_imacs = [[0, 1., 0.], [1., 1., 1.]]
weight_nrcs = [[0, 1., 0.], [1., 1., 1.]]
fetch = 500e3

model_file = os.path.join(data_directory, f'{_pattern}.pyo')

out_directory = paths["results"]
out_file = f'L2_{_pattern}'
