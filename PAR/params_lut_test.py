import os
from pathlib import Path

import stereoid.utils.config as st_config
# Set this to the directory of your data and model results
user_cfg_path = Path("~/").expanduser() / "user.cfg"
paths = st_config.parse(user_cfg_path, section="Paths")
path = paths["main"]
pardir = paths["par"]
datadir= paths["data"]
resdir = paths["results"]
scns_dir =  os.path.join(datadir, "Ocean/Scenarios")
progress_bar = True
nprocessor = 4
noise = False

# -- Satellite parameters
# distance between satellites
dau = 450E3 # should be consistent with the PAR file
incident_angle = 31
# Mode for SAR: either WM or IWS
mode = "IWS"
run_id = f'ocean_{mode}_testlut'
parfile = os.path.join(pardir, f'Hrmny_2021_1.cfg')
# some additional information
rx_ipc_name = "tud_2020_tripple_ati"
rx_cpc_name = "tud_2020_tripple"
az_res_dct = {"WM": 5, "IWS": 20}
az_res = az_res_dct[mode]
b_ati = 9

# polarizations
txpol = 'V'
rxpolbase = 'mM'

# -- LUT Parameters
# Wind Range for LUT computation: [min, max, step] in m/s
lut_wind_range = [9, 12, 2]
# Wave age range: [min max step] 
lut_iwa_range = [0.5, 2, 0.2]
# Incidence range (size of the observed scene): [range_max, nb of points] in km
lut_incidence_range = [200e3, 5]
# list_variable = ["Doppler", "Backscatter", "imacs", "cut_off"]
list_variable = ["imacs", "cut_off"]

# -- Dynamical model parameters
# paths for ocean model
model_run = 'California'
model_reader = "California"
scn_dir =  os.path.join(scns_dir, model_run)
scn_file = os.path.join(scn_dir, 'ocean_lionel.mat')
sizeaz = [200E3, 210E3]
sizer = [300E3, 310E3]
l1_resolution = 1E3

# -- Wave model parameters
# paths to SWAN wave spectra
run='R13'
#swan_dir = os.path.join(path, 'RESULTS', 'SWAN')
swan_dir = os.path.join(scn_dir, 'SWAN')
swan_file = os.path.join(swan_dir, f'specCal_{run}.xarray')
read_from_netcdf = True
swan_as_nc = False
# Range Wave length: [min, max, number of points] in km
lambda_range = [0.01, 1000, 200]
# Minimum Wave length
lambda_min = lambda_range[0]
# Maximum Wave length
lambda_max = lambda_range[1]
# Number of frequencies single side (total 2*n_k -1)
n_k = lambda_range[2]
# Fetch for short waves
fetch = 100E3

# SAR Spectrum parameters
# Spatial sampling of spectra
spec_samp = [1, 5]
# -- Output parameters
# output paths
sc_run='lut_450kmcompanion_20220503T16'
main_out = os.path.join(resdir, 'OceanE2e', 'Scenarios', model_run)
os.makedirs(main_out, exist_ok=True)
pattern =  sc_run #f'{model_run}_{run}_{sc_run}_{int(dau/1000):03d}'
sigma_file = os.path.join(main_out, f'Backscatter_{pattern}.nc')
Doppler_file = os.path.join(main_out, f'Doppler_{pattern}.nc')
spectra_file = os.path.join(main_out, f'Spectra_{pattern}_{spec_samp[0]:02d}.nc')
obs_file = os.path.join(main_out, f'{pattern}_')
obs_nonoise_file = os.path.join(main_out, f'{pattern}_')
