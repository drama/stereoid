import os
from pathlib import Path

import stereoid.utils.config as st_config
# Set this to the directory of your data and model results
user_cfg_path = Path("~/").expanduser() / "user.cfg"
paths = st_config.parse(user_cfg_path, section="Paths")
# Main project path
path = paths["main"]
# Path to PAR file
pardir = paths["par"]
datadir= paths["data"]
resdir = paths["results"]
scns_dir =  os.path.join(datadir, "OceanModels")

progress_bar = True
noise = True

# -- Satellite parameters
# distance between satellites
dau = 300E3 # should be consistent with the PAR file
incident_angle = 31
# Mode for SAR: either WM or IWS
mode = "IWS"
run_id = f'ocean_{mode}'
parfile = os.path.join(pardir, f'Harmony_{run_id}.cfg')
# some additional information
rx_ipc_name = 'tud_2020_tripple_ati'
rx_cpc_name = 'tud_2020_tripple'
az_res_dct = {"WM": 5, "IWS": 20}
az_res = az_res_dct[mode]
b_ati = 10
rot_angle = 11

# polarizations
txpol = 'V'
rxpolbase = 'mM'

# Add or not the current Doppler_
add_current_Doppler = True

# -- Dynamical model parameters
# paths for ocean model
model_run = 'DALES_EUREC4A'
model_reader = "DALES"
scn_dir =  os.path.join(scns_dir, model_run)
scn_file = os.path.join(scn_dir, 'Dales_36_hrs_12_01_00_surface_winds.nc')
sizeaz = [25.7E3, 125.0E3]
sizer = [44.93E3, 143.8E3]
l1_resolution = 500

# -- Wave model parameters
# paths to SWAN wave spectra
run='R01'
swan_dir = os.path.join(scns_dir, 'DALES_EUREC4A', 'SWAN')
swan_file = os.path.join(swan_dir, f'specDA1_{run}.nc')
read_from_netcdf = False
swan_in_nc = True
# Minimum Wave length
lambda_min = 0.005
# Maximum Wave length
lambda_max = 500
# Number of frequencies single side (total 2*n_k -1)
n_k = 128
# Fetch for short waves
fetch = 100E3

# -- SAR spectrum parameters
# Maximum wavelength in SAR spectra (inherent scene size)
SAR_spectra_lambda_max = 2000
# Spatial sampling of spectra
spec_samp=[1, 1] # in case of 1000 m resolution, 10 = each 10 km one spectrum
# Number of looks (should be coupled to SAR_spectra_lambda_max and spec_samp)
SAR_spectra_looks = 25
spec_type_doppler_backscatter = "SWAN_noneq"
spec_type_imacs_cutoff = "SWAN_noneq"
# Separating wave number for merging the long and short wave spectra.
k_l = None

# -- Output parameters
# output paths
sc_run='S01'
main_out = os.path.join(resdir, 'OceanE2E', 'Scenarios', model_run)
os.makedirs(main_out, exist_ok=True)
pattern = f'{model_run}_{run}_{sc_run}_{int(dau/1000):03d}'
sigma_file = os.path.join(main_out, f'Backscatter_{pattern}.nc')
Doppler_file = os.path.join(main_out, f'Doppler_{pattern}.nc')
spectra_file = os.path.join(main_out, f'Spectra_{pattern}_{spec_samp[0]:02d}.nc')
obs_nonoise_file = os.path.join(main_out, f'All_obs_nonoise_{pattern}_{spec_samp[0]:02d}')
obs_file = os.path.join(main_out, f'All_obs_{pattern}_{spec_samp[0]:02d}')
