import os

# -- Set this to the directory of your data and model results
import stereoid.utils.config as st_config
import pathlib
paths = st_config.parse(pathlib.Path('/home/lgaultier/src/stereoid/PAR/user_defaults.cfg'),
                        section="Paths")
# Path to PAR file
pardir = paths["par"]
path = paths["par"]
run_id = 'R13'

# Set up computing parameters
progress_bar = True
noise = False
nprocessor = 5

# -- Satellite parameters
# distance between satellites
dau = 400E3 # should be consistent with the PAR file
incident_angle = 31
# Mode for SAR: either WM or IWS
mode = "IWS"
parfile = os.path.join(pardir, f'Hrmny_2021_1.cfg')
# some additional information
rx_ipc_name = "tud_2020_tripple_ati"
rx_cpc_name = "tud_2020_tripple"
az_res_dct = {"WM": 5, "IWS": 20}
az_res = az_res_dct[mode]
b_ati = 10

# polarizations
txpol = 'V'
rxpolbase = 'mM'

# -- LUT Parameters
# Wind Range for LUT computation: [min, max, step] in m/s
# Default [5, 22, 2]
lut_wind_range = [6, 16, 1]
# Wave age range: [min max step] , default [0.5, 2, 0.05]
lut_iwa_range = [0.84, 1.44, 0.1]
# Incidence range (size of the observed scene): [range_max, nb of points] in k
# Default [250e3, 10], range should not inceed 250 km
lut_incidence_range = [240e3, 20]
list_variable = ["Doppler", "Backscatter", "imacs", "cut_off"]


# -- Wave model parameters
# Spatial sampling of spectra
spec_samp=[1, 1] # in case of 1000 m resolution, 10 = each 10 km one spectrum
# Minimum Wave length
lambda_min = 0.01
# Maximum Wave length
lambda_max = 1000
# Number of frequencies single side (total 2*n_k -1)
n_k = 200

# -- Output parameters
# output paths
date = '20220531'
sc_run = f'lut_{date}_companion{int(dau/1000):03d}km'
main_out = os.path.join(paths["results"], 'lut', date)
os.makedirs(main_out, exist_ok=True)
pattern =  sc_run
obs_file = os.path.join(main_out, f'{pattern}_')
