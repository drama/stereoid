import os

# Set this to the directory of your data and model results
path = '/mnt/data/Harmony'
progress_bar = True
noise = False

# -- Satellite parameters
# distance between satellites
dau = 5E3 # should be consistent with the PAR file
incident_angle = 31
# Mode for SAR: either WM or IWS
mode = "IWS"
run_id = f'ocean_{mode}_testlut'
pardir = os.path.join('/home/lgaultier/src/stereoid/', 'PAR')
parfile = os.path.join(pardir, f'Harmony_{run_id}.cfg')
# some additional information
rx_ipc_name = "tud_2020_9m_half"
rx_cpc_name = "tud_2020_dual9m"
az_res_dct = {"WM": 5, "IWS": 20}
az_res = az_res_dct[mode]
b_ati = 9

# polarizations
txpol = 'V'
rxpolbase = 'mM'

# Add or not the current Doppler_
add_current_Doppler = True

# -- Dynamical model parameters
# paths for ocean model
model_run = 'California'
model_reader = "California"
rot_angle = -14
scn_dir =  os.path.join(path, model_run)
scn_file = os.path.join(scn_dir, 'ocean_lionel.mat')
sizeaz = [200E3, 210E3]
sizer = [300E3, 310E3]
l1_resolution = 1E3

# -- Wave model parameters
# paths to SWAN wave spectra
run='R13'
#swan_dir = os.path.join(path, 'RESULTS', 'SWAN')
swan_dir = '/home/lgaultier/src/stereoid/assets/test'
swan_file = os.path.join(swan_dir, f'specCal_{run}.xarray')
read_from_netcdf = True
swan_as_nc = False
# Minimum Wave length
lambda_min = 0.005
# Maximum Wave length
lambda_max = 500
# Number of frequencies single side (total 2*n_k -1)
n_k = 128
# Fetch for short waves
fetch = 100E3

# -- SAR spectrum parameters
# Maximum wavelength in SAR spectra (inherent scene size)
SAR_spectra_lambda_max = 2000
# Spatial sampling of spectra
spec_samp=10 # in case of 1000 m resolution, 10 = each 10 km one spectrum
# Number of looks (should be coupled to SAR_spectra_lambda_max and spec_samp)
SAR_spectra_looks = 25
spec_type_doppler_backscatter = "SWAN_noneq"
spec_type_imacs_cutoff = "SWAN_noneq"
# Separating wave number for merging the long and short wave spectra.
k_l = None

# -- Output parameters
# output paths
sc_run='S01'
resdir = '/tmp'
main_out = os.path.join(path, 'RESULTS', 'Scenarios', model_run)
os.makedirs(main_out, exist_ok=True)
pattern = f'{model_run}_{run}_{sc_run}_{int(dau/1000):03d}'
sigma_file = os.path.join(main_out, f'Backscatter_{pattern}.nc')
Doppler_file = os.path.join(main_out, f'Doppler_{pattern}.nc')
spectra_file = os.path.join(main_out, f'Spectra_{pattern}_{spec_samp:02d}.nc')
obs_file = os.path.join(main_out, f'All_obs_{pattern}_{spec_samp:02d}')
obs_nonoise_file = os.path.join(main_out, f'All_obs_nonoise_{pattern}_{spec_samp:02d}')
