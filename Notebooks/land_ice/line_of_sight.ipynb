{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.colors as colors\n",
    "from mpl_toolkits.axes_grid1 import make_axes_locatable\n",
    "import cartopy.crs as ccrs\n",
    "import cartopy.feature as cfeature\n",
    "\n",
    "from drama.performance.sar import SARModeFromCfg\n",
    "from drama.io import cfg\n",
    "from drama.mission.timeline import LatLonTimeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Set up radar parameters\n",
    "Change the second parameter of `SARModeFromCfg` to the acquisition mode of interest: `\"EW\"`, `\"WM\"`, `\"stripmap\"`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "par_file = Path(\"~/Code/stereoid/PAR/XTI.cfg\").expanduser()\n",
    "mode = SARModeFromCfg(cfg.ConfigFile(par_file), \"IWS\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define the latitude and longitude range that we are interested in. If we are interested in a single swath, we can set the longitudinal range to match that of the satellite. For example Sentinel-1 has repeat cycle of 175 orbits.\n",
    "\n",
    "The coordinates were chosen to match Antarctica. The sampling rate of the interpolator, `dlat` and `dlon` should be adjusted to allow for the spacing of the desired lat and lon coordinates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_orbits_cycle = 175\n",
    "lon_repeat_cycle = 360 / n_orbits_cycle\n",
    "min_lon = -180\n",
    "max_lon = 180\n",
    "min_lat = -90\n",
    "max_lat = -60\n",
    "\n",
    "lat_grid, lon_grid = np.mgrid[min_lat:max_lat:0.25, min_lon:max_lon:0.1]\n",
    "latitudes = lat_grid[:, 0]\n",
    "longitudes = lon_grid[0, :]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ask drama to compute the acquisition geometry for our range of lat and lon."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "orbit_resolution = 0.1\n",
    "timeline = LatLonTimeline(\n",
    "    par_file, np.ravel(lat_grid), np.ravel(lon_grid), inc_angle_range=(mode.incs[0, 0], mode.incs[-1, 1]), dlat=orbit_resolution, dlon=orbit_resolution\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Acquisitions\n",
    "The instance of LatLonTimeline that we created holds several variables that are of interest."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dir(timeline)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "asc_acqs and desc_acqs hold information about acquisitions made during ascending and descending parts of the orbits respectively.\n",
    "\n",
    "They each are a numpy array of PointTimeline objects. Each PointTimeline object holds information that corresponds to a combination of longitude and latitude in the order that they were passed to the initialiser of LatLonTimeline.\n",
    "\n",
    "* theta_i is an array of the incidence angles with which the point is observed by the satellite\n",
    "* northing is an array of the azimuth angles, defined in the east of north direction (0° is North, 90° is East), with which the point is observed by the satellite\n",
    "* orbtime is an array of the time instances [s] during the orbit that the point is observed.\n",
    "* orbnum is an array of the orbit numbers at which the point is observed\n",
    "* slant_range is an array of the slant ranges\n",
    "* theta_l is an array of the look angles"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(timeline.asc_acqs[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To find how many times the satellite sees a specific point on the ground we can count the number of incidence angles that correspond to each point. We do this for ascending acquisitions and descending acquisitions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "no_asc_aq = np.array([len(acq.theta_i) for acq in timeline.asc_acqs]).reshape(lon_grid.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "no_dsc_aq = np.array([len(acq.theta_i) for acq in timeline.desc_acqs]).reshape(lon_grid.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Projection of the line of sight in W-E, S-N and Down-Up directions\n",
    "For each acquisition we have an incident angle, $\\theta_i$ and an azimuth angle defined as East of North, $\\phi_i$. Thus for each spacecraft the unit vector in the direction of the line of sight is given by\n",
    "$$\\hat{l}_s = \\left(\\begin{array}{c}\n",
    "\\sin\\theta_{i,s} \\sin\\phi_{i,s}\\\\\n",
    "\\sin\\theta_{i,s} \\cos\\phi_{i,s} \\\\\n",
    "\\cos \\theta_{i,s}\n",
    "\\end{array}\\right)$$\n",
    "where $s$ labels the spacraft. In case of a main satellite and two companions $n\\in\\{m,c_1,c_2\\}$.\n",
    "\n",
    "For now let's focus on the main satellite.\n",
    "\n",
    "Retrieve the incidence and azimuth angles. \n",
    "\n",
    "Unpack the incidence angle for all the acquisitions. For each combination of lat and lon there might be more than acquisition (different satellite tracks). Thus multiple incidence angles. We select the mean."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "incs = np.empty(lon_grid.ravel().shape)\n",
    "for i, acq in enumerate(timeline.asc_acqs):\n",
    "    inc = acq.theta_i\n",
    "    if inc.size == 0:\n",
    "        incs[i]= np.NaN\n",
    "    else:\n",
    "        incs[i] = np.mean(inc)\n",
    "incs = incs.reshape(lon_grid.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "northings = np.empty(lon_grid.ravel().shape)\n",
    "for i, acq in enumerate(timeline.asc_acqs):\n",
    "    northing = acq.northing\n",
    "    if northing.size == 0:\n",
    "        northings[i]= np.NaN\n",
    "    else:\n",
    "        northings[i] = np.mean(northing)\n",
    "northings = northings.reshape(lon_grid.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compute the line-of-sight vector."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "los = np.empty(incs.shape + (3,))\n",
    "los[:, :, 0] = np.sin(incs) * np.sin(northings)\n",
    "los[:, :, 1] = np.sin(incs) * np.cos(northings)\n",
    "los[:, :, 2] = np.cos(incs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ocean_110m = cfeature.NaturalEarthFeature('physical', 'ocean', '110m',\n",
    "                                        edgecolor='face',\n",
    "                                        facecolor=cfeature.COLORS['water'])\n",
    "land_110m = cfeature.NaturalEarthFeature('physical', 'land', '110m',\n",
    "                                        edgecolor='face',\n",
    "                                        facecolor=cfeature.COLORS['land'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10, 10))\n",
    "ax1 = fig.add_subplot(1, 1, 1, projection=ccrs.SouthPolarStereo())\n",
    "ax1.coastlines(resolution='110m', zorder=10, alpha=0.5)\n",
    "ax1.add_feature(ocean_110m, zorder=0)\n",
    "ax1.add_feature(land_110m, zorder=0)\n",
    "ax1.set_extent([min_lon, max_lon, min_lat, max_lat], ccrs.PlateCarree())\n",
    "ax1.quiver(longitudes, latitudes, los[..., 0], los[..., 1], transform=ccrs.PlateCarree(), regrid_shape=35, zorder=6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
