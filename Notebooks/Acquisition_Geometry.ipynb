{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Acquisition geometry"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this Notebook we demonstrate the use of Drama to compute the acquistion geometry of a set of points on the surface of the Earth"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.colors as colors\n",
    "from mpl_toolkits.axes_grid1 import make_axes_locatable\n",
    "import cartopy.crs as ccrs\n",
    "import cartopy.feature as cfeature\n",
    "\n",
    "import stereoid.utils.config as st_config\n",
    "from drama.performance.sar import SARModeFromCfg\n",
    "from drama.io import cfg\n",
    "from drama.mission.timeline import LatLonTimeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Set up radar parameters\n",
    "Change the second parameter of `SARModeFromCfg` to the acquisition mode of interest: `\"EW\"`, `\"WM\"`, `\"stripmap\"`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "config = st_config.parse()\n",
    "run_id = \"2020_1\"\n",
    "par_file = config[\"par\"] / f\"Hrmny_{run_id}.cfg\"\n",
    "mode = SARModeFromCfg(cfg.ConfigFile(par_file), \"IWS\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define the latitude and longitude range that we are interested in. We select the longitudinal range to match that of the repeat cycle of the Sentinel-1. S-1 has a repeat cycle of 175 orbits."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_orbits_cycle = 175\n",
    "lon_repeat_cycle = 360 / n_orbits_cycle\n",
    "min_lon = -180\n",
    "min_lat = -90\n",
    "max_lat = 85\n",
    "n_orbits = 1\n",
    "\n",
    "lat_grid, lon_grid = np.mgrid[min_lat:max_lat:0.1, min_lon:min_lon+lon_repeat_cycle*n_orbits:0.5]\n",
    "latitudes = lat_grid[:, 0]\n",
    "longitudes = lon_grid[0, :]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ask drama to compute the acquisition geometry for our range of lat and lon. By default drama does this for one complete repeat cycle (175 orbits in the case of Sentinel-1)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "orbit_resolution = 1\n",
    "timeline = LatLonTimeline(\n",
    "    par_file, np.ravel(lat_grid), np.ravel(lon_grid), inc_angle_range=(mode.incs[0, 0], mode.incs[-1, 1]), dlat=orbit_resolution, dlon=orbit_resolution\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Acquisitions\n",
    "The instance of LatLonTimeline that we created holds several variables that are of interest."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dir(timeline)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "asc_acqs and desc_acqs hold information about acquisitions made during ascending and descending parts of the orbits respectively.\n",
    "\n",
    "They each are a numpy array of PointTimeline objects. Each PointTimeline object holds information that corresponds to a combination of longitude and latitude in the order that they were passed to the initialiser of LatLonTimeline.\n",
    "\n",
    "* theta_i is an array of the incidence angles with which the point is observed by the satellite\n",
    "* northing is an array of the azimuth angles, defined in the east of north direction (0° is North, 90° is East), with which the point is observed by the satellite\n",
    "* orbtime is an array of the time instances [s] during the orbit that the point is observed.\n",
    "* orbnum is an array of the orbit numbers at which the point is observed\n",
    "* slant_range is an array of the slant ranges\n",
    "* theta_l is an array of the look angles"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(timeline.asc_acqs[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To find how many times the satellite sees a specific point on the ground we can count the number of incidence angles that correspond to each point. We do this for ascending acquisitions and descending acquisitions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "no_asc_aq = np.array([len(acq.theta_i) for acq in timeline.asc_acqs]).reshape(lon_grid.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "no_dsc_aq = np.array([len(acq.theta_i) for acq in timeline.desc_acqs]).reshape(lon_grid.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sum the ascending and descending acquisitions to find the total number"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_cover = np.tile(no_asc_aq + no_dsc_aq, n_orbits_cycle // n_orbits)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To save time, we repeat the longitudes around the globe enough times to make one complete repeat cycle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_lat_grid = np.tile(lat_grid, n_orbits_cycle // n_orbits)\n",
    "full_lons = (np.arange(0, n_orbits_cycle // n_orbits) * lon_repeat_cycle * n_orbits)[:, np.newaxis] + longitudes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_lon_grid = np.empty_like(full_lat_grid)\n",
    "full_lon_grid[..., :] = full_lons.ravel()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_lon_grid[0,:]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ocean_110m = cfeature.NaturalEarthFeature('physical', 'ocean', '110m',\n",
    "                                        edgecolor='face',\n",
    "                                        facecolor=cfeature.COLORS['water'])\n",
    "land_110m = cfeature.NaturalEarthFeature('physical', 'land', '110m',\n",
    "                                        edgecolor='face',\n",
    "                                        facecolor=cfeature.COLORS['land'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#full_cover = full_cover[..., -1]\n",
    "#full_lat_grid = full_lat_grid[..., :-1]\n",
    "#full_lon_grid = full_lon_grid[..., :-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_lat_grid"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Global Number of Acquisitions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12, 12))\n",
    "ax = fig.add_subplot(1, 1, 1, projection=ccrs.Mollweide())\n",
    "ax.coastlines(resolution='110m', zorder=10, alpha=0.5)\n",
    "ax.add_feature(ocean_110m, zorder=5)\n",
    "ax.add_feature(land_110m, zorder=1)\n",
    "\n",
    "norm = colors.LogNorm(vmin=2, vmax=np.max(full_cover))\n",
    "data_cmap = plt.get_cmap('jet')\n",
    "filled_c = ax.pcolormesh(full_lon_grid[:,:-1], full_lat_grid[:,:-1], full_cover[:,:-1], \n",
    "                         transform=ccrs.PlateCarree(), cmap=data_cmap, norm=norm, zorder=6)\n",
    "colorbar = fig.colorbar(filled_c, orientation='horizontal', fraction=0.046, pad=0.04)\n",
    "colorbar.set_ticks([1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,25,30,35,40, 53])\n",
    "colorbar.set_ticklabels([1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,25,30,35,40, 53])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Incidence Angle\n",
    "\n",
    "Unpack the incidence angle for all the acquisitions. For each combination of lat and lon there might be more than acquisition (different satellite tracks). Thus multiple incidence angles. We select the mean."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "incs = np.empty(lon_grid.ravel().shape)\n",
    "for i, acq in enumerate(timeline.asc_acqs):\n",
    "    inc = acq.theta_i\n",
    "    if inc.size == 0:\n",
    "        incs[i]= np.NaN\n",
    "    else:\n",
    "        incs[i] = np.rad2deg(np.mean(inc))\n",
    "incs = incs.reshape(lon_grid.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Repeat for the azimuths."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "northings = np.empty(lon_grid.ravel().shape)\n",
    "for i, acq in enumerate(timeline.asc_acqs):\n",
    "    northing = acq.northing\n",
    "    if northing.size == 0:\n",
    "        northings[i]= np.NaN\n",
    "    else:\n",
    "        northings[i] = np.rad2deg(np.mean(northing))\n",
    "northings = northings.reshape(lon_grid.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_incs = np.tile(incs, n_orbits_cycle // n_orbits)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Global Incidence Angles"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12, 12))\n",
    "ax = fig.add_subplot(1, 1, 1, projection=ccrs.Mollweide())\n",
    "ax.coastlines(resolution='110m', zorder=10, alpha=0.5)\n",
    "#ax.add_feature(ocean_110m, zorder=5)\n",
    "#ax.add_feature(land_110m)\n",
    "\n",
    "#norm = colors.LogNorm(vmin=2, vmax=np.max(full_cover))\n",
    "filled_c = ax.contourf(full_lon_grid[:,:-1], full_lat_grid[:,:-1], full_incs[:,:-1], \n",
    "                         transform=ccrs.PlateCarree(),)\n",
    "colorbar = fig.colorbar(filled_c, orientation='horizontal')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Number of Acquisitions Over a Specific Region\n",
    "\n",
    "We can select a specific region to plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lat_lim = [-90, -60]\n",
    "lon_lim = [-180, 180]\n",
    "lat_id = [np.argmin(np.abs(latitudes - lat_lim[0])), np.argmin(np.abs(latitudes - lat_lim[1])) + 1]\n",
    "lon_id = [np.argmin(np.abs(longitudes - lon_lim[0])), np.argmin(np.abs(full_lon_grid[0, :] - lon_lim[1])) + 1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "figure = plt.figure(dpi=150, figsize=(8, 8))\n",
    "main_axis = figure.add_subplot(111, projection=ccrs.SouthPolarStereo())\n",
    "main_axis.coastlines(resolution='110m', zorder=10, alpha=0.5)\n",
    "main_axis.add_feature(ocean_110m, zorder=5)\n",
    "main_axis.add_feature(land_110m, zorder=1)\n",
    "\n",
    "norm = colors.LogNorm(vmin=5, vmax=np.max(full_cover[lat_id[0]:lat_id[1], lon_id[0]:lon_id[1]]))\n",
    "data_cmap = plt.get_cmap('jet')\n",
    "main_plot = main_axis.pcolormesh(full_lon_grid[lat_id[0]:lat_id[1], lon_id[0]:lon_id[1]],\n",
    "                     full_lat_grid[lat_id[0]:lat_id[1], lon_id[0]:lon_id[1]],\n",
    "                     full_cover[lat_id[0]:lat_id[1], lon_id[0]:lon_id[1]],\n",
    "                     cmap=data_cmap, zorder=6,\n",
    "                     transform=ccrs.PlateCarree(), norm=norm)\n",
    "cbar = plt.colorbar(main_plot)\n",
    "cbar.set_ticks([5,6,7,8,9,10,12,14,16,18,20,25,30,35,40])\n",
    "cbar.set_ticklabels([5,6,7,8,9,10,12,14,16,18,20,25,30,35,40])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
