{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Forward Model\n",
    "\n",
    "The forward model used in Harmony consists of a look-up table that relates values of wind speed and wind direction to the backscatter, in terms of the normalised radar cross-section (NRCS), and the Doppler received by the satellite for a given observation geometry. The observation geometry is specified by the angle of incidence of the signal that reaches the scatterer and the direction of the wind with respect to the incident signal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import drama.utils as drtls\n",
    "import drama.geo as sargeo\n",
    "import stereoid.sar_performance as strsarperf\n",
    "from stereoid.oceans import FwdModel\n",
    "from stereoid.instrument import ObsGeo, RadarModel\n",
    "import stereoid.utils.config as st_config"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "paths = st_config.parse(section=\"Paths\")\n",
    "data_dir = paths[\"data\"] / 'ScatteringModels/Oceans'\n",
    "fnameisv = 'C_band_isv_ocean_simulation.nc'\n",
    "modelstr = 'SSAlin'"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Input Model\n",
    "We instantiate the LUT by choosing the model we would like to use to build it, in this case linear SSA. Several models are available including:\n",
    "* SSA linear\n",
    "* KA linear\n",
    "* WCA linear\n",
    "* Hybrid model using Yan Yuan's model for the NRCS and one of the models above for the Doppler.\n",
    "(citations for all the models? Paco?)\n",
    "\n",
    "To build the look-up table, we start with precomputed model results that were produced by running simulations using one of the supported models. The simulations are run for a specific scene, illuminator-companion separation and transmitter-receiver polarisation and their results are stored in netCDF files. \n",
    "\n",
    "The LUT initialiser takes as input a string describing the desired model. It looks for the netCDF files that correspond to the specified model, and reads the files (there might be several corresponding to different polarisations and along-track separations). Before merging them, it ensures that all the datasets have the same dimensions and that their coordinates are aligned. In addition to the NRCS, the Doppler Centroid Anomaly is also extracted from the datasets and added to the LUT. Then the different datasets are merged into one in memory and separate arrays are constructed for each physical quantity of interest, e.g., incidence angle, NRCSS, incident azimuth.\n",
    "\n",
    "In the following section we will show an example of how to constrtuct the forward model using data from the SSA model at an angle of incidence of 35° and along-track separation of 350 km."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "fwdm = FwdModel(data_dir, data_dir / fnameisv, dspd=2, duvec=0.25, model=modelstr)\n",
    "fwdm.at_distance = 350e3\n",
    "fwdm.inc = 35"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A note on the coordinates\n",
    "After instantiating the forward model we can access the NRCS for different satellites. The data from the model are in a polar coordinate basis. The wind is described in terms of speed magnitude and azimuth. To make the processing easier and on a regular grid we convert the LUT to a cartesian grid of cross and along-track wind velocities. The conversion is done by first creating a cartesian grid that fits all the points of the polar system and then by mapping values of NRCS and Doppler from polar to cartesian using cubic spline interpolation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "u_min = fwdm.w_u.min()\n",
    "u_max = fwdm.w_u.max()\n",
    "# set indicent angle\n",
    "fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(15, 5))\n",
    "im1 = ax[0].imshow(drtls.db(fwdm.nrcs_lut(1, cart=True)[0]), vmin=-20, vmax=-7,\n",
    "                   origin='lower', extent=(u_min, u_max, u_min, u_max))\n",
    "#fig.colorbar(im1, ax=ax[0],fraction=0.046, pad=0.04)\n",
    "ax[0].set_title(\"$\\sigma_{0,VV}$ Sentinel-1\")\n",
    "im2 = ax[1].imshow(drtls.db(fwdm.nrcs_lut(1, cart=True)[1]), vmin=-20, vmax=-7,\n",
    "                   origin='lower', extent=(u_min, u_max, u_min, u_max))\n",
    "#fig.colorbar(im2, ax=ax[1], fraction=0.046, pad=0.01, label=\"[dB]\")\n",
    "fig.colorbar(im2, ax=ax.ravel().tolist(), label=\"[dB]\")\n",
    "ax[1].set_title(\"$\\sigma_{0,VP_s}$ Hrmny-A\")\n",
    "for ind in range(2):\n",
    "    ax[ind].set_xlabel(\"$U_u [m/s]$\")\n",
    "    if ind == 0:\n",
    "        ax[ind].set_ylabel(\"$U_v [m/s]$\")\n",
    "    ax[ind].set_aspect(\"equal\")"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Jacobian\n",
    "The link between the L1 nrcs and L2 surface stress uncertainty is established _mostly_ through the Jacobian of the multi-dimensional GMF, for example, for a single polarization and in Stereo configuration, the Jabobian of the GMF is given by\n",
    "$$J = \\begin{pmatrix} \\frac{\\partial \\sigma_{0,S1}}{\\partial U_u} & \\frac{\\partial \\sigma_{0,S1}}{\\partial U_v} \\\\ \\frac{\\partial \\sigma_{0,H-A}}{\\partial U_u} & \\frac{\\partial \\sigma_{0,H-A}}{\\partial U_v} \\\\ \n",
    "\\frac{\\partial \\sigma_{0,H-B}}{\\partial U_u} & \\frac{\\partial \\sigma_{0,H-B}}{\\partial U_v} \\end{pmatrix}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "## Jacobian of NRCS GMF and CDCA GMF for the incident angle specified\n",
    "jac_n, jac_d = fwdm.fwd_jacobian(incm)\n",
    "# the result is a n_sat x 2 pol x 2-D space x n_wind_v x n_wind_u matrix\n",
    "# we can visualize a few gradients\n",
    "fig, ax = plt.subplots(nrows=3, ncols=2, figsize=(12,14))\n",
    "sats = (\"S1\", \"H-A\", \"H-B\")\n",
    "wcmps = (\"U_u\", \"U_v\")\n",
    "for ind1 in range(3):\n",
    "    for ind2 in range(2):\n",
    "        im = ax[ind1, ind2].imshow(jac_n[ind1, 1, ind2], origin='lower', extent=(u_min, u_max, u_min, u_max),\n",
    "                                   cmap='bwr', vmin=-np.nanmax(np.abs(jac_n[0, 1, ind2])),\n",
    "                                   vmax=np.nanmax(np.abs(jac_n[0, 1, ind2])))\n",
    "        fig.colorbar(im, ax=ax[ind1, ind2],fraction=0.046, pad=0.04)\n",
    "        ax[ind1, ind2].set_title(\"$\\partial\\sigma_{0,%s}/\\partial %s$\" % (sats[ind1], wcmps[ind2]))\n",
    "        ax[ind1, ind2].set_xlabel(\"$U_u [m/s]$\")\n",
    "        ax[ind1, ind2].set_ylabel(\"$U_v [m/s]$\")"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition to the NRCS from the geophysical model, we expect the data to have noise that will give rise to uncertainty. We assume that the covariance matrix of the level-1 uncertainty is the following sum\n",
    "$$\n",
    "  \\Sigma_\\sigma = \\Sigma_{\\sigma,n} + \\Sigma_{\\sigma, g} + \\Sigma_{\\sigma, s}\n",
    "$$\n",
    "of three terms:\n",
    "1. random measurement noise, which is quickly suppressed if enough looks are available;\n",
    "2. geophysical noise;\n",
    "3. systematic errors, such as calibration errors.\n",
    "\n",
    "## Measurement noise\n",
    "\n",
    "For the first two terms we follow [1]. The measurement noise will be described by\n",
    "$$\n",
    "\\Sigma_{\\sigma,n} = \\begin{pmatrix} k_{n,S1}^2 \\sigma_{0,S1}^2 & 0 & 0 \\\\\n",
    "                                     0 & k_{n,H-A}^2\\sigma_{0,H-A}^2 & 0 \\\\\n",
    "                                     0 & 0 & k_{n,H-B}^2\\sigma_{0,H-B}^2\\end{pmatrix}.\n",
    "$$\n",
    "with \n",
    "$$\n",
    "  k^2_{n} = \\frac{1}{N_\\mathrm{looks}}\\left(1+\\frac{1}{\\mathrm{SNR}} \\right)^2 + \\frac{1}{N_\\mathrm{noise-looks}\\mathrm{SNR}^2}\n",
    "$$\n",
    "\n",
    "[1] Lin, Chung-Chi, Maurizio Betto, Maria Belmonte Rivas, Ad Stoffelen, and Jos de Kloe. “EPS-SG Windscatterometer Concept Tradeoffs and Wind Retrieval Performance Assessment.” IEEE Transactions on Geoscience and Remote Sensing 50, no. 7 (July 2012): 2458–72. https://doi.org/10.1109/TGRS.2011.2180393.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To compute the mesurement noise we need to first calculate the SNR of the receivers. In the interest of time. the NESZ is precomputed. We can load it into memory and use it compute the SNR."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "incm = 35 # Monostatic incident angle\n",
    "along_track_separation = 350e3\n",
    "swth_bst = sargeo.SingleSwathBistatic(par_file=paths[\"par\"] / \"Hrmny_2020_1.cfg\", dau=along_track_separation) \n",
    "obsgeo = ObsGeo.from_swath_geo(incm, swth_bst, ascending=True)\n",
    "mode = 'IWS'  # Set observation mode\n",
    "az_res_dct = {\"WM\":5, \"IWS\":20}\n",
    "az_res = az_res_dct[mode]\n",
    "rx_ati_name = 'tud_2020_half'  # name of system in parameter file\n",
    "rx_dual_name = 'tud_2020_dual6m' # full system, will have 3 dB better NESZ, etc.\n",
    "\n",
    "# read computed performance\n",
    "main_dir = paths[\"main\"]\n",
    "par_dir = paths[\"par\"]\n",
    "run_id = \"2020_1\"\n",
    "fstr_dual = strsarperf.sarperf_files(main_dir, rx_dual_name, mode=mode, runid=run_id, pardir=par_dir)\n",
    "fstr_ati = strsarperf.sarperf_files(main_dir, rx_ati_name, mode=mode, runid=run_id, pardir=par_dir)\n",
    "fstr_s1 = strsarperf.sarperf_files(main_dir, 'sentinel', is_bistatic=False, mode=mode, runid=run_id, pardir=par_dir)\n",
    "radarm = RadarModel(obsgeo, fstr_s1, fstr_dual, fstr_ati, az_res=az_res, prod_res=2e3, b_ati=9) "
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have loaded the NESZ of the receivers we can calculate the SNR and compute the diagonal entries of $\\Sigma_{\\sigma, n}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "# SNR determination\n",
    "incind = np.abs(radarm.dual_nesz.inc_v - incm).argmin()\n",
    "nesz_hrmny = np.mean(10**(radarm.dual_nesz.nesz[:, incind]/10))\n",
    "nesz_S1 = np.mean(10**(radarm.s1_nesz.nesz[:, incind]/10))\n",
    "\n",
    "snrs = (np.transpose(fwdm.nrcs_lut(1, cart=True),[1, 2, 0]) /\n",
    "        np.array([nesz_S1, nesz_hrmny, nesz_hrmny]).reshape((1, 1, 3)))\n",
    "wind_prod_res = 1e3\n",
    "n_looks = wind_prod_res**2 / az_res_dct[mode] / 5\n",
    "alpha_p = np.sqrt(1/n_looks * ((1+1 / snrs)**2 + 1 / snrs**2))\n",
    "fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 5))\n",
    "plt.subplots_adjust(bottom=0.25)\n",
    "for ind1 in range(3):\n",
    "    im = ax[ind1].imshow(alpha_p[:,:, ind1], origin='lower', extent=(u_min, u_max, u_min, u_max),\n",
    "                         vmax = 0.015, vmin=0.005) # cmap='inferno') , vmin=0, vmax=3)\n",
    "    if ind1 == 2:\n",
    "        fig.colorbar(im, ax=ax[ind1],fraction=0.046, pad=0.04)\n",
    "    #ax[ind1, ind2].set_title(titles[ind1][ind2])\n",
    "    ax[ind1].set_xlabel(\"$U_u [m/s]$\")\n",
    "    if ind1 == 0:\n",
    "        ax[ind1].set_ylabel(\"$U_v [m/s]$\")\n",
    "    \n",
    "figtitle = r\"Diagonal elements of $k_{n, i}$. \"\n",
    "plt.figtext(0.1, 0.01,figtitle, wrap=True, fontsize='large')"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "j = jac_n[:, 1]   # n_sat x 2-D space x n_wind_v x n_wind_u matrix\n",
    "# JˆH \\cdot J\n",
    "# we also transpose while we are at it\n",
    "jhj = np.einsum(\"jimn,jkmn->mnik\", j, j)\n",
    "# Now pseudo inverse\n",
    "jhi_i = np.linalg.inv(jhj)\n",
    "j_pi = np.einsum(\"mnik,jkmn->mnij\", jhi_i, j)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "cov_s = np.zeros((j.shape[2], j.shape[3], j.shape[0], j.shape[0]))\n",
    "fwdm.inc = incm\n",
    "#alpha = (10**(0.25/10) - 1)\n",
    "for ind in range(3):\n",
    "    cov_s[:, :, ind, ind] = alpha_p[:,:, ind]**2 * fwdm.nrcs_lut(1, cart=True)[ind]**2\n",
    "cov_w = np.einsum(\"mnik,mnkj,mnlj->mnil\", j_pi, cov_s, j_pi)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 5))\n",
    "plt.subplots_adjust(bottom=0.25)\n",
    "for ind1 in range(3):\n",
    "    im = ax[ind1].imshow(cov_s[:,:, ind1, ind1], origin='lower', extent=(u_min, u_max, u_min, u_max)) # cmap='inferno') , vmin=0, vmax=3)\n",
    "    if ind1 == 2:\n",
    "        fig.colorbar(im, ax=ax[ind1],fraction=0.046, pad=0.04)\n",
    "    #ax[ind1, ind2].set_title(titles[ind1][ind2])\n",
    "    ax[ind1].set_xlabel(\"$U_u [m/s]$\")\n",
    "    if ind1 == 0:\n",
    "        ax[ind1].set_ylabel(\"$U_v [m/s]$\")\n",
    "    \n",
    "figtitle = r\"Diagonal elements of $\\Sigma_{\\sigma,n}$. \"\n",
    "plt.figtext(0.1, 0.01,figtitle, wrap=True, fontsize='large')"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Geophysical noise\n",
    "\n",
    "The geophysical noise is more complicated to model. In [1] the authors follow [2], where a model for the geophysical noise was derived, giving a NRCS dispersion approximated by \n",
    "$$\n",
    "  k_g = 0.12 \\cdot e^{-U/12}.\n",
    "$$\n",
    "at ASCAT resolutions.\n",
    "As discussed in the literature, several factors contribute to this geophysical noise:\n",
    "1. Variability of the stress equivalent wind within the resolution cell of the product. Considering the wind as a random field, the instantaneous mean over an area is itself a random variable. The effect of this variability will depend on the product resolution (or size of the footprint, thinking in scatterometric terms), and on the scales over which the wind field varies. Since the spectral peak of the wind wavenumber spectrum is typically O(1km), at higher resolutions than that the effect of this variability drops.\n",
    "2. Imperfect overlap between the NRCS measurements for different beams (colocation errors). In the case of Hrmony, given the high resolution of the SLC products, this effect should be small.\n",
    "3. Temporal lag between observations of different views. This temporal lag is absent in the case of Harmony.\n",
    "\n",
    "Other effects would include, for example, modulations due to surfactants.\n",
    "\n",
    "In general, we will assume that for high resolution products, this geo-physical noise contributions are small enough to be ignored. An alternative take is to assume that all observed variability can be interpreted as a variability of the stress equivalent wind vector. \n",
    "\n",
    "### Geophysical noise correlation\n",
    "\n",
    "If some level of gephysial noise where to be considered, it is worth noting that this should be expected to be correlated for the different views (Sentinel-1, and Harmony A and B), as the observations are simultaneous and the azimuth diversity is limited. We postulate that the covariance matrix would have a form\n",
    "$$\n",
    "\\Sigma_{\\sigma,gn} = k_g^2 \\begin{pmatrix}  \\sigma_{0,S1}^2 & \\cos \\alpha_a\\sigma_{0,S1}\\sigma_{0,H-A} & \\cos \\alpha_a\\sigma_{0,S1}\\sigma_{0,H-B}  \\\\\n",
    "                                     \\cos \\alpha_a\\sigma_{0,S1}\\sigma_{0,H-A} & \\sigma_{0,H-A}^2 & \\cos (2\\alpha_a)\\sigma_{0,H-A}\\sigma_{0,H-B} \\\\\n",
    "                                     \\cos \\alpha_a\\sigma_{0,S1}\\sigma_{0,H-B} & \\cos (2\\alpha_a)\\sigma_{0,H-A}\\sigma_{0,H-B} & \\sigma_{0,H-B}^2\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "with $\\alpha_a$ the effective ground-projected azimuh angle between the observations taken as half the azimuth-projected bistatic angle. This model makes the error fully correlated if the observation geometries are identical, as it should, and fully uncorrelated if the azimuth viewing directions are orthogonal. The latter is unrealistic, but relatively unimportant as the effective angles conseidered are smal, less than 25 degree.\n",
    "\n",
    "To illustrate the impact of geophysical noise under this assumption we have assumed \n",
    "$$\n",
    "  k_g = 0.06 \\cdot e^{-U/12},\n",
    "$$\n",
    "i.e. we assume that the disperssion of the NRCS is half that of ASCAT.\n",
    "\n",
    "[1] Lin, Chung-Chi, Maurizio Betto, Maria Belmonte Rivas, Ad Stoffelen, and Jos de Kloe. “EPS-SG Windscatterometer Concept Tradeoffs and Wind Retrieval Performance Assessment.” IEEE Transactions on Geoscience and Remote Sensing 50, no. 7 (July 2012): 2458–72. https://doi.org/10.1109/TGRS.2011.2180393.\n",
    "\n",
    "[2] Portabella, M., and A. Stoffelen. “Scatterometer Backscatter Uncertainty Due to Wind Variability.” IEEE Transactions on Geoscience and Remote Sensing 44, no. 11 (November 2006): 3356–62. https://doi.org/10.1109/TGRS.2006.877952."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "source": [
    "def plot_cov_wind(u_u, u_v, cov_w, vmax1=0.5):\n",
    "    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(12,10))\n",
    "    wcmps = (\"U_u\", \"U_v\")\n",
    "    vmax= [[vmax1, 1],[1, vmax1]]\n",
    "    vmin = [[0, -1], [-1,0]]\n",
    "    levels = [[np.linspace(0,vmax1,21), np.linspace(-1,1,41)], [np.linspace(-1,1,41), np.linspace(0,vmax1,21)]]\n",
    "    extnd = [['max', 'neither'], ['neither', 'max']]\n",
    "    \n",
    "    cmps = [['Spectral','seismic'],['seismic', 'Spectral']]\n",
    "    titles = [[\"$\\sigma_{U_u}$\", r\"$\\rho_{U_uU_v}$\"], [r\"$\\rho_{U_vU_u}$\", \"$\\sigma_{U_v}$\"]]\n",
    "    pdat = [[np.sqrt(cov_w[:, :, 0, 0]),  \n",
    "             cov_w[:, :, 0, 1]/np.sqrt(cov_w[:, :, 0, 0]*cov_w[:, :, 1, 1])],\n",
    "            [cov_w[:, :, 1, 0]/np.sqrt(cov_w[:, :, 0, 0]*cov_w[:, :, 1, 1]),\n",
    "             np.sqrt(cov_w[:, :, 1, 1])]]\n",
    "\n",
    "    for ind1 in range(2):\n",
    "        for ind2 in range(2):\n",
    "            #im = ax[ind1, ind2].imshow(pdat[ind1][ind2], origin='lower', extent=(u_min, u_max, u_min, u_max),\n",
    "            #                           vmax=vmax[ind1][ind2], vmin=vmin[ind1][ind2], cmap=cmps[ind1][ind2])\n",
    "            im = ax[ind1, ind2].contourf(u_u, u_v, pdat[ind1][ind2],\n",
    "                                         cmap=cmps[ind1][ind2], levels=levels[ind1][ind2],\n",
    "                                         vmax=vmax[ind1][ind2], vmin=vmin[ind1][ind2], \n",
    "                                         extend=extnd[ind1][ind2])\n",
    "            ax[ind1, ind2].set_aspect(\"equal\")\n",
    "            fig.colorbar(im, ax=ax[ind1, ind2],fraction=0.046, pad=0.04)\n",
    "            ax[ind1, ind2].set_title(titles[ind1][ind2])\n",
    "            ax[ind1, ind2].set_xlabel(\"$U_u [m/s]$\")\n",
    "            ax[ind1, ind2].set_ylabel(\"$U_v [m/s]$\")\n",
    "    return fig"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "alpha_a = np.radians(obsgeo.bist_ang/2)\n",
    "cov_g = np.zeros((j.shape[2], j.shape[3], j.shape[0], j.shape[0]))\n",
    "fwdm.inc = incm\n",
    "u_u = fwdm.w_u.reshape((1, fwdm.w_u.size))\n",
    "u_v = fwdm.w_v.reshape((fwdm.w_v.size, 1))\n",
    "u_mag = np.sqrt(u_v**2 + u_u**2)\n",
    "k_g = 0.04 * np.exp(-u_mag/12)\n",
    "for ind in range(3):\n",
    "    cov_g[:, :, ind, ind] = fwdm.nrcs_lut(1, cart=True)[ind]**2\n",
    "cov_g[:, :, 0, 1] = fwdm.nrcs_lut(1, cart=True)[0] * fwdm.nrcs_lut(1, cart=True)[1] * np.cos(alpha_a)\n",
    "cov_g[:, :, 1, 0] = cov_g[:, :, 0, 1]\n",
    "cov_g[:, :, 0, 2] = fwdm.nrcs_lut(1, cart=True)[0] * fwdm.nrcs_lut(1, cart=True)[2] * np.cos(alpha_a)\n",
    "cov_g[:, :, 2, 0] = cov_g[:, :, 0, 2]\n",
    "cov_g[:, :, 1, 2] = fwdm.nrcs_lut(1, cart=True)[1] * fwdm.nrcs_lut(1, cart=True)[2] * np.cos(2*alpha_a)\n",
    "cov_g[:, :, 2, 1] = cov_g[:, :, 1, 2] \n",
    "cov_g = k_g.reshape(u_mag.shape + (1,1))**2 * cov_g\n",
    "cov_wg = np.einsum(\"mnik,mnkj,mnlj->mnil\", j_pi, cov_g, j_pi)\n",
    "\n",
    "cov_w_fig = plot_cov_wind(fwdm.w_u, fwdm.w_v, cov_wg, vmax1=1)\n",
    "figtitle = \"Diagonal panels: standard deviation of retrieved wind component uncertainties due to geophysical noise. \\\n",
    " \\nOff-diagonal: correlation coefficients between the errors.\"\n",
    "\n",
    "plt.figtext(0.1, 0.03,figtitle, wrap=True, fontsize='large')\n"
   ],
   "outputs": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now sum together the two covariance matrices to compute the total covariance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [
    "total_cov = cov_wg + cov_w\n",
    "cov_w_fig = plot_cov_wind(fwdm.w_u, fwdm.w_v, total_cov, vmax1=1)"
   ],
   "outputs": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "source": [],
   "outputs": []
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,py:light"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
