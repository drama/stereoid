{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Some theory\n",
    "\n",
    "## Doppler resolved spectra\n",
    "We assume an intensity signal $i[n] = x[n] \\cdot [n]^2$, and its Discrete Fourier Transform $I_1[k]$, which we can call the _single look_ complex one dimensional spectrum. Note that except for the $k=0$ value, which is the (scaled) mean of the intensity, and therefore positive valued, the other coefficients are complex valued and with, generally speaking, a uniformly distributed random phase. Its expected value is the actual complex spectrum:\n",
    "$$\n",
    "E\\left[I_1[k]\\right] = I[k].\n",
    "$$\n",
    "The variance of the estimate of this is\n",
    "$$\n",
    "\\sigma^2_{I_1}[k]=\\frac{2}{M}|I[k]|^2 * \\Lambda\\left(\\frac{k}{M/2}\\right),\n",
    "$$\n",
    "the convolution of the energy spectrum of the intensity signal with a triangular function, where the triangular shape is the result of a factor two oversampling of the complex data ($x[n]$).\n",
    "\n",
    "After _coherent_ averaging of $N_{blk}$ independent samples (or apertures) we have\n",
    "$$\n",
    "E\\left[I_{N_{blk}}[k]\\right] = I[k]\n",
    "$$\n",
    "and\n",
    "$$\n",
    "\\sigma^2_{I_{N_{blk}}}[k]=\\frac{2}{N_{blk} \\cdot M}|I[k]|^2 * \\Lambda\\left(\\frac{k}{M/2}\\right).\n",
    "$$\n",
    "Here, coherent averaging implies that we have independent realizations of the _speckle_ but that the underlying modulation is the same. Note that if the averaging has a filtering effect on the signal itself the previous relations do not necessarily hold.\n",
    "\n",
    "For incoherent averaging of the intensity spectra we take averages of $|I_{N_{blk}}[k]|^2$.\n",
    "\n",
    "$$\n",
    " E\\left[|I_{N_{blk}}|^2_{N_i}[k]\\right]  = |I[k]|^2 * \\left(\\delta[k] + \\frac{2}{N_{blk}\\cdot M}\\Lambda\\left(\\frac{k}{M/2}\\right) \\right)\n",
    "$$\n",
    "\n",
    "Finally, the variance of this estimate is given by\n",
    "$$\n",
    "  \\sigma^2_{|I|^2} \\approx \\frac{4}{N_i} \\left(|I[k||^2 + \\frac{1}{2}\\sigma^2_{I_{N_{blk}}}[k]\\right) \\cdot \\sigma^2_{I_{N_{blk}}}[k]\n",
    "$$\n",
    "\n",
    "## Intensity spectrum\n",
    "\n",
    "The error will depend on the intensity spectrum, $S_I[k]$. This 1-D spectrum is a discretization of a 2-D intensity spectrum, $S_I(k_a, k_r)$ filtered by the radar and sampled at $(0, k_r)$. We assume that this spectrum has the shape\n",
    "$$\n",
    "  S_I(k_a, k_r) = C_a(k_a) \\cdot k^\\alpha S_w(k_r) + C_0\\cdot \\delta(k_a, k_r),\n",
    "$$\n",
    "with $S_w(k_r)$ the omnidirectional wave spectrum. The radar filters the intensity spectrum convolving it with a function $H_I(k_a, k_r)$, where in the simplest approximation we can assume  it to be\n",
    "$$\n",
    "  H_I(k_a, k_r) = \\Pi\\left(\\frac{k_a}{2\\pi/L_a}\\right) \\cdot \\Pi\\left(\\frac{k_r}{2\\pi/L_r}\\right),\n",
    "$$\n",
    "with $L_a$ and $L_r$ the width of the antenna foot-print in the azimuth direction and the ground-projected length of the echo window, respectively. Assuming a smooth spectrum compared to the spectral resolution, we get for the 1-D radar spectrum\n",
    "$$\n",
    "  S_I(k_r) \\approx \\frac{4\\pi}{L_a \\cdot L_r} C_a(0) \\cdot k_r^\\alpha S_w(k_r) + C_0\\cdot \\delta(k_r).\n",
    "$$\n",
    "\n",
    "## Signal to _clutter_ and _unfocused_ SAR\n",
    "\n",
    "Since most of the uncertainty in this model comes from the convolution of the triangular function with the $C_0\\cdot \\delta(k_r)$ term, it seems useful to define a Signal to Clutter ratio as\n",
    "$$\n",
    "\\mathrm{SCR} = \\frac{4\\pi}{L_a \\cdot L_r}\\cdot\\frac{\\int  C_a(0) \\cdot k_r^\\alpha S_w(k_r) dk_r}{C_0}.\n",
    "$$\n",
    "This SCR is, therefore, proportional to $1/L_a$. By decreasing $L_a$, SAR focusing  will increase this ratio by a factor equal to the effective number of resolved azimuth (or Doppler) beams, $n_\\mathrm{Dop}$.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from matplotlib import pyplot as plt\n",
    "from oceansar import spec as ocsspec\n",
    "from oceansar import spread as ocsspread\n",
    "from oceansar.swell_spec.dir_swell_spec import ardhuin_swell_spec as swell_spec\n",
    "from drama.geo import sar as drgeo\n",
    "from drama import utils as drtls"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_az = 1024\n",
    "n_sar = 32\n",
    "az = np.radians(90)\n",
    "azovs = 32e3/(7.5e3*2)\n",
    "\n",
    "U_10 = 7\n",
    "\n",
    "Bp = 190e6\n",
    "ovs = 1.25\n",
    "Fs = 250e6\n",
    "fetch = 500e3\n",
    "inc_ang = np.radians(12)\n",
    "nrg = 2048\n",
    "echo_win_length = 3e8 / 2 / Fs * nrg / np.sin(inc_ang)\n",
    "rad_az = 90  # or 90\n",
    "add_swell = True\n",
    "\n",
    "k = 2 * np.pi * np.fft.fftfreq(2 * nrg, echo_win_length/nrg/2) #[0:nrg]\n",
    "k_ = np.copy(k)\n",
    "k_[0] = k[1] / 10  # To avoid divide by zero warnings\n",
    "# k_max according to pulse bandwidth\n",
    "k_max = np.sin(inc_ang) * 2 * np.pi / (3e8/ 2 / Bp)\n",
    "wspec_wind = ocsspec.elfouhaily(np.abs(k_), U_10, fetch)/2\n",
    "\n",
    "if add_swell:\n",
    "    wspec_swell = swell_spec(np.abs(k_), np.radians(rad_az), 45)/2\n",
    "    wspread = ocsspread.elfouhaily(np.abs(k_), np.angle(np.exp(1j*np.radians(rad_az - 90))), U_10, fetch)\n",
    "else:\n",
    "    wspec_swell = np.zeros_like(k)\n",
    "    rad_az = 30\n",
    "    wspread = ocsspread.elfouhaily(np.abs(k_), np.angle(np.exp(1j*np.radians(rad_az - 30))), U_10, fetch)\n",
    "wspec = wspec_wind * wspread\n",
    "\n",
    "n_dop = np.ceil(n_sar/azovs * np.abs(np.sin(np.radians(rad_az)) + 0.1))  # approximation\n",
    "n_smth = 8\n",
    "# but because we had applied a Hanning in processing:\n",
    "n_smth = n_smth * np.sum(np.hanning(1024)**2)/1024\n",
    "n_incoh = n_dop * n_smth\n",
    "n_blk = n_az / n_sar\n",
    "\n",
    "plt.plot(k[0:nrg], (np.abs(k) * (wspec_swell + wspec))[0:nrg])\n",
    "plt.grid(True)\n",
    "plt.xlim((0,1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# wavespectrum to intensity modulation coefficient \n",
    "def wspec_to_nrcsspec(k, wspec, alpha, scr_rar, n_dop, wswell_spec=None):\n",
    "    # Signal to DC clutter\n",
    "    scr = scr_rar * n_dop\n",
    "    # I assume that the n_dop dependency is true, but the number in front I don't know, so I am guessing something to make this work.\n",
    "    nrcs_spec = np.abs(k)**alpha * wspec \n",
    "    # Add the DC peak\n",
    "    nrcs_spec[0] = np.nansum(nrcs_spec)/scr\n",
    "    alpha2 = 1\n",
    "    k_ = np.copy(k)\n",
    "    k_[0] = k[1]/10\n",
    "    if not wswell_spec is None:\n",
    "        print('Adding swell')\n",
    "        k_y_c = 2 * np.pi / 6e3 * n_dop\n",
    "        k_y_bw = np.abs(k_) * np.radians(7)\n",
    "        Hk = 2.2 * np.erf(0.12 * k_y_c / k_y_bw)  #np.where(k_y_bw > k_y_c, k_y_c / k_y_bw, 1)\n",
    "        #plt.figure()\n",
    "        #plt.plot(k, Hk)\n",
    "        nrcs_spec = nrcs_spec + np.abs(k)**alpha2 * np.abs(wswell_spec) * nrcs_spec[0] * scr * Hk / n_dop \n",
    "        \n",
    "    return nrcs_spec\n",
    "\n",
    "alpha = 1\n",
    "wspec_swell = None\n",
    "S_I = wspec_to_nrcsspec(k, wspec, alpha, 0.002, n_dop, wswell_spec=wspec_swell)\n",
    "S_I_rar = wspec_to_nrcsspec(k, wspec, alpha, 0.002, 1, wswell_spec=wspec_swell)\n",
    "\n",
    "plt.plot(k[1:nrg], S_I[1:nrg], label='Doppler resolved')\n",
    "#plt.plot(k[1:nrg], S_I_rar[1:nrg], label='RAR')\n",
    "plt.xlabel('k [rad/m]')\n",
    "#plt.legend(loc=0)\n",
    "plt.grid(True)\n",
    "#plt.yscale('log')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def nrcs_spec_var(k, nrcs_spec, nrg, n_blk=1, range_ovs=1, k_max=None):\n",
    "    if k_max is None:\n",
    "        k_max = k.max()\n",
    "    \n",
    "    #Triangular window\n",
    "    triang = (k_max - np.abs(k))/k_max\n",
    "    triang = np.where(triang > 0, triang, 0)\n",
    "    aux = np.zeros_like(nrcs_spec)\n",
    "    aux[0]=1\n",
    "    S_I_conv_tr = np.abs(np.fft.ifft(np.fft.fft(triang) * np.fft.fft(nrcs_spec)))\n",
    "    var_I_Nblk = 2 / (nrg / range_ovs) / n_blk * S_I_conv_tr\n",
    "    return var_I_Nblk\n",
    "    \n",
    "var_I_Nblk = nrcs_spec_var(k, S_I, nrg, n_blk=n_blk, range_ovs=Fs/Bp, k_max=k_max)\n",
    "var_I_Nblk_rar = nrcs_spec_var(k, S_I_rar, nrg, n_blk=n_az, range_ovs=Fs/Bp, k_max=k_max)\n",
    "\n",
    "I2_Nblk = S_I + var_I_Nblk\n",
    "I2_rar = S_I_rar + var_I_Nblk_rar\n",
    "var_I2 = (4 /n_incoh) * (S_I + var_I_Nblk/2) * var_I_Nblk\n",
    "var_I2_rar = (4 /n_smth) * (S_I_rar + var_I_Nblk_rar/2) * var_I_Nblk_rar\n",
    "plt.figure()\n",
    "plt.plot(k[1:nrg], I2_Nblk[1:nrg])\n",
    "# plt.plot(k[1:nrg], var_I_Nblk[1:nrg],'--')\n",
    "plt.xlabel('k [rad/m]')\n",
    "#plt.ylim((0,0.2))\n",
    "plt.grid(True)\n",
    "plt.figure()\n",
    "plt.fill_between(k[1:nrg], I2_Nblk[1:nrg] - np.sqrt(var_I2[1:nrg]),\n",
    "                 I2_Nblk[1:nrg] + np.sqrt(var_I2[1:nrg]), alpha=0.2, color='blue')\n",
    "plt.plot(k[1:nrg], I2_Nblk[1:nrg], color='blue', label=(\"$N_{dop}=%i$\" % (int(n_dop))))\n",
    "plot_rar = True\n",
    "if plot_rar:\n",
    "    plt.fill_between(k[1:nrg], I2_rar[1:nrg] - np.sqrt(var_I2_rar[1:nrg]),\n",
    "                     I2_rar[1:nrg] + np.sqrt(var_I2_rar[1:nrg]), alpha=0.2, color='green')\n",
    "    plt.plot(k[1:nrg], I2_rar[1:nrg], color='green', label=(\"$N_{dop}=%i$\" % (int(1))))\n",
    "\n",
    "plt.legend(loc=0)\n",
    "plt.xlabel('k [rad/m]')\n",
    "#plt.ylim((0,0.2))\n",
    "plt.grid(True)\n",
    "plt.figure()\n",
    "plt.plot(k[1:nrg], np.sqrt(var_I2[1:nrg])/I2_Nblk[1:nrg], color='blue', label=(\"$N_{dop}=%i$\" % (int(n_dop))))\n",
    "if plot_rar:\n",
    "   plt.plot(k[1:nrg], np.sqrt(var_I2_rar[1:nrg])/I2_rar[1:nrg], color='green', label=(\"$N_{dop}=%i$\" % 1))\n",
    "\n",
    "plt.legend(loc=0)\n",
    "#plt.ylim((0,0.15))\n",
    "plt.xlabel('k [rad/m]')\n",
    "plt.grid(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cross check with Oceansar4SKIM simulations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fdir = '/Users/plopezdekker/Documents/WORK/SKIM/RESULTS/Spectra'\n",
    "# specf = 'spectra_data_U7at90.npz'\n",
    "if add_swell:\n",
    "    if rad_az == 45:\n",
    "        if n_sar == 32:\n",
    "            specf = 'spectra_data_wind90_swell45_radar45_nsar32.npz'\n",
    "            casestr = 'w90_s45_rad45_nsar32'\n",
    "        else:\n",
    "            specf = 'spectra_data_wind90_swell45_radar45.npz'\n",
    "            casestr = 'w90_s45_rad45'\n",
    "        ylim = (-0.01, 1)\n",
    "        linthreshy = 0.01\n",
    "    else:\n",
    "        if n_sar == 32:\n",
    "            specf = 'spectra_data_wind90_swell45_radar90_nsar32.npz'\n",
    "            casestr = 'w90_s45_rad90_nsar32'\n",
    "        else:\n",
    "            specf = 'spectra_data_wind90_swell45_radar90.npz'\n",
    "            casestr = 'w90_s45_rad90'\n",
    "        ylim = (-0.01, 1)\n",
    "        linthreshy = 0.01\n",
    "else:\n",
    "    if n_sar == 32:\n",
    "        specf = 'spectra_data_wind30_noswell_radar30_nsar32.npz'\n",
    "        casestr = 'w30_noswell_rad30_nsar32'\n",
    "    else:\n",
    "        specf = 'spectra_data_wind30_noswell_radar30.npz'\n",
    "        casestr = 'w30_noswell_rad30'\n",
    "    ylim = (-0.01, 0.1)\n",
    "    linthreshy = 0.01\n",
    "ylim2 = (0, 0.03)\n",
    "xlim2 = (0.03,1.5)\n",
    "import os\n",
    "fullfilename = os.path.join(fdir,specf)\n",
    "specdata = np.load(fullfilename)\n",
    "# np.savez('spectra_data.npz', int_spec=int_spec, sar_int_spec=sar_int_spec,dkx=dkx)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k_sim = np.real(specdata['dkx']).flatten()\n",
    "sar_int_spec = specdata['sar_int_spec']\n",
    "\n",
    "m_sar_spec = np.mean(np.real(sar_int_spec), axis=0)[0:nrg]\n",
    "\n",
    "normf = I2_Nblk[100:nrg].max()/drtls.smooth(m_sar_spec, 40)[100:nrg].max()\n",
    "if rad_az == 90:\n",
    "    normf90 = 0 + normf\n",
    "# Fixing it for one case\n",
    "print(normf)\n",
    "#normf = normf90\n",
    "print(normf90)\n",
    "\n",
    "m_sar_spec = normf * m_sar_spec \n",
    "m_sar_spec[0] = m_sar_spec[1]\n",
    "s_sar_spec = normf * np.std(np.real(sar_int_spec), axis=0)[0:nrg]\n",
    "s_sar_spec[0] = s_sar_spec[1]\n",
    "try:\n",
    "    rar_int_spec = specdata['int_spe_nouguier']\n",
    "except:\n",
    "    rar_int_spec = specdata['int_spec']\n",
    "m_rar_spec = np.mean(np.real(rar_int_spec), axis=0)[0:nrg]\n",
    "\n",
    "normfr = I2_rar[100:nrg].max()/drtls.smooth(m_rar_spec, 40)[100:nrg].max()\n",
    "if rad_az == 90:\n",
    "    normfr90 = 0 + normfr\n",
    "print(normfr)\n",
    "# Fixing it\n",
    "#normfr = normfr90\n",
    "print(normfr90)\n",
    "m_rar_spec = normfr * m_rar_spec \n",
    "m_rar_spec[0] = m_rar_spec[1]\n",
    "s_rar_spec = normfr * np.std(np.real(rar_int_spec), axis=0)[0:nrg]\n",
    "s_rar_spec[0] = s_rar_spec[1]\n",
    "\n",
    "if n_sar == 32:\n",
    "    m_sar_spec_32 = m_sar_spec\n",
    "    s_sar_spec_32 = s_sar_spec\n",
    "    I2_Nblk_32 = I2_Nblk\n",
    "    var_I2_32 = var_I2\n",
    "elif n_sar == 64:\n",
    "    m_sar_spec_64 = m_sar_spec\n",
    "    s_sar_spec_64 = s_sar_spec\n",
    "    I2_Nblk_64 = I2_Nblk\n",
    "    var_I2_64 = var_I2\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "#plt.plot(k_sim, m_sar_spec, color='red')\n",
    "plt.fill_between(k_sim, m_sar_spec - s_sar_spec,\n",
    "                 m_sar_spec + s_sar_spec, alpha=0.2, color='blue')\n",
    "plt.plot(k_sim, m_sar_spec, color='blue')\n",
    "\n",
    "plt.fill_between(k[1:nrg], I2_Nblk[1:nrg] - np.sqrt(var_I2[1:nrg]),\n",
    "                 I2_Nblk[1:nrg] + np.sqrt(var_I2[1:nrg]), alpha=0.2, color='red')\n",
    "plt.plot(k[1:nrg], I2_Nblk[1:nrg], color='red')\n",
    "plt.xlabel('k [rad/m]')\n",
    "plt.ylim(ylim2)\n",
    "plt.xlim(xlim2)\n",
    "#plt.xscale('log')\n",
    "#plt.yscale('symlog', linthreshy=linthreshy)\n",
    "# plt.ylim((0,0.1))\n",
    "plt.grid(True)\n",
    "plt.savefig(os.path.join(fdir,'intensity_spec_semianalytic_ovl_sim.png'))\n",
    "\n",
    "plt.figure()\n",
    "#plt.plot(k_sim, m_sar_spec, color='red')\n",
    "plt.fill_between(k_sim, m_rar_spec - s_rar_spec,\n",
    "                 m_rar_spec + s_rar_spec, alpha=0.2, color='red')\n",
    "plt.plot(k_sim, m_rar_spec, color='red')\n",
    "\n",
    "plt.fill_between(k[1:nrg], I2_rar[1:nrg] - np.sqrt(var_I2_rar[1:nrg]),\n",
    "                 I2_rar[1:nrg] + np.sqrt(var_I2_rar[1:nrg]), alpha=0.2, color='blue')\n",
    "plt.plot(k[1:nrg], I2_rar[1:nrg])\n",
    "plt.xlabel('k [rad/m]')\n",
    "# plt.ylim((0,0.1))\n",
    "plt.grid(True)\n",
    "plt.savefig(os.path.join(fdir,'intensity_spec_semianalytic_ovl_sim_rar.png'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fontsize = 14\n",
    "import matplotlib as mpl\n",
    "font = {'family': \"Arial\",\n",
    "        'weight': 'normal',\n",
    "        'size': fontsize}\n",
    "mpl.rc('font', **font)\n",
    "#ylim = (0.01, 0.2)\n",
    "fstsmp = 6\n",
    "xlim = (k_sim[fstsmp], 1.)\n",
    "plt.figure(figsize=(12,6))\n",
    "plt.subplot(1,2,1)\n",
    "plt.fill_between(k[1:nrg], I2_Nblk[1:nrg] - np.sqrt(var_I2[1:nrg]),\n",
    "                 I2_Nblk[1:nrg] + np.sqrt(var_I2[1:nrg]), alpha=0.2, color='blue')\n",
    "plt.plot(k[1:nrg], I2_Nblk[1:nrg])\n",
    "if plot_rar:\n",
    "    plt.fill_between(k[1:nrg], I2_rar[1:nrg] - np.sqrt(var_I2_rar[1:nrg]),\n",
    "                     I2_rar[1:nrg] + np.sqrt(var_I2_rar[1:nrg]), alpha=0.2, color='green')\n",
    "    plt.plot(k[1:nrg], I2_rar[1:nrg], color='green', label=(\"$N_{dop}=%i$\" % (int(1))))\n",
    "\n",
    "plt.xlabel('k [rad/m]')\n",
    "# plt.ylim((0,0.1))\n",
    "plt.grid(True)\n",
    "plt.ylim(ylim)\n",
    "plt.xlim(xlim)\n",
    "plt.xscale('log')\n",
    "plt.xscale('log')\n",
    "plt.yscale('symlog', linthreshy=linthreshy)\n",
    "plt.ylabel('$S_I(k)$')\n",
    "plt.title('Semi-analytical')\n",
    "plt.subplot(1,2,2)\n",
    "plt.fill_between(k_sim[fstsmp:], m_sar_spec[fstsmp:] - s_sar_spec[fstsmp:],\n",
    "                 m_sar_spec[fstsmp:] + s_sar_spec[fstsmp:], alpha=0.2, color='blue')\n",
    "plt.plot(k_sim[fstsmp:], m_sar_spec[fstsmp:], color='blue', label='Doppler-resolved')\n",
    "if plot_rar:\n",
    "    plt.fill_between(k_sim, m_rar_spec - s_rar_spec,\n",
    "                 m_rar_spec + s_rar_spec, alpha=0.2, color='green')\n",
    "    plt.plot(k_sim, m_rar_spec, color='green', label='real-aperture')\n",
    "plt.xlabel('k [rad/m]')\n",
    "plt.ylabel('$S_I(k)$')\n",
    "plt.title('Numerical')\n",
    "plt.ylim(ylim)\n",
    "plt.xlim(xlim)\n",
    "plt.xscale('log')\n",
    "plt.yscale('symlog', linthreshy=linthreshy)\n",
    "# plt.yscale('log')\n",
    "plt.grid(True)\n",
    "plt.legend(loc=0)\n",
    "plt.tight_layout()\n",
    "figname = (\"nrcs_spec_theor_num_%s.png\" % casestr)\n",
    "plt.savefig(os.path.join(fdir, figname))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Remove noise floor from data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def spectrum_noise_floor(k, nrcs_spec, k_max, k_hcut=0.75):\n",
    "    from numpy.polynomial import polynomial as nppol\n",
    "    nit = nrcs_spec.shape[0]\n",
    "    # Assume noise floor for small wavenumbers, will fail if we have swell\n",
    "    Sk30 = np.mean(np.real(nrcs_spec[:, 10:51]), axis=1).reshape((nrcs_spec.shape[0], 1))\n",
    "    k_km = (k_max - k).reshape((1, int(nrcs_spec.shape[1]/2)))\n",
    "    slope_1 = Sk30/(k_max - k_sim[30])\n",
    "    # k_km = (k_max - k_sim).reshape((1, int(sar_int_spec.shape[1]/2)))\n",
    "    noise_fl_1 = np.where(k_km > 0, k_km * slope_1, 0)\n",
    "    k_hcut_ind = np.argmin(np.abs(k - k_hcut))\n",
    "    k_max_ind = np.argmin(np.abs(k - k_max))\n",
    "    # print(k[k_hcut_ind])\n",
    "    # print(k[k_max_ind])\n",
    "    k_seg = k[k_hcut_ind:k_max_ind]\n",
    "    spec_seg = np.transpose(nrcs_spec[:, k_hcut_ind:k_max_ind])\n",
    "    # plt.figure()\n",
    "    # plt.plot(k_seg, spec_seg[:,0])\n",
    "    coefs = nppol.polyfit(k_max - k_seg, np.real(spec_seg),1)\n",
    "    noise_fl_2 = np.where(k_km > 0, k_km * coefs[1].reshape((nit, 1)), 0)\n",
    "    # print(coefs.shape)\n",
    "    print(\"Mean slope at high wavenumbers: %f\" % coefs[1].mean())\n",
    "    print(\"stdev: %f\" % np.std(coefs[1]))\n",
    "    print(\"Slope according to low-wavenumber plateau: %f\" % slope_1.mean())\n",
    "    print(\"stdev: %f\" % np.std(slope_1))\n",
    "    return noise_fl_1, noise_fl_2\n",
    "\n",
    "noise_fl_1, noise_fl_2 = spectrum_noise_floor(k_sim, normf * sar_int_spec, k_max, k_hcut=0.75)\n",
    "noise_fl = noise_fl_2\n",
    "m_sar_specdn = np.mean(np.real(normf * np.real(sar_int_spec[:, 0:nrg])) - noise_fl, axis=0)\n",
    "m_sar_specdn[0] = m_sar_specdn[1]\n",
    "s_sar_specdn = np.std(np.real(normf * np.real(sar_int_spec[:, 0:nrg])) - noise_fl, axis=0)[0:nrg]\n",
    "s_sar_specdn[0] = s_sar_specdn[1]\n",
    "\n",
    "noise_fl_1, noise_fl_2 = spectrum_noise_floor(k_sim, normfr * rar_int_spec, k_max, k_hcut=0.75)\n",
    "noise_fl = noise_fl_2\n",
    "m_rar_specdn = np.mean(np.real(normfr * np.real(rar_int_spec[:, 0:nrg])) - noise_fl, axis=0)\n",
    "m_rar_specdn[0] = m_rar_specdn[1]\n",
    "s_rar_specdn = np.std(np.real(normfr * np.real(rar_int_spec[:, 0:nrg])) - noise_fl, axis=0)[0:nrg]\n",
    "s_rar_specdn[0] = s_rar_specdn[1]\n",
    "\n",
    "plt.figure()\n",
    "plt.fill_between(k_sim, m_sar_specdn - s_sar_specdn,\n",
    "                 m_sar_specdn + s_sar_specdn, alpha=0.2, color='blue')\n",
    "plt.plot(k_sim, m_sar_specdn, color='blue')\n",
    "if plot_rar:\n",
    "    plt.fill_between(k_sim, m_rar_specdn - s_rar_specdn,\n",
    "                 m_rar_specdn + s_rar_specdn, alpha=0.2, color='green')\n",
    "    plt.plot(k_sim, m_rar_specdn, color='green')\n",
    "\n",
    "# plt.fill_between(k[1:nrg], I2_Nblk[1:nrg] - np.sqrt(var_I2[1:nrg]),\n",
    "#                  I2_Nblk[1:nrg] + np.sqrt(var_I2[1:nrg]), alpha=0.2, color='blue')\n",
    "# plt.plot(k[1:nrg], I2_Nblk[1:nrg])\n",
    "plt.xlabel('k [rad/m]')\n",
    "plt.ylim((0.001,1))\n",
    "plt.xlim(xlim)\n",
    "plt.xscale('log')\n",
    "# plt.yscale('log')\n",
    "plt.grid(True)\n",
    "plt.ylabel(\"$S_\\sigma(k)$\")\n",
    "figname = (\"nrcs_spec_denoised_%s.png\" % casestr)\n",
    "plt.savefig(os.path.join(fdir,figname))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "fstsmp = 6\n",
    "xlim = (k_sim[fstsmp], 1.0)\n",
    "plt.figure(figsize=(12,4))\n",
    "plt.subplot(1,2,1)\n",
    "plt.fill_between(k_sim[fstsmp:], m_sar_spec[fstsmp:] - s_sar_spec[fstsmp:],\n",
    "                 m_sar_spec[fstsmp:] + s_sar_spec[fstsmp:], alpha=0.2, color='blue')\n",
    "plt.plot(k_sim[fstsmp:], m_sar_spec[fstsmp:], color='blue', label='Doppler-resolved')\n",
    "if plot_rar:\n",
    "    plt.fill_between(k_sim, m_rar_spec - s_rar_spec,\n",
    "                 m_rar_spec + s_rar_spec, alpha=0.2, color='green')\n",
    "    plt.plot(k_sim, m_rar_spec, color='green', label='real-aperture')\n",
    "plt.xlabel('k [rad/m]')\n",
    "plt.ylabel('$S_I(k)$')\n",
    "plt.title('Biased')\n",
    "plt.ylim(ylim)\n",
    "plt.xlim(xlim)\n",
    "plt.xscale('log')\n",
    "# plt.yscale('log')\n",
    "plt.yscale('symlog', linthreshy=linthreshy)\n",
    "plt.grid(True)\n",
    "plt.legend(loc=0)\n",
    "plt.subplot(1,2,2)\n",
    "plt.fill_between(k_sim, m_sar_specdn - s_sar_specdn,\n",
    "                 m_sar_specdn + s_sar_specdn, alpha=0.2, color='blue')\n",
    "plt.plot(k_sim, m_sar_specdn, color='blue')\n",
    "if plot_rar:\n",
    "    plt.fill_between(k_sim, m_rar_specdn - s_rar_specdn,\n",
    "                 m_rar_specdn + s_rar_specdn, alpha=0.2, color='green')\n",
    "    plt.plot(k_sim, m_rar_specdn, color='green')\n",
    "plt.grid(True)\n",
    "plt.title('Corrected')\n",
    "# plt.legend(loc=0)\n",
    "plt.ylim(ylim)\n",
    "plt.xlim(xlim)\n",
    "plt.xlabel('k [rad/m]')\n",
    "plt.ylabel('$S_I(k)$')\n",
    "plt.xscale('log')\n",
    "    plt.yscale('symlog', linthreshy=linthreshy)\n",
    "plt.tight_layout()\n",
    "figname = (\"nrcs_spec_bias_nobias_%s.png\" % casestr)\n",
    "plt.savefig(os.path.join(fdir,figname))\n",
    "figname = (\"nrcs_spec_bias_nobias_%s.pdf\" % casestr)\n",
    "plt.savefig(os.path.join(fdir,figname))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Moments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ps = np.array([0, 1, 1.5, 2])\n",
    "k_ll = 2 * np.pi / 500 # 0.015\n",
    "k_ul = 2 * np.pi / 10 # 0.3\n",
    "print(k_ll)\n",
    "print(k_ul)\n",
    "ki_ll = np.argmin(np.abs(k_sim-k_ll))\n",
    "ki_ul = np.argmin(np.abs(k_sim-k_ul))\n",
    "spcs = np.real(sar_int_spec[:, ki_ll:ki_ul])\n",
    "spcs_rar = np.real(rar_int_spec[:, ki_ll:ki_ul])\n",
    "k_sim_int = k_sim[ki_ll:ki_ul]\n",
    "k_sim_int = k_sim_int.reshape((1, k_sim_int.size))\n",
    "dk_sim = k_sim[1] - k_sim[0]\n",
    "m_mom = []\n",
    "s_mom = []\n",
    "m_mom_rar = []\n",
    "s_mom_rar = []\n",
    "for p in ps:\n",
    "    mom_i = np.sum((k_sim_int**p) * spcs * dk_sim, axis=1)\n",
    "    m_mom.append(np.mean(mom_i))\n",
    "    s_mom.append(np.std(mom_i))\n",
    "    mom_i = np.sum((k_sim_int**p) * spcs_rar * dk_sim, axis=1)\n",
    "    m_mom_rar.append(np.mean(mom_i))\n",
    "    s_mom_rar.append(np.std(mom_i))\n",
    "m_mom = np.array(m_mom)\n",
    "s_mom = np.array(s_mom)\n",
    "m_mom_rar = np.array(m_mom_rar)\n",
    "s_mom_rar = np.array(s_mom_rar)\n",
    "if rad_az == 90:\n",
    "    m_mom_90 = np.copy(m_mom)\n",
    "    m_mom_rar_90 = np.copy(m_mom_rar)\n",
    "print(\"Doppler resolved\")\n",
    "print(m_mom_90)\n",
    "print(m_mom)\n",
    "print(s_mom)\n",
    "print(s_mom/m_mom_90)\n",
    "print(s_mom/m_mom)\n",
    "#print(s_mom/m_mom/np.sqrt(10))\n",
    "print(\"RAR\")\n",
    "print(m_mom_rar_90)\n",
    "print(m_mom_rar)\n",
    "print(s_mom_rar)\n",
    "print(s_mom_rar/m_mom_rar_90)\n",
    "print(s_mom_rar/m_mom_rar)\n",
    "#print(s_mom_rar/m_mom_rar/np.sqrt(10))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sar_pts_comp = False\n",
    "ylim2 = (0.00,0.05)\n",
    "xlim2 = (0.025, 1)\n",
    "if sar_pts_comp:\n",
    "    plt.figure(figsize=(12,4))\n",
    "    plt.subplot(1,2,1)\n",
    "    #plt.plot(k_sim, m_sar_spec, color='red')\n",
    "    plt.fill_between(k_sim, m_sar_spec_32 - s_sar_spec_32,\n",
    "                     m_sar_spec_32 + s_sar_spec_32, alpha=0.4, color='blue')\n",
    "    plt.plot(k_sim, m_sar_spec, color='blue')\n",
    "\n",
    "    plt.fill_between(k[1:nrg], I2_Nblk_32[1:nrg] - np.sqrt(var_I2_32[1:nrg]),\n",
    "                     I2_Nblk_32[1:nrg] + np.sqrt(var_I2_32[1:nrg]), alpha=0.3, color='red')\n",
    "    plt.plot(k[1:nrg], I2_Nblk_32[1:nrg], color='red')\n",
    "    plt.xlabel('k [rad/m]')\n",
    "    plt.ylabel('$S_I(k)$')\n",
    "    plt.ylim(ylim2)\n",
    "    plt.xlim(xlim2)\n",
    "    plt.xscale('log')\n",
    "    plt.title('$N_{Doppler\\,\\,proc}= 32$')\n",
    "    #plt.yscale('symlog', linthreshy=linthreshy)\n",
    "    # plt.ylim((0,0.1))\n",
    "    plt.grid(True)\n",
    "    plt.subplot(1,2,2)\n",
    "    #plt.plot(k_sim, m_sar_spec, color='red')\n",
    "    plt.fill_between(k_sim, m_sar_spec_64 - s_sar_spec_64,\n",
    "                     m_sar_spec_64 + s_sar_spec_64, alpha=0.4, color='blue')\n",
    "    plt.plot(k_sim, m_sar_spec, color='blue')\n",
    "\n",
    "    plt.fill_between(k[1:nrg], I2_Nblk_64[1:nrg] - np.sqrt(var_I2_64[1:nrg]),\n",
    "                     I2_Nblk_64[1:nrg] + np.sqrt(var_I2_64[1:nrg]), alpha=0.3, color='red')\n",
    "    plt.plot(k[1:nrg], I2_Nblk_64[1:nrg], color='red')\n",
    "    plt.xlabel('k [rad/m]')\n",
    "    plt.ylabel('$S_I(k)$')\n",
    "    plt.ylim(ylim2)\n",
    "    plt.xlim(xlim2)\n",
    "    plt.xscale('log')\n",
    "    plt.title('$N_{Doppler\\,\\,proc}= 64$')\n",
    "    #plt.yscale('symlog', linthreshy=linthreshy)\n",
    "    # plt.ylim((0,0.1))\n",
    "    plt.grid(True)\n",
    "    plt.tight_layout()\n",
    "    figname = (\"nrcs_spec_sar32_vs_sar64_%s.pdf\" % casestr)\n",
    "    plt.savefig(os.path.join(fdir,figname))\n",
    "    figname = (\"nrcs_spec_sar32_vs_sar64_%s.png\" % casestr)\n",
    "    plt.savefig(os.path.join(fdir,figname))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "position": {
    "height": "516px",
    "left": "2058px",
    "right": "20px",
    "top": "120px",
    "width": "350px"
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
