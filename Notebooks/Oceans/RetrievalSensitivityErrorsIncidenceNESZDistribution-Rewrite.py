# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Ocean Retrieval Performance
# In this notebook we compute the performance using the probability density function of the velocity error.

# +
# %matplotlib inline
import concurrent.futures
import time
from itertools import repeat, product
from pathlib import Path

import numpy as np
import scipy.stats as spstats
from matplotlib import pyplot as plt, rc
from scipy.integrate import simps

import stereoid.oceans.retrieval_performance as retrieval_perf
from drama.io import cfg

rc("text", usetex=True)
params = {
    "text.latex.preamble": r"\usepackage{siunitx}",
}
plt.rcParams.update(params)
# -

work_dir = Path("~/Documents/WORK/STEREOID/").expanduser()
data_dir = work_dir / "DATA" / "ScatteringModels" / "Oceans"
par_dir = work_dir / "PAR"
conf = cfg.ConfigFile(par_dir / "Hrmny_2020_1.cfg")


# ## Arriving at a the density function of the velocity errors
# The probability density function is computed by initialising a RetrievalPerformance object for each NESZ and incidence angle combination, at 350 km along-track separation and 9m ATI baseline. For each combination of NESZ and incidence angle the covariance matrices due to the mesurement noise $ \Sigma_{\sigma,m} $ and due to geophysical noise $ \Sigma_{\sigma,g} $ are summed to give the total covariance matrix $ \Sigma_{\sigma,n} $.
#
# The covariance matrices are computed for any given combination of the two wind velocity components $U_u$ and $U_v$. $ \Sigma_{\sigma,n} $ is used to define the joint distribution of the wind velocity error components given the wind velocity as:
# $$
# E_u, E_v \mid U_u=u_u, U_v= u_v \ \sim\ \mathcal{N}\left(\left(0, 0 \right)^T,\, \Sigma_{\sigma,n}\right).
# $$
#
# The distribution of wind speeds is assumed to follow a Weibull distribution:
# $$
#   f_{U_u, U_v}(u_u, u_v) = \frac{p_2}{p_1} \left(\frac{|\mathbf{u}|}{p_1} \right)^{p_2-1} e^{-\left(v/p_1 \right)^p_2}.
# $$
# where $p_2=2.2$ is the shape parameter and $p_1=10$ is the scale parameter. Then using the definition of conditional probability we obtain the joint density of the error and the velocity
# $$
# f_{E_u, E_v, U_u, U_v}(e_u, e_v, u_u, u_v) = f_{E_u, E_v \mid U_u, U_v}(e_u, e_v \mid u_u, u_v) \times f_{U_u, U_v}(u_u, u_v).
# $$
# Then by integrating over the domains of the two components of the velocity we obtain the (marginal) joint probability density of the errors.
# $$
# f_{E_u, E_v}(e_u, e_v) = \int_{-\infty}^{\infty}\int_{-\infty}^{\infty} f_{E_u, E_v, U_u, U_v}(e_u, e_v, u_u, u_v)\,\mathrm{d}u_u\,\mathrm{d}u_v.
# $$
# The probability of each error component can then be obtained by marginalising out the other error component.


def value_at_percentile(x, pdf, percentile):
    assert 0 <= percentile <= 100, "percentile must be between 0 and 100."
    percentile /= 100
    # normalization
    pdf /= simps(pdf, x)
    # print(simps(pdf, x)) # after normalization
    N = x.size
    # now compute integral cutting right limit down by one
    # with each iteration, stop as soon as we hit percentile
    integral = 1.0
    for k in range(0, N):
        if k == 0:
            xx = x
            xx_prev = x
            yy = pdf
        else:
            xx_prev = xx
            xx = x[0:-k]
            yy = pdf[0:-k]
        integral_prev = integral
        integral = simps(yy, xx)
        # print(f"Integral {k} from {xx[0]} to {xx[-1]} is equal to {integral}")
        if integral <= percentile:
            if np.abs(percentile - integral) < np.abs(percentile - integral_prev):
                value = xx[-1]
            else:
                value = xx_prev[-1]
            break
    return value


def wind_pdf(u, v, p1=10, p2=2.2):
    un = np.sqrt(u**2 + v**2)
    f_v = p2/p1 * (un / p1)**(p2-1) * np.exp(-(un/p1)**p2)
    return f_v / (2*np.pi) / un


def compute_wind_error_density(cov_matrix, wind_pdf, domain_grid, wind_domain):
    u_err, v_err = domain_grid
    pdf_domain = np.dstack((u_err, v_err))
    u_error_pdf = np.empty(u_err.shape[0])
    v_error_pdf = np.empty_like(u_error_pdf)
    joint_u_v_error_pdf = np.empty(wind_pdf.shape + u_err.shape)
    shpout = cov_matrix.shape[0:2]
    for i in range(shpout[0]):
        for j in range(shpout[1]):
            if not np.isnan(cov_matrix[i, j, :]).any():
                joint_u_v_error_pdf[i, j, :] = spstats.multivariate_normal.pdf(
                    pdf_domain, mean=None, cov=cov_matrix[i, j]
                )
            else:
                joint_u_v_error_pdf[i, j, :] = np.nan
    joint_error_and_wind_pdf = (
        wind_pdf[..., np.newaxis, np.newaxis] * joint_u_v_error_pdf
    )
    # integrate out the u and v component of the wind
    w_u, w_v = wind_domain
    joint_error_pdf = simps(
        simps(np.nan_to_num(joint_error_and_wind_pdf), w_u, axis=0),
        w_v,
        axis=0,
    )
    u_error_pdf = simps(joint_error_pdf, v_err[0], axis=1)
    v_error_pdf = simps(joint_error_pdf, u_err[:, 0], axis=0)
    return (u_error_pdf, v_error_pdf)


def percentile_map(nesz, inc, domain_grid, min_max_speed=None):
    swth_bst = geo.SingleSwathBistatic(par_file=par_dir / "Hrmny_2020_1.cfg", dau=350e3)
    obsgeo = ObsGeo.from_swath_geo(inc, swth_bst, ascending=True)
    la_v = geo.inc_to_look(np.radians(inc), 693e3)
    s1_nesz = NESZdata(la_v, inc, nesz, nesz, [0], conf, "IWS", 0)
    dual_nesz = NESZdata(la_v, inc_v, nesz + 3, nesz + 3, [0], conf, "IWS", 0)
    ati_nesz = NESZdata(la_v, inc_v, nesz + 6, nesz + 6, [0], conf, "IWS", 0)
    radar = RadarModel(obsgeo, sentinel_nesz=s1_nesz, dual_nesz=dual_nesz, ati_nesz=ati_nesz, b_ati=6)
    forward_model = FwdModel(work_dir / "DATA/ScatteringModels/Oceans", work_dir / "DATA/ScatteringModels/Oceans" / "C_band_isv_ocean_simulation.nc",
                             dspd=2, duvec=0.25, model="SSAlin", min_max_speed=min_max_speed)
    cov = L2Cov(radar, inc, forward_model)
    # print(f"Computing error pdfs for NESZ {nesz} and incident angle {inc}.")
    u_error_pdf, v_error_pdf = compute_wind_error_density(cov., domain_grid)
    u_err, v_err = domain_grid
    percentile_u = value_at_percentile(u_err[:, 0], u_error_pdf, 68.27)
    percentile_v = value_at_percentile(v_err[0, :], v_error_pdf, 68.27)
    print(f"Returning from NESZ {nesz} and incident angle {inc}.")
    return (percentile_u, percentile_v)


def call_percentile_map( arg1, arg2, arg3):
    return percentile_map(*arg1, arg2, arg3)


neszs = np.linspace(-15, -30, 16)
conf = cfg.ConfigFile(par_dir / "Hrmny_2020_1.cfg")
ninc = 16
incv = np.linspace(np.ceil(conf.IWS.inc_near[0]), np.floor(conf.IWS.inc_far[-1]), ninc)
domain = np.mgrid[-3:3:0.1, -3:3:0.1]
min_max_speed = None

# ThreadPoolExecutor can also be used. In testing, processees were found to be faster, presumably because the overhead of switching between threads is higher than the initial overhead of launching separate processes. Nevertheless, threads also provide a benefit since calls to numpy and scipy c-extensions release the GIL. Carefull, each process can take up to 2 GB!

start_time = time.time()
percentiles = np.empty((neszs.size, ninc, 2))
with concurrent.futures.ProcessPoolExecutor(max_workers=None) as executor:
    for i, error_pdf_tuple in zip(
        product(range(len(neszs)), range(len(incv))),
        executor.map(
            call_percentile_map,
            product(neszs, incv),
            repeat(domain),
            repeat(min_max_speed),
        ),
    ):
        percentiles[i[0], i[1], 0] = error_pdf_tuple[0]
        percentiles[i[0], i[1], 1] = error_pdf_tuple[1]
end_time = time.time()
print(f"ThreadPool method => {end_time-start_time} s")

u_fig, u_axs = plt.subplots(1, 2, figsize=(15, 8), sharey=True)
xlabel = "Incident angle $/\si{\degree}$"
ylabel = "NESZ $/\si{\decibel}$"
titles = (
    r"$1\sigma\, e_{V_u} / \si{\m \per \s}$",
    r"$1\sigma\, e_{V_V} / \si{\m \per \s}$",
)
levels = (6, 5)
for i, (title, ax) in enumerate(zip(titles, u_axs.flatten())):
    cf = ax.contourf(incv, neszs, percentiles[..., i], levels=levels[i])
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    u_fig.colorbar(cf, ax=ax)

u_fig.savefig(
    Path(f"~/Code/stereoid/Results/68_percentile_9m_ati_umin_{min_max_speed[0]}_umax_{min_max_speed[1]}_TSC_error_distribution.png"
    ).expanduser(),
    dpi=200,
)


# ## The rest of this notebook is for testing different ways of computing the same result

def plot_cov_wind(u_u, u_v, cov_w, vmax1=0.5):
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(12, 10))
    wcmps = ("U_u", "U_v")
    vmax = [[vmax1, 1], [1, vmax1]]
    vmin = [[0, -1], [-1, 0]]
    levels = [
        [np.linspace(0, vmax1, 21), np.linspace(-1, 1, 41)],
        [np.linspace(-1, 1, 41), np.linspace(0, vmax1, 21)],
    ]
    extnd = [["max", "neither"], ["neither", "max"]]

    cmps = [["Spectral", "seismic"], ["seismic", "Spectral"]]
    titles = [
        ["$\sigma_{U_u}$", r"$\rho_{U_uU_v}$"],
        [r"$\rho_{U_vU_u}$", "$\sigma_{U_v}$"],
    ]
    pdat = [
        [
            np.sqrt(cov_w[:, :, 0, 0]),
            cov_w[:, :, 0, 1] / np.sqrt(cov_w[:, :, 0, 0] * cov_w[:, :, 1, 1]),
        ],
        [
            cov_w[:, :, 1, 0] / np.sqrt(cov_w[:, :, 0, 0] * cov_w[:, :, 1, 1]),
            np.sqrt(cov_w[:, :, 1, 1]),
        ],
    ]

    for ind1 in range(2):
        for ind2 in range(2):
            # im = ax[ind1, ind2].imshow(pdat[ind1][ind2], origin='lower', extent=(u_min, u_max, u_min, u_max),
            #                           vmax=vmax[ind1][ind2], vmin=vmin[ind1][ind2], cmap=cmps[ind1][ind2])
            im = ax[ind1, ind2].contourf(
                u_u,
                u_v,
                pdat[ind1][ind2],
                cmap=cmps[ind1][ind2],
                levels=levels[ind1][ind2],
                vmax=vmax[ind1][ind2],
                vmin=vmin[ind1][ind2],
                extend=extnd[ind1][ind2],
            )
            ax[ind1, ind2].set_aspect("equal")
            fig.colorbar(im, ax=ax[ind1, ind2], fraction=0.046, pad=0.04)
            ax[ind1, ind2].set_title(titles[ind1][ind2])
            ax[ind1, ind2].set_xlabel("$U_u [m/s]$")
            ax[ind1, ind2].set_ylabel("$U_v [m/s]$")
    return fig


retr_perf = retrieval_perf.RetrievalPerformance(
    maindir=work_dir,
    run_id="2020_1",
    parfile=par_dir / "Hrmny_2020_1.cfg",
    d_at=350e3,
    b_ati=9,
    prod_res=2e3,
    mode="IWS",
    rx_ati_name="tud_2020_9m_half",
    rx_dual_name="tud_2020_dual9m",
    nesz_s1=None,
    nesz_full=neszs[0],
    nesz_ati=neszs[0] + 3,
    min_max_speed=min_max_speed,
)

cov_w_fig = plot_cov_wind(
    retr_perf.fwdm.w_u, retr_perf.fwdm.w_v, retr_perf.cov_wind_mg, vmax1=1
)

u_fig.savefig(
    Path(
        "~/Code/stereoid/Results/75_percentile_9m_ati_umin_12_distribution_parallel.png"
    ).expanduser(),
    dpi=200,
)


def performance_vs_inc(retr_perf, domain_grid, ninc=20):
    """Compute performace vs incident angle.

    Parameters
    ----------
    retr_perf : RetrievalPerformance
        The retrieval performance instance for whcih to compute the performance as a function of the incidence angle.
        
    domain_grid : tuple
        tuple of length two. The entries form the grid of values, as returned by np.mgrid, for the pdf to be evaluated at.

    ninc: int
        number of angles of incidence.

    Returns
    -------
    a tuple with the vector of incident angles and the covariance matrices of the errors
    """
    wind_pdf = retr_perf.wind_pdf()
    modecfg = getattr(retr_perf.conf, retr_perf.mode)
    incv = np.linspace(
        np.ceil(modecfg.inc_near[0]), np.floor(modecfg.inc_far[-1]), ninc
    )
    shpout = retr_perf.cov_wind_mg.shape[0:2]
    u_err, v_err = domain_grid
    pdf_domain = np.dstack((u_err, v_err))
    u_error_pdf = np.zeros((ninc, u_err.shape[0]))
    v_error_pdf = np.zeros_like(u_error_pdf)
    for incind in range(ninc):
        # print(incv[incind])
        retr_perf.inc_m = incv[incind]
        error_pdf = np.empty(wind_pdf.shape + u_err.shape)
        for i in range(shpout[0]):
            for j in range(shpout[1]):
                if not np.isnan(retr_perf.cov_wind_mg[i, j, :]).any():
                    error_pdf[i, j, :] = spstats.multivariate_normal.pdf(
                        pdf_domain, mean=None, cov=retr_perf.cov_wind_mg[i, j]
                    )
                else:
                    error_pdf[i, j, :] = np.nan
        joint_error_and_wind_pdf = wind_pdf[..., np.newaxis, np.newaxis] * error_pdf
        # integrate out the u and v component of the wind
        joint_error_pdf = simps(
            simps(np.nan_to_num(joint_error_and_wind_pdf).T, retr_perf.fwdm.w_u),
            retr_perf.fwdm.w_v,
        )
        v_error_pdf[incind] = simps(joint_error_pdf, u_err[:, 0])
        u_error_pdf[incind] = simps(joint_error_pdf.T, u_err[:, 0])
    return incv, u_error_pdf, v_error_pdf


def error_pdf_per_nesz(nesz, ninc, u_v_error):
    print(f"Instantiating retrieval performance with NESZ {nesz}.")
    ret_perf = retrieval_perf.RetrievalPerformance(
        maindir=work_dir,
        run_id="2020_1",
        parfile=par_dir / "Hrmny_2020_1.cfg",
        d_at=350e3,
        b_ati=9,
        prod_res=2e3,
        mode="IWS",
        rx_ati_name="tud_2020_9m_half",
        rx_dual_name="tud_2020_dual9m",
        nesz_s1=None,
        nesz_full=nesz,
        nesz_ati=nesz + 3,
        u_mag_min=None,
    )
    print(f"Computing error pdfs for NESZ {nesz}.")
    return performance_vs_inc(ret_perf, u_v_error, ninc)


start_time = time.time()
ninc = 10
u_err, v_err = np.mgrid[-3:3:0.1, -3:3:0.1]
percentiles = np.empty((neszs.size, ninc, 2))
with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    for i, error_pdf_tuple in enumerate(
        executor.map(error_pdf_per_nesz, neszs, repeat(ninc), repeat((u_err, v_err)))
    ):
        incv, u_error_pdf, v_error_pdf = error_pdf_tuple
        for inc_index, (u_pdf, v_pdf) in enumerate(zip(u_error_pdf, v_error_pdf)):
            # print(f"At incident angle {incv[inc_index]}, computing the percentiles.")
            percentiles[i, inc_index, 0] = value_at_percentile(u_err[:, 0], u_pdf, 75)
            percentiles[i, inc_index, 1] = value_at_percentile(u_err[:, 0], v_pdf, 75)
end_time = time.time()
print(f"ThreadPool second method => {(end_time-start_time)*1000} ms")

start_time = time.time()
ninc = 10
u_err, v_err = np.mgrid[-3:3:0.1, -3:3:0.1]
percentiles = np.empty((neszs.size, ninc, 2))
for i, nesz in enumerate(neszs):
    print(f"Instantiating retrieval performance with NESZ {nesz}.")
    ret_perf = retrieval_perf.RetrievalPerformance(
        maindir=work_dir,
        run_id="2020_1",
        parfile=par_dir / "Hrmny_2020_1.cfg",
        d_at=350e3,
        b_ati=9,
        prod_res=2e3,
        mode="IWS",
        rx_ati_name="tud_2020_9m_half",
        rx_dual_name="tud_2020_dual9m",
        nesz_s1=None,
        nesz_full=nesz,
        nesz_ati=nesz + 3,
        u_mag_min=None,
    )
    print(f"Computing error pdfs for {ninc} incident angles.")
    incv, u_error_pdf, v_error_pdf = performance_vs_inc(ret_perf, (u_err, v_err), ninc)
    for inc_index, (u_pdf, v_pdf) in enumerate(zip(u_error_pdf, v_error_pdf)):
        print(f"At incident angle {incv[inc_index]}, computing the percentiles.")
        percentiles[i, inc_index, 0] = value_at_percentile(u_err[:, 0], u_pdf, 75)
        percentiles[i, inc_index, 1] = value_at_percentile(u_err[:, 0], v_pdf, 75)
end_time = time.time()
print(f"Sequential method => {(end_time-start_time)*1000} ms")
