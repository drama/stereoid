# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Ocean Retrieval Performance

# +
# %matplotlib widget
from pathlib import Path

import numpy as np
from numba import njit, types, typed
import matplotlib.pyplot as plt
from matplotlib import rc

rc("text", usetex=True)
params = {
    "text.latex.preamble": r"\usepackage{siunitx}",
}
plt.rcParams.update(params)
import stereoid.oceans.retrieval_performance as retrieval_perf

# -

work_dir = Path("~/Documents/WORK/STEREOID/").expanduser()
data_dir = work_dir / "DATA" / "ScatteringModels" / "Oceans"
par_dir = work_dir / "PAR"


# ## Compute Retrieval Performance for $B_{\parallel} = 350 \ \mathrm{km}$

neszs = np.linspace(-15, -30, 16)

neszs

ninc = 20
# this is hard-coded in the RetrievalPerformance class, sigma_to_pdf takes an argument for nbins but calc() calls it with the defaults which is 100 and calc takes no argument for nbins. So it is hard-coded to 100
nbins = 100


def percentile_cdf(cdfs, p, axis_percentile):
    """percentile_cdf computes the pth percentile of the list of cdfs over the axis

    Parameters
    ----------
    cdfs: list/tuple
        collections of cdfs to compute the mean of

    p: float
        pth percentile

    axis_percentile: int
        the axis over which to compute the percentile

    Returns
    -------
    a list with the pth percentile of histograms
    """
    # these are the cdfs we expect:
    # umag_hists, udir_hists, tsc_u_hists, tsc_v_hists, rtsc_u_hists, rtsc_v_hists
    # pre-allocate
    # output = typed.List.empty_list(types.float64[:])
    output = np.zeros((len(cdfs), cdfs[0].shape[axis_percentile]))
    for i, cdf in enumerate(cdfs):
        percentiles = []
        for j in range(cdf.shape[axis_percentile]):
            percentiles.append((np.abs(cdf[j] - p)).argmin())
        output[i] = percentiles
    return output


# retrieval performance computes the histograms of 6 quantities
result = np.zeros((neszs.size, 6, ninc), dtype=np.int8)

min_speed = 20

for i, nesz in enumerate(neszs):
    ret_perf = retrieval_perf.RetrievalPerformance(
        maindir=work_dir,
        run_id="2020_1",
        parfile=par_dir / "Hrmny_2020_1.cfg",
        d_at=350e3,
        b_ati=9,
        prod_res=2e3,
        mode="IWS",
        rx_ati_name = 'tud_2020_9m_half',
        rx_dual_name = 'tud_2020_dual9m',
        nesz_s1=None,
        nesz_full=nesz,
        nesz_ati=nesz + 3,
        min_max_speed = (8, 20))
    incv, *cdfs = retrieval_perf.performance_vs_inc(ret_perf, ninc)
    percentiles_idx = percentile_cdf(cdfs, 0.75, 0)
    result[i] = percentiles_idx

result[:, 0, 1]

# +
xs = np.linspace(0, ret_perf.umag_err_max, 100)
xs = np.tile(xs, (neszs.size, ninc))
umag_p = xs[np.tile(range(xs.shape[0]), (20, 1)).T, result[:, 0, :]]

xs = np.linspace(0, ret_perf.udir_err_max, 100)
xs = np.tile(xs, (neszs.size, ninc))
udir_p = xs[np.tile(range(xs.shape[0]), (20, 1)).T, result[:, 1, :]]

xs = np.linspace(0, ret_perf.tscmag_err_max, 100)
xs = np.tile(xs, (neszs.size, ninc))
tsc_u_p = xs[np.tile(range(xs.shape[0]), (20, 1)).T, result[:, 2, :]]

tsc_v_p = xs[np.tile(range(xs.shape[0]), (20, 1)).T, result[:, 3, :]]

zs = (umag_p, udir_p, tsc_u_p, tsc_v_p)
# -

u_fig, u_ax = plt.subplots()
# xlabels = (r"$\sigma_{|U|} [m/s]$", r"$\sigma_{\angle U} [deg]$")
# ylabels = (r"$F_{\Sigma_{|U|}}(\sigma_{|U|})$", r"$F_{\Sigma_{\angle U}}(\sigma_{\angle U})$")
# ylabels = (r"$\sigma_{|U|} / \si{\m \per \s}$", r"$\sigma_{\angle U} / \si{\degree}$", r"$\sigma_{TSC_u} / \si{\m \per \s}$", r"$\sigma_{TSC_v} / \si{\m \per \s}$")
cf = u_ax.contourf(incv, neszs, umag_p)
xlabel = "Incident angle $/\si{\degree}$"
ylabel = "NESZ $[\si{\decibel}]$"
u_ax.set_xlabel(xlabel)
u_ax.set_ylabel(ylabel)
# ax.grid(True)
u_fig.colorbar(cf)
u_ax.set_title(r"75th percentile $\sigma_{|U|} / \si{\m \per \s}$")

u_fig, u_axs = plt.subplots(2, 2, figsize=(10, 8), sharex=True, sharey=False)
titles = (
    r"$\sigma_{|U|} / \si{\m \per \s}$",
    r"$\sigma_{\angle U} / \si{\degree}$",
    r"$\sigma_{TSC_u} / \si{\m \per \s}$",
    r"$\sigma_{TSC_v} / \si{\m \per \s}$",
)
ati = (False, False, True, True)
ylabels = ["NESZ Harmony $[\si{\decibel}]$"] * 2 + ["NESZ ATI $[\si{\decibel}]$"] * 2
for i, ax in enumerate(u_axs.flatten()):
    if ati[i]:
        neszs_to_plot = neszs + 3
    else:
        neszs_to_plot = neszs
    cf = ax.contourf(incv, neszs_to_plot, zs[i])
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabels[i])
    ax.set_title(titles[i])
    u_fig.colorbar(cf, ax=ax)
#u_fig.suptitle("75th percentile")

u_fig.savefig("/home/andreas/Code/stereoid_public/wind_and_tsc_histogram_8_min_20_max.png")


