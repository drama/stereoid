# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.6.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # SATRoSS/STEREOID ocean E2E performance model

# ## Imports

# +
# Imports
import os

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import rc
import numpy as np

import drama.geo as sargeo
import drama.utils as drtls
from stereoid.oceans import (
    RetrievalModel,
    ObsGeo,
    SceneGenerator,
    RadarModel,
    read_GoM_scenario,
    FwdModel,
)
import stereoid.sar_performance as strsarperf
import stereoid.utils.config as st_config
# -

# ## Models and simulation parameters
#
# The scene file is required. It is available on the project drive at `/staff-umbrella/harmony/DATA/OceanModels/GoM.mat`. `stereoid.utils.config` will, by default look into the `/Data` directory on the root level of the stereoid directory for data. If you would like to change this directory then create a file called `user.cfg` in `/PAR` and use `user_defaults.cfg` as a template. Change the data entry in the file to point to the absolute, or relative path of the data folder on your computer.

# ### Setting up the directories

paths = st_config.parse(section="Paths")
# Unpack the paths read from user.cfg. If user.cfg is not found user_defaults.cfg is used.
main_dir = paths["main"]
datadir = paths["data"]
pardir = paths["par"]
scn_file = "GoM.mat"
fwddir = os.path.join(datadir, "ScatteringModels/Oceans")
scndir = os.path.join(datadir, "Ocean/Scenarios")

# ### Setting up the radar parameters

run_id = "2020_1"
rx_ati_name = "tud_2020_half"
rx_dual_name = "tud_2020_dual6m"
mode = "IWS"
az_res_dct = {"WM": 5, "IWS": 20}
az_res = az_res_dct[mode]
b_ati = 9

# ### Read radar performance

parfile = os.path.join(pardir, ("Hrmny_%s.cfg" % run_id))
fstr_dual = strsarperf.sarperf_files(
    main_dir, rx_dual_name, mode=mode, runid=run_id, pardir=pardir
)
fstr_ati = strsarperf.sarperf_files(
    main_dir, rx_ati_name, mode=mode, runid=run_id, pardir=pardir
)
fstr_s1 = strsarperf.sarperf_files(
    main_dir, "sentinel", is_bistatic=False, mode=mode, runid=run_id, pardir=pardir
)

# ### Plot parameters

figsize_2x1 = (9, 4)
figsize_3x1 = (14, 4)
fontsize = 14
font = {"family": "Arial", "weight": "normal", "size": fontsize}
rc("font", **font)
plotdir = os.path.join(os.path.join(main_dir, "RESULTS/OceanE2E"), "GoM")
plotdir = os.path.join(plotdir, "%4.1f" % b_ati)
os.makedirs(plotdir, exist_ok=True)

# ## Instantiate forward model
# Forward model for NERCS, DCA, etc, stored in ncdf files. A class has been implemented to work with that.

# ### Forward model parameters

fname = "C_band_nrcs_dop_ocean_simulation.nc"
fnameisv = "C_band_isv_ocean_simulation.nc"
along_track_separation = 350e3

# The lookup tables are linearly interpolated to facilitate thee inversion
fwdm = FwdModel(
    fwddir, os.path.join(fwddir, fnameisv), dspd=2, duvec=0.5, model="SSAlin"
)
# fwdm.nrcs_crt.shape
fwdm.at_distance = along_track_separation

# ## Define observation geometry

# Incident angle
incm = 36
swth_bst = sargeo.SingleSwathBistatic(par_file=parfile, dau=along_track_separation)
# Observation geometry calculated from orbit
obsgeo = ObsGeo.from_swath_geo(incm, swth_bst, ascending=True)


# ## Instantiate scene generator and retrieval model
# Here we also read scene from Claudia

# +
# Image size, let us arbitrarily do (20,20)
tsc_o, wind, zeta, dx = read_GoM_scenario(os.path.join(scndir, scn_file), smp_out=1e3)
# Boost tsc
tsc_k = np.fft.fft(np.fft.fft(tsc_o, axis=0), axis=1)
kx = np.fft.fftfreq(tsc_o.shape[1], dx).reshape((1, tsc_o.shape[1]))
ky = np.fft.fftfreq(tsc_o.shape[0], dx).reshape((tsc_o.shape[0], 1))
k = np.sqrt(kx ** 2 + ky ** 2)
kc = 1 / 30e3
kn = k / kc
Hk = 1 + 0 * kn ** 2 / (1 + kn ** 2)
tsc_k = tsc_k * Hk[:, :, np.newaxis]
tsc_b = np.real(np.fft.ifft(np.fft.ifft(tsc_k, axis=0), axis=1))
tsc = tsc_b
grid_spacing = dx
obsgeo.set_swath(
    incm, np.arange(tsc.shape[1]).reshape((1, tsc.shape[1])) * grid_spacing
)
# Reduce mean wind because in this case it is too high for forward model
wind[:, :, 0] = wind[:, :, 0] - 2
# wind[:,:,1] = wind[:,:,1] - np.mean(wind[:,:,1]) * 0.6
print(np.mean(np.mean(wind, axis=0), axis=0))
dy = dx
xs = dx * np.arange(tsc.shape[1])
ys = dy * np.arange(tsc.shape[0])
imshp = tsc.shape[0:2]

# The sgm takes some default values for wind and wind direction,
# but we can pass a number or a matrix of an appropiate shape

sgm = SceneGenerator(
    fwdm,
    imshp,
    wspd=np.linalg.norm(wind, axis=2),
    wdir=np.degrees(np.arctan2(wind[:, :, 1], wind[:, :, 0])),
    cartesian=True,
    grid_spacing=grid_spacing,
)
sgm.tsc = tsc
retm = RetrievalModel(fwdm, obsgeo, grid_spacing=dx, cartesian=True)
plt.figure()
plt.imshow(wind[:, :, 0])
plt.colorbar()
# -

# ## Radar model
# Right now the radar model is a path through box (mostly a data class with only a few methods), although subcomponents are there

radarm = RadarModel(
    obsgeo, fstr_s1, fstr_dual, fstr_ati, az_res=az_res, prod_res=dx, b_ati=b_ati
)

# ## End to end run (in progress)
# Now we can run the E2E chain, i.e. connect the components. Not everythig is implemented...

s_nrcs, s_dca = sgm.l1(obsgeo)
s_isv = np.zeros_like(s_nrcs)
r_nrcs, r_dca, r_isv = radarm.add_errors(s_nrcs, s_dca, s_isv)
# Plotting
scene_size = r_nrcs.shape[0] * grid_spacing
extent = [0, scene_size / 1e3, 0, scene_size / 1e3]
smin = 0.000
smax = 0.02
fig, ax = plt.subplots(nrows=1, ncols=3, figsize=figsize_3x1)
im1 = ax[0].imshow(r_nrcs[:, :, 0], origin="lower", extent=extent, vmin=smin, vmax=smax)
# fig.colorbar(im1, ax=ax[0],fraction=0.046, pad=0.04)
ax[0].set_title("NRCS S1")
ax[0].set_ylabel("Azimuth [km]")
im2 = ax[1].imshow(r_nrcs[:, :, 1], origin="lower", extent=extent, vmin=smin, vmax=smax)
# fig.colorbar(im2, ax=ax[1],fraction=0.046, pad=0.04)
ax[1].set_title("NRCS Hrmny-A")
im3 = ax[2].imshow(r_nrcs[:, :, 2], origin="lower", extent=extent, vmin=smin, vmax=smax)
fig.colorbar(im3, ax=ax[2], fraction=0.046, pad=0.04, label="NRCS")
ax[2].set_title("NRCS Hrmny-B")
ax[0].set_xlabel("Ground range [km]")
ax[1].set_xlabel("Ground range [km]")
ax[2].set_xlabel("Ground range [km]")
plt.savefig(os.path.join(plotdir, "NRCS_meas_lin.png"))
smin = -28
smax = -16
fig, ax = plt.subplots(nrows=1, ncols=3, figsize=figsize_3x1)
im1 = ax[0].imshow(
    drtls.db(r_nrcs[:, :, 0]), origin="lower", extent=extent, vmin=smin, vmax=smax
)
# fig.colorbar(im1, ax=ax[0],fraction=0.046, pad=0.04)
ax[0].set_title("NRCS S1")
ax[0].set_ylabel("Azimuth [km]")
im2 = ax[1].imshow(
    drtls.db(r_nrcs[:, :, 1]), origin="lower", extent=extent, vmin=smin, vmax=smax
)
# fig.colorbar(im2, ax=ax[1],fraction=0.046, pad=0.04)
ax[1].set_title("NRCS Hrmny-A")
im3 = ax[2].imshow(
    drtls.db(r_nrcs[:, :, 2]), origin="lower", extent=extent, vmin=smin, vmax=smax
)
fig.colorbar(im3, ax=ax[2], fraction=0.046, pad=0.04, label="NRCS [dB]")
ax[2].set_title("NRCS Hrmny-B")
ax[0].set_xlabel("Ground range [km]")
ax[1].set_xlabel("Ground range [km]")
ax[2].set_xlabel("Ground range [km]")
plt.savefig(os.path.join(plotdir, "NRCS_meas_dB.png"))

w_u, w_v, dca_fwd = retm.retrieval_1(
    r_nrcs, 0, dir0=180, sigma_nrcs_db=0.2, window="hamming"
)

r_dca.shape
# obsgeo.bist_ang.shape
tscv, a, b = retm.tscv(r_dca, dca_fwd)
usv, a, b = retm.tscv(r_dca, 0 * dca_fwd)
tscv_noradar, a, b = retm.tscv(s_dca, dca_fwd)
tscv[np.isnan(tscv)] = 0
usv[np.isnan(usv)] = 0

# ### Estimate the wind vector

# Retrieved wind-vector
wind_ret = np.zeros_like(wind)
wspd_est = np.sqrt(w_u ** 2 + w_v ** 2)
wind_ret[:, :, 0] = w_u  # wspd_est * np.cos(np.radians(wdir_est))
wind_ret[:, :, 1] = w_v  # wspd_est * np.sin(np.radians(wdir_est))
# plt.figure(figsize=(16, 4.5))
plt.figure(figsize=(17, 5))
plt.subplot(1, 3, 1)
cnorm = matplotlib.colors.Normalize(
    vmin=np.max([sgm.wspd.min() - 2, 0]), vmax=sgm.wspd.max() + 2
)
strm_win = plt.streamplot(
    xs / 1e3,
    ys / 1e3,
    wind[:, :, 0],
    wind[:, :, 1],
    color=sgm.wspd,
    cmap="viridis_r",
    norm=cnorm,
)
plt.colorbar(strm_win.lines)
plt.xlabel("Range [km]")
plt.ylabel("Azimuth [km]")
plt.title("True $U_{10}$")
plt.subplot(1, 3, 2)
strm_wst = plt.streamplot(
    xs / 1e3,
    ys / 1e3,
    wind_ret[:, :, 0],
    wind_ret[:, :, 1],
    color=wspd_est,
    cmap="viridis_r",
    norm=cnorm,
)
plt.colorbar(strm_wst.lines)
plt.xlabel("Range [km]")
plt.title("Estimated $U_{10}$")
plt.subplot(1, 3, 3)
wind_ret_err = wind_ret - wind
norm_wind_ret_err = np.linalg.norm(wind_ret - wind, axis=-1)
cnorm = matplotlib.colors.Normalize(
    vmin=0, vmax=2 * np.mean(norm_wind_ret_err)
)  # + 4*np.std(norm_wind_ret_err))
strm_wer = plt.streamplot(
    xs / 1e3,
    ys / 1e3,
    wind_ret_err[:, :, 0],
    wind_ret_err[:, :, 1],
    color=norm_wind_ret_err,
    cmap="viridis_r",
    norm=cnorm,
)
plt.colorbar(strm_wer.lines)
plt.xlabel("Range [km]")
plt.title("$U_{10}$ error")
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "U10_stream.png"))
plt.savefig(os.path.join(plotdir, "U10_stream.svg"))

# ### Wind direction

# +
wdir_est = np.degrees(np.arctan2(w_v, w_u))

fig = plt.figure(figsize=(14, 4))
plt.subplot(1, 3, 1)
ax = plt.gca()
im = ax.imshow(drtls.db(r_nrcs[:, :, 0]), origin="lower")
ax.set_title("NRCS S1")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 3, 2)
ax = plt.gca()
im = ax.imshow(drtls.db(r_nrcs[:, :, 1]), origin="lower")
ax.set_title("NRCS STEREOID-A")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 3, 3)
ax = plt.gca()
im = ax.imshow(drtls.db(s_nrcs[:, :, 2]), origin="lower")
ax.set_title("NRCS STEREOID-B")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "NRCS_measured.png"))
plt.savefig(os.path.join(plotdir, "NRCS_measured.svg"))

plt.figure(figsize=(9, 4))
plt.subplot(1, 2, 1)

ax = plt.gca()
im = ax.imshow(
    sgm.wspd + np.zeros(imshp),
    origin="lower",
    vmin=sgm.wspd.min() - 0.1,
    vmax=sgm.wspd.max() + 0.1,
)
ax.set_title("Wind speed")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 2, 2)
ax = plt.gca()
im = ax.imshow(wspd_est, origin="lower")
ax.set_title("Estimated wind speed")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "SGM_Wind_field.png"))
plt.savefig(os.path.join(plotdir, "SGM_Wind_field.svg"))

plt.figure(figsize=(9, 4))
plt.subplot(1, 2, 1)
ax = plt.gca()
im = ax.imshow(
    sgm.wdir + np.zeros(imshp), origin="lower", cmap="hsv", vmin=-180, vmax=180
)
ax.set_title("Wind direction")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 2, 2)
ax = plt.gca()
im = ax.imshow(wdir_est, origin="lower", cmap="hsv", vmin=-180, vmax=180)
ax.set_title("Estimated wind direction")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "L2_Wind_field.png"))
plt.savefig(os.path.join(plotdir, "L2_Wind_field.svg"))


fig = plt.figure(figsize=(14, 4))
plt.subplot(1, 3, 3)
ax = plt.gca()
im = ax.imshow(r_dca[:, :, 0], origin="lower")
ax.set_title("S1 Doppler")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 3, 2)
ax = plt.gca()
im = ax.imshow(dca_fwd[:, :, 0], origin="lower")
ax.set_title("S1 Wind->Doppler")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 3, 1)
ax = plt.gca()
im = ax.imshow(r_dca[:, :, 0] - dca_fwd[:, :, 0], origin="lower")
ax.set_title("S1 Residual")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "S1_Dopplers.png"))
plt.savefig(os.path.join(plotdir, "S1_Dopplers.svg"))

fig = plt.figure(figsize=(14, 4))
plt.subplot(1, 3, 3)
ax = plt.gca()
im = ax.imshow(r_dca[:, :, 1], origin="lower")
ax.set_title("STEREOID-A Doppler")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 3, 2)
ax = plt.gca()
im = ax.imshow(dca_fwd[:, :, 1], origin="lower")
ax.set_title("STEREOID-A Wind->Doppler")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 3, 1)
ax = plt.gca()
im = ax.imshow(r_dca[:, :, 1] - dca_fwd[:, :, 1], origin="lower")
ax.set_title("STEREOID-A Residual")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "STR-A_Dopplers.png"))
plt.savefig(os.path.join(plotdir, "STR-A_Dopplers.svg"))

fig = plt.figure(figsize=(14, 4))
plt.subplot(1, 3, 3)
ax = plt.gca()
im = ax.imshow(r_dca[:, :, 2], origin="lower")
ax.set_title("STEREOID-B Doppler")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 3, 2)
ax = plt.gca()
im = ax.imshow(dca_fwd[:, :, 2], origin="lower")
ax.set_title("STEREOID-B Wind->Doppler")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 3, 1)
ax = plt.gca()
im = ax.imshow(r_dca[:, :, 2] - dca_fwd[:, :, 1], origin="lower")
ax.set_title("STEREOID-B Residual")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "STR-B_Dopplers.png"))
plt.savefig(os.path.join(plotdir, "STR-B_Dopplers.svg"))
# -

# ### Smooth the TSC

# +
# TSC stream-plot
from drama.utils.filtering import smooth1d


def smooth2d(data, s):
    return smooth1d(smooth1d(data, s, axis=0), s, axis=1)


tscf = smooth2d(tscv, 2)
plt.figure(figsize=(17, 5))
plt.subplot(1, 3, 1)
tsc_m = np.linalg.norm(tsc, axis=-1)
strn_tsc = plt.streamplot(
    xs / 1e3, ys / 1e3, tsc_b[:, :, 0], tsc_b[:, :, 1], color=tsc_m, cmap="viridis_r"
)
plt.colorbar(strn_tsc.lines)
plt.xlabel("Range [km]")
plt.ylabel("Azimuth [km]")
ax = plt.gca()
ax.set_title("TSC")
plt.subplot(1, 3, 2)
tscv_m = np.linalg.norm(tscf, axis=-1)
strm_tsc_est = plt.streamplot(
    xs / 1e3, ys / 1e3, tscf[:, :, 0], tscf[:, :, 1], color=tscv_m, cmap="viridis_r"
)
plt.colorbar(strm_tsc_est.lines)
plt.xlabel("Range [km]")
ax = plt.gca()
ax.set_title("Smoothed TSC")
plt.subplot(1, 3, 3)
dtsc_m = np.linalg.norm(tscf - tsc, axis=-1)
strm_tsc_err = plt.streamplot(
    xs / 1e3,
    ys / 1e3,
    tscf[:, :, 0] - tsc[:, :, 0],
    tscf[:, :, 1] - tsc[:, :, 1],
    color=dtsc_m,
    cmap="viridis_r",
)
plt.colorbar(strm_tsc_err.lines)
plt.xlabel("Range [km]")
plt.tight_layout()
ax = plt.gca()
ax.set_title("Smoothing error")
plt.savefig(os.path.join(plotdir, "TSC_stream.png"))
plt.savefig(os.path.join(plotdir, "TSC_stream.svg"))
# -

plt.figure(figsize=(9, 4))
plt.subplot(1, 2, 1)
ax = plt.gca()
im = ax.imshow(tscv[:, :, 0], origin="lower", cmap="bwr", vmin=-1, vmax=1)
ax.set_title("Cross-track TSC")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 2, 2)
ax = plt.gca()
im = ax.imshow(tscv[:, :, 1], origin="lower", cmap="bwr", vmin=-1, vmax=1)
ax.set_title("Along-track TSC")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "L2_TSC.png"))
plt.savefig(os.path.join(plotdir, "L2_TSC.svg"))

# # Denoising...

from skimage.restoration import (
    denoise_tv_chambolle,
    denoise_bilateral,
    denoise_tv_bregman,
    denoise_wavelet,
    estimate_sigma,
    denoise_nl_means,
)

# ### Filter selection
#
# Choose out of `{"nl", "bregman", "bilateral", "wavelet", "chambolle"}`. Or define your own filter.

dnmethod = "nl"

sigma_est = estimate_sigma(tscv[:, :, :], multichannel=True, average_sigmas=True)
print(sigma_est)
# tscv_dn = denoise_tv_chambolle(tscv, weight=0.15, multichannel=True)
if dnmethod == "nl":
    tscv_dn = denoise_nl_means(
        tscv, multichannel=True, h=0.14, patch_size=3, patch_distance=5
    )
    usv_dn = denoise_nl_means(
        usv, multichannel=True, h=0.14, patch_size=3, patch_distance=5
    )
elif dnmethod == "bregman":
    tscv_dn = np.zeros_like(tscv)
    usv_dn = np.zeros_like(tscv)
    weight = 3
    tscv_dn[:, :, 0] = denoise_tv_bregman(tscv[:, :, 0], weight, isotropic=False)
    tscv_dn[:, :, 1] = denoise_tv_bregman(tscv[:, :, 1], weight, isotropic=False)
    usv_dn[:, :, 0] = denoise_tv_bregman(usv[:, :, 0], weight, isotropic=False)
    usv_dn[:, :, 1] = denoise_tv_bregman(usv[:, :, 1], weight, isotropic=False)
elif dnmethod == "bilateral":
    sigma_spatial = 7
    wsizeu = 10
    wsizev = 15
    tscv_dn = np.zeros_like(tscv)
    usv_dn = np.zeros_like(tscv)
    tscv_dn[:, :, 0] = (
        denoise_bilateral(tscv[:, :, 0] + 10, wsizeu, sigma_spatial=sigma_spatial) - 10
    )  # [:,:,0] + 10)
    tscv_dn[:, :, 1] = (
        denoise_bilateral(tscv[:, :, 1] + 10, wsizev, sigma_spatial=sigma_spatial) - 10
    )  # [:,:,0] + 10- 10
    usv_dn[:, :, 0] = (
        denoise_bilateral(usv[:, :, 0] + 10, wsizeu, sigma_spatial=sigma_spatial) - 10
    )  # [:,:,0] + 10)
    usv_dn[:, :, 1] = (
        denoise_bilateral(usv[:, :, 1] + 10, wsizev, sigma_spatial=sigma_spatial) - 10
    )  # [:,:,0] + 10- 10
elif dnmethod == "wavelet":
    tscv_dn = denoise_wavelet(
        tscv, multichannel=True, rescale_sigma=False, wavelet="haar"
    )
    usv_dn = denoise_wavelet(
        usv, multichannel=True, rescale_sigma=False, wavelet="haar"
    )
elif dnmethod == "chambolle":
    tscv_dn = denoise_tv_chambolle(tscv, weight=0.15, multichannel=True)
    usv_dn = denoise_tv_chambolle(usv, weight=0.15, multichannel=True)

# After filtering, put the filtered tsc vector in a variable called tscv_dn and usv in usv_dn.

plt.figure(figsize=(9, 4))
plt.subplot(1, 2, 1)
ax = plt.gca()
im = ax.imshow(tscv_dn[:, :, 0], origin="lower", cmap="bwr", vmin=-1, vmax=1)
ax.set_title("Cross-track TSC")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 2, 2)
ax = plt.gca()
im = ax.imshow(tscv_dn[:, :, 1], origin="lower", cmap="bwr", vmin=-1, vmax=1)
ax.set_title("Along-track TSC")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "L2_TSC_dn.png"))
plt.savefig(os.path.join(plotdir, "L2_TSC_dn.svg"))

plt.figure(figsize=(18, 5))
plt.subplot(1, 3, 1)
tsc_m = np.linalg.norm(tsc, axis=-1)
strn_tsc = plt.streamplot(
    xs / 1e3, ys / 1e3, tsc[:, :, 0], tsc[:, :, 1], color=tsc_m, cmap="viridis_r"
)
plt.colorbar(strn_tsc.lines)
plt.xlabel("Range [km]")
plt.ylabel("Azimuth [km]")
plt.title("True TSC")
plt.subplot(1, 3, 2)
tscv_dn_m = np.linalg.norm(tscv_dn, axis=-1)
strm_tsc_est = plt.streamplot(
    xs / 1e3, ys / 1e3, tscf[:, :, 0], tscf[:, :, 1], color=tscv_dn_m, cmap="viridis_r"
)
plt.colorbar(strm_tsc_est.lines)
plt.xlabel("Range [km]")
# plt.ylabel('Azimuth [km]')
plt.title("Estimated TSC")
plt.subplot(1, 3, 3)
dtsc_m = np.linalg.norm(tscv_dn - tsc, axis=-1)
strm_tsc_err = plt.streamplot(
    xs / 1e3,
    ys / 1e3,
    tscv_dn[:, :, 0] - tsc[:, :, 0],
    tscf[:, :, 1] - tsc[:, :, 1],
    color=dtsc_m,
    cmap="viridis_r",
)
plt.colorbar(strm_tsc_err.lines)
plt.xlabel("Range [km]")
# plt.ylabel('Azimuth [km]')
plt.title("TSC error")
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "TSC_stream_dn.png"))
plt.savefig(os.path.join(plotdir, "TSC_stream_dn.svg"))

# ## TSC without Doppler estimation errors

# +
plt.figure(figsize=(9, 4))
plt.subplot(1, 2, 1)
ax = plt.gca()
im = ax.imshow(tscv_noradar[:, :, 0], origin="lower", cmap="bwr", vmin=-1, vmax=1)
ax.set_title("Cross-track TSC")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 2, 2)
ax = plt.gca()
im = ax.imshow(tscv_noradar[:, :, 1], origin="lower", cmap="bwr", vmin=-1, vmax=1)
ax.set_title("Along-track TSC")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "L2_TSCnoradar.png"))
plt.savefig(os.path.join(plotdir, "L2_TSCnoradar.svg"))

plt.figure(figsize=(9, 4))
plt.subplot(1, 2, 1)
ax = plt.gca()
im = ax.imshow(
    tscv_noradar[:, :, 0] - np.median(tscv_noradar[:, :, 0]),
    origin="lower",
    cmap="bwr",
    vmin=-1,
    vmax=1,
)
ax.set_title("Cross-track TSC")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(1, 2, 2)
ax = plt.gca()
im = ax.imshow(
    tscv_noradar[:, :, 1] - np.median(tscv_noradar[:, :, 1]),
    origin="lower",
    cmap="bwr",
    vmin=-1,
    vmax=1,
)
ax.set_title("Along-track TSC")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "L2_TSCnoradar_ac.png"))
plt.savefig(os.path.join(plotdir, "L2_TSCnoradar_ac.svg"))


# -

# # Vorticity
# The vorticity of the flow is computed as
# $$
# \omega = \nabla \times \overrightarrow{\mathrm{TSC}}
# $$
# i.e.
# $$
#  \omega = \frac{\partial}{\partial x} \mathrm{TSC}_v - \frac{\partial}{\partial y} \mathrm{TSC}_u
# $$
# The challenge in its estimation is that the derivatives are noisy. In the case of Harmony this affects in particular the $v$ component of TSC, which has an uncertaity in the order of 3 times worse than the $u$ component.
#
# This a grand challenge is to find the best possible estimator of the vorticity (or of the gradients leading to the vorticity) considering:
#
# - The anisotropy of the errors. For example one one tend to filter the $v$ component more than the $u$ component to find the best compromise between resolution and sensitivity.
# - The spatial structure of the $\omega$ and of the derivatives of the TSC field, which is smooth and tending to form line features.

# ### Function to denoise gradients

def denoise_gradient(
    dudx,
    dudy,
    dvdx,
    dvdy,
    method="bilateral",
    wsizeu=10,
    wsizev=15,
    sigma_spatial=7,
    wavelet="haar",
):
    from skimage.restoration import (
        denoise_tv_chambolle,
        denoise_bilateral,
        denoise_tv_bregman,
        denoise_wavelet,
        estimate_sigma,
        denoise_nl_means,
    )

    if method == "bilateral":
        dudx_dn = (
            denoise_bilateral(
                dudx - np.min(dudx) + 1, wsizeu, sigma_spatial=sigma_spatial
            )
            - 1
            + np.min(dudx)
        )
        dudy_dn = (
            denoise_bilateral(
                dudy - np.min(dudy) + 1, wsizeu, sigma_spatial=sigma_spatial
            )
            - 1
            + np.min(dudy)
        )
        dvdx_dn = (
            denoise_bilateral(
                dvdx - np.min(dvdx) + 1, wsizev, sigma_spatial=sigma_spatial
            )
            - 1
            + np.min(dvdx)
        )
        dvdy_dn = (
            denoise_bilateral(
                dvdy - np.min(dvdy) + 1, wsizev, sigma_spatial=sigma_spatial
            )
            - 1
            + np.min(dvdy)
        )
    elif method == "wavelet":
        grads = np.stack([dudx, dudy, dvdx, dvdy], axis=-1)
        grads_dn = denoise_wavelet(
            grads, multichannel=True, rescale_sigma=False, wavelet=wavelet
        )
        print(grads.shape)
        print(grads_dn.shape)
        dudx_dn = grads_dn[:, :, 0]
        dudy_dn = grads_dn[:, :, 1]
        dvdx_dn = grads_dn[:, :, 2]
        dvdy_dn = grads_dn[:, :, 3]

    return dudx_dn, dudy_dn, dvdx_dn, dvdy_dn


# ### Apply the denoising

# +
# Original
dvtsc_dy, dvtsc_dx = np.gradient(tsc[:, :, 1], dy, dx)
dutsc_dy, dutsc_dx = np.gradient(tsc[:, :, 0], dy, dx)
vort_tsc = dvtsc_dx - dutsc_dy
div_tsc = dutsc_dx + dvtsc_dy

dvusv_dy, dvusv_dx = np.gradient(usv[:, :, 1], dy, dx)
duusv_dy, duusv_dx = np.gradient(usv[:, :, 0], dy, dx)
vort_usv = dvusv_dx - duusv_dy
div_usv = duusv_dx + dvusv_dy

# Simulated
dvtscf_dy, dvtscf_dx = np.gradient(tscf[:, :, 1], dy, dx)
dutscf_dy, dutscf_dx = np.gradient(tscf[:, :, 0], dy, dx)
vort_tscf = dvtscf_dx - dutscf_dy
div_tscf = dutscf_dx + dvtscf_dy

(dutscf_dx_dng, dutscf_dy_dng, dvtscf_dx_dng, dvtscf_dy_dng) = denoise_gradient(
    dutscf_dx,
    dutscf_dy,
    dvtscf_dx,
    dvtscf_dy,
    method="bilateral",
    wsizeu=7,
    wsizev=20,
    sigma_spatial=10,
)
vort_tscf_dng = dvtscf_dx_dng - dutscf_dy_dng
# Denoised
dvtscv_dn_dy, dvtscv_dn_dx = np.gradient(tscv_dn[:, :, 1], dy, dx)
dutscv_dn_dy, dutscv_dn_dx = np.gradient(tscv_dn[:, :, 0], dy, dx)
vort_tscv_dn = dvtscv_dn_dx - dutscv_dn_dy
div_tscv_dn = dutscv_dn_dx + dvtscv_dn_dy

dvusv_dn_dy, dvusv_dn_dx = np.gradient(usv_dn[:, :, 1], dy, dx)
duusv_dn_dy, duusv_dn_dx = np.gradient(usv_dn[:, :, 0], dy, dx)
vort_usv_dn = dvusv_dn_dx - duusv_dn_dy
div_usv_dn = duusv_dn_dx + dvusv_dn_dy


# Without Doppler errors
dvtscf_noradar_dy, dvtscf_noradar_dx = np.gradient(tscv_noradar[:, :, 1], dy, dx)
dutscf_noradar_dy, dutscf_noradar_dx = np.gradient(tscv_noradar[:, :, 0], dy, dx)
vort_tscf_noradar = dvtscf_noradar_dx - dutscf_noradar_dy
div_tscf_noradar = dutscf_noradar_dx + dvtscf_noradar_dy
# -

# ### Plot the results

# +
res_out = 8
plt.figure(figsize=(15, 5))
plt.subplot(1, 3, 1)
# smooth factor
res_out = 3
ax = plt.gca()
im = ax.imshow(
    vort_tsc,
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(vort_tsc)),
    vmax=np.max(np.abs(vort_tsc)),
    cmap="bwr",
)
plt.colorbar(im, fraction=0.046, pad=0.04, format="%.0e")
plt.ylabel("Azimuth [km]")
plt.xlabel("Ground range [km]")
plt.title("TSC Vorticity")
ax.set_xlim((10, 120))
ax.set_ylim((10, 120))
plt.subplot(1, 3, 2)
ax = plt.gca()
im = ax.imshow(
    smooth2d(vort_tscf, res_out),
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(vort_tsc)),
    vmax=np.max(np.abs(vort_tsc)),
    cmap="bwr",
)
ax.set_title("Est. TSC Vorticity")
plt.colorbar(im, fraction=0.046, pad=0.04, format="%.0e")
plt.xlabel("Ground range [km]")
ax.set_xlim((10, 120))
ax.set_ylim((10, 120))
plt.subplot(1, 3, 3)
ax = plt.gca()
im = ax.imshow(
    smooth2d(vort_tscf_dng, res_out),
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(vort_tsc)),
    vmax=np.max(np.abs(vort_tsc)),
    cmap="bwr",
)
ax.set_title("Est. TSC Vorticity (DNG)")
plt.colorbar(im, fraction=0.046, pad=0.04, format="%.0e")
plt.xlabel("Ground range [km]")
ax.set_xlim((10, 120))
ax.set_ylim((10, 120))
plt.tight_layout()
# plt.savefig(os.path.join(plotdir, 'TSC_vorticity_1x3.png'))
# plt.savefig(os.path.join(plotdir, 'TSC_vorticity_1x3.svg'))


plt.figure(figsize=(15, 5))
plt.subplot(1, 3, 1)
# smooth factor
res_out = 4
ax = plt.gca()
im = ax.imshow(
    vort_tsc,
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(vort_tsc)),
    vmax=np.max(np.abs(vort_tsc)),
    cmap="bwr",
)
plt.colorbar(im, fraction=0.046, pad=0.04, format="%.0e")
plt.ylabel("Azimuth [km]")
plt.xlabel("Ground range [km]")
plt.title("TSC Vorticity")
ax.set_xlim((10, 120))
ax.set_ylim((10, 120))
plt.subplot(1, 3, 2)
ax = plt.gca()
im = ax.imshow(
    smooth2d(vort_usv_dn, res_out),
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(vort_tsc)),
    vmax=np.max(np.abs(vort_tsc)),
    cmap="bwr",
)
ax.set_title("Est. USV Vorticity")
plt.colorbar(im, fraction=0.046, pad=0.04, format="%.0e")
plt.xlabel("Ground range [km]")
ax.set_xlim((10, 120))
ax.set_ylim((10, 120))
plt.subplot(1, 3, 3)
ax = plt.gca()
im = ax.imshow(
    smooth2d(vort_tscv_dn, res_out),
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(vort_tsc)),
    vmax=np.max(np.abs(vort_tsc)),
    cmap="bwr",
)
ax.set_title("Est. TSC Vorticity")
plt.colorbar(im, fraction=0.046, pad=0.04, format="%.0e")
plt.xlabel("Ground range [km]")
ax.set_xlim((10, 120))
ax.set_ylim((10, 120))
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "TSC_vorticity_1x3.png"))
plt.savefig(os.path.join(plotdir, "TSC_vorticity_1x3.svg"))
# -

# # Gaussian filtered gradients

# +
from drama.utils.filtering import GradientFilter

gf = GradientFilter(grid_spacing, grid_spacing, 5e3, 5e3, edge_data=True)
gfu = GradientFilter(grid_spacing, grid_spacing, 5e3, 5e3, edge_data=True)
dutscfdx, dutscfdy = gfu.gradient(tscv[..., 0])
dvtscfdx, dvtscfdy = gf.gradient(tscv[..., 1])
dutscdx, dutscdy = gfu.gradient(tsc_b[..., 0])
dvtscdx, dvtscdy = gf.gradient(tsc_b[..., 1])
duusvdx, duusvdy = gfu.gradient(usv[..., 0])
dvusvdx, dvusvdy = gf.gradient(usv[..., 1])

gvort_tscf = dvtscfdx - dutscfdy
gdiv_tscf = dutscfdx + dvtscfdy
gvort_tsc = dvtscdx - dutscdy
gdiv_tsc = dutscdx + dvtscdy
gvort_usv = dvusvdx - duusvdy
gdiv_usv = duusvdx + dvusvdy

plt.figure(figsize=(15, 5))
plt.subplot(1, 3, 1)
# smooth factor
# res_out = 8

ax = plt.gca()
im = ax.imshow(
    gvort_tsc,
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(vort_tsc)),
    vmax=np.max(np.abs(vort_tsc)),
    cmap="bwr",
)
plt.colorbar(im, fraction=0.046, pad=0.04, format="%.0e")
plt.ylabel("Azimuth [km]")
plt.xlabel("Ground range [km]")
plt.title("TSC Vorticity")
ax.set_xlim((10, 120))
ax.set_ylim((10, 120))
plt.subplot(1, 3, 2)
ax = plt.gca()
im = ax.imshow(
    gvort_tscf,
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(vort_tsc)),
    vmax=np.max(np.abs(vort_tsc)),
    cmap="bwr",
)
ax.set_title("Est. TSC Vorticity")
plt.colorbar(im, fraction=0.046, pad=0.04, format="%.0e")
plt.xlabel("Ground range [km]")
ax.set_xlim((10, 120))
ax.set_ylim((10, 120))
plt.subplot(1, 3, 3)
ax = plt.gca()
im = ax.imshow(
    gvort_usv,
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(vort_tsc)),
    vmax=np.max(np.abs(vort_tsc)),
    cmap="bwr",
)
ax.set_title("Est. USV Vorticity")
plt.colorbar(im, fraction=0.046, pad=0.04, format="%.0e")
plt.xlabel("Ground range [km]")
ax.set_xlim((10, 120))
ax.set_ylim((10, 120))
plt.tight_layout()
# plt.savefig(os.path.join(plotdir, 'TSC_vorticity_1x3.png'))
# plt.savefig(os.path.join(plotdir, 'TSC_vorticity_1x3.svg'))


# +

plt.figure(figsize=(10, 10))
plt.subplot(2, 2, 1)
# smooth factor
res_out = 4

ax = plt.gca()
im = ax.imshow(
    drtls.smooth(div_tsc, res_out),
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(div_tsc)),
    vmax=np.max(np.abs(div_tsc)),
    cmap="bwr",
)
ax.set_title("TSC Divergence (True)")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.subplot(2, 2, 2)
ax = plt.gca()
im = ax.imshow(
    drtls.smooth(div_tscf, res_out),
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(div_tsc)),
    vmax=np.max(np.abs(div_tsc)),
    cmap="bwr",
)
ax.set_title("TSC Divergence (Estimated)")
plt.colorbar(im, fraction=0.046, pad=0.04)
# plt.tight_layout()

plt.subplot(2, 2, 3)
ax = plt.gca()
im = ax.imshow(
    drtls.smooth(div_tscv_dn, res_out),
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(div_tsc)),
    vmax=np.max(np.abs(div_tsc)),
    cmap="bwr",
)
ax.set_title("TSC Divergence (DN)")
plt.colorbar(im, fraction=0.046, pad=0.04)
# plt.tight_layout()

plt.subplot(2, 2, 4)
ax = plt.gca()
im = ax.imshow(
    drtls.smooth(div_usv_dn, res_out),
    origin="lower",
    extent=[xs[0] / 1e3, xs[-1] / 1e3, ys[0] / 1e3, ys[-1] / 1e3],
    vmin=-np.max(np.abs(div_tsc)),
    vmax=np.max(np.abs(div_tsc)),
    cmap="bwr",
)
ax.set_title("USV Divergence (DN)")
plt.colorbar(im, fraction=0.046, pad=0.04)
plt.tight_layout()
plt.savefig(os.path.join(plotdir, "TSC_divergence.png"))
plt.savefig(os.path.join(plotdir, "TSC_divergence.svg"))
