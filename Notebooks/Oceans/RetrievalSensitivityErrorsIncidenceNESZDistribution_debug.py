import concurrent.futures
import time
from itertools import repeat, product
from pathlib import Path

import numpy as np
import scipy.stats as spstats
from matplotlib import pyplot as plt, rc
from scipy.integrate import simps

import stereoid.oceans.retrieval_performance as retrieval_perf
from drama.io import cfg

# ## Arriving at a the density function of the velocity errors
# The probability density function is computed by initialising a RetrievalPerformance object for each NESZ and incidence angle combination, at 350 km along-track separation and 9m ATI baseline. For each combination of NESZ and incidence angle the covariance matrices due to the mesurement noise $ \Sigma_{\sigma,m} $ and due to geophysical noise $ \Sigma_{\sigma,g} $ are summed to give the total covariance matrix $ \Sigma_{\sigma,n} $.
#
# The covariance matrices are computed for any given combination of the two wind velocity components $U_u$ and $U_v$. $ \Sigma_{\sigma,n} $ is used to define the joint distribution of the wind velocity error components given the wind velocity as:
# $$
# E_u, E_v \mid U_u=u_u, U_v= u_v \ \sim\ \mathcal{N}\left(\left(0, 0 \right)^T,\, \Sigma_{\sigma,n}\right).
# $$
#
# The distribution of wind speeds is assumed to follow a Weibull distribution:
# $$
#   f_{U_u, U_v}(u_u, u_v) = \frac{p_2}{p_1} \left(\frac{|\mathbf{u}|}{p_1} \right)^{p_2-1} e^{-\left(v/p_1 \right)^p_2}.
# $$
# where $p_2=2.2$ is the shape parameter and $p_1=10$ is the scale parameter. Then using the definition of conditional probability we obtain the joint density of the error and the velocity
# $$
# f_{E_u, E_v, U_u, U_v}(e_u, e_v, u_u, u_v) = f_{E_u, E_v \mid U_u, U_v}(e_u, e_v \mid u_u, u_v) \times f_{U_u, U_v}(u_u, u_v).
# $$
# Then by integrating over the domains of the two components of the velocity we obtain the (marginal) joint probability density of the errors.
# $$
# f_{E_u, E_v}(e_u, e_v) = \int_{-\infty}^{\infty}\int_{-\infty}^{\infty} f_{E_u, E_v, U_u, U_v}(e_u, e_v, u_u, u_v)\,\mathrm{d}u_u\,\mathrm{d}u_v.
# $$
# The probability of each error component can then be obtained by marginalising out the other error component.


def value_at_percentile(x, pdf, percentile):
    assert 0 <= percentile <= 100, "percentile must be between 0 and 100."
    percentile /= 100
    # normalization
    pdf /= simps(pdf, x)
    # print(simps(pdf, x)) # after normalization
    N = x.size
    # now compute integral cutting right limit down by one
    # with each iteration, stop as soon as we hit percentile
    integral = 1.0
    for k in range(0, N):
        if k == 0:
            xx = x
            xx_prev = x
            yy = pdf
        else:
            xx_prev = xx
            xx = x[0:-k]
            yy = pdf[0:-k]
        integral_prev = integral
        integral = simps(yy, xx)
        # print(f"Integral {k} from {xx[0]} to {xx[-1]} is equal to {integral}")
        if integral <= percentile:
            if np.abs(percentile - integral) < np.abs(percentile - integral_prev):
                value = xx[-1]
            else:
                value = xx_prev[-1]
            break
    return value


def compute_wind_error_density(retr_perf, domain_grid):
    u_err, v_err = domain_grid
    pdf_domain = np.dstack((u_err, v_err))
    u_error_pdf = np.empty(u_err.shape[0])
    v_error_pdf = np.empty_like(u_error_pdf)
    wind_pdf = retr_perf.wind_pdf()
    joint_u_v_error_pdf = np.empty(wind_pdf.shape + u_err.shape)
    shpout = retr_perf.cov_wind_mg.shape[0:2]
    for i in range(shpout[0]):
        for j in range(shpout[1]):
            if not np.isnan(retr_perf.cov_wind_mg[i, j, :]).any():
                joint_u_v_error_pdf[i, j, :] = spstats.multivariate_normal.pdf(
                    pdf_domain, mean=None, cov=retr_perf.cov_tsc[i, j]
                )
            else:
                joint_u_v_error_pdf[i, j, :] = np.nan
    joint_error_and_wind_pdf = (
        wind_pdf[..., np.newaxis, np.newaxis] * joint_u_v_error_pdf
    )
    # integrate out the u and v component of the wind
    joint_error_pdf = simps(
        simps(np.nan_to_num(joint_error_and_wind_pdf), retr_perf.fwdm.w_u, axis=0),
        retr_perf.fwdm.w_v,
        axis=0,
    )
    u_error_pdf = simps(joint_error_pdf, v_err[0], axis=1)
    v_error_pdf = simps(joint_error_pdf, u_err[:, 0], axis=0)
    return (u_error_pdf, v_error_pdf)


def percentile_map(nesz, inc, domain_grid, min_max_speed=None):
    retr_perf = retrieval_perf.RetrievalPerformance(
        maindir=work_dir,
        run_id="2020_1",
        parfile=par_dir / "Hrmny_2020_1.cfg",
        d_at=350e3,
        b_ati=9,
        inc_m=inc,
        prod_res=2e3,
        mode="IWS",
        rx_ati_name="tud_2020_9m_half",
        rx_dual_name="tud_2020_dual9m",
        nesz_s1=None,
        nesz_full=nesz,
        nesz_ati=nesz + 3,
        min_max_speed=min_max_speed,
    )
    # print(f"Computing error pdfs for NESZ {nesz} and incident angle {inc}.")
    u_error_pdf, v_error_pdf = compute_wind_error_density(retr_perf, domain_grid)
    u_err, v_err = domain_grid
    percentile_u = value_at_percentile(u_err[:, 0], u_error_pdf, 68.27)
    percentile_v = value_at_percentile(v_err[0, :], v_error_pdf, 68.27)
    print(f"Returning from NESZ {nesz} and incident angle {inc}.")
    return (percentile_u, percentile_v)

def call_percentile_map( arg1, arg2, arg3):
    return percentile_map(*arg1, arg2, arg3)

if __name__ == '__main__':
    work_dir = Path("~/Documents/WORK/STEREOID/").expanduser()
    data_dir = work_dir / "DATA" / "ScatteringModels" / "Oceans"
    par_dir = work_dir / "PAR"
    
    neszs = np.linspace(-15, -30, 16)
    conf = cfg.ConfigFile(par_dir / "Hrmny_2020_1.cfg")
    ninc = 16
    incv = np.linspace(np.ceil(conf.IWS.inc_near[0]), np.floor(conf.IWS.inc_far[-1]), ninc)
    domain = np.mgrid[-3:3:0.1, -3:3:0.1]
    min_max_speed = (3, 6)

    start_time = time.time()
    percentiles = np.empty((neszs.size, ninc, 2))
    print("Starting processes")
    with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
        for i, error_pdf_tuple in zip(
            product(range(len(neszs)), range(len(incv))),
            map(
                call_percentile_map,
                product(neszs, incv),
                repeat(domain),
                repeat(min_max_speed),
            ),
        ):
            percentiles[i[0], i[1], 0] = error_pdf_tuple[0]
            percentiles[i[0], i[1], 1] = error_pdf_tuple[1]
    end_time = time.time()
    print(f"ThreadPool method => {end_time-start_time} s")
