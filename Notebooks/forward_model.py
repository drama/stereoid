# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Forward Model
#
# The forward model used in Harmony consists of a look-up table that relates values of wind speed and wind direction to the backscatter, in terms of the normalised radar cross-section (NRCS), and the Doppler received by the satellite for a given observation geometry. The observation geometry is specified by the angle of incidence of the signal that reaches the scatterer and the direction of the wind with respect to the incident signal.

# +
import numpy as np
import matplotlib.pyplot as plt

import drama.utils as drtls
import drama.geo as sargeo
import stereoid.sar_performance as strsarperf
from stereoid.oceans import FwdModel
from stereoid.instrument import ObsGeo, RadarModel
import stereoid.utils.config as st_config
# -

paths = st_config.parse(section="Paths")
data_dir = paths["data"] / 'ScatteringModels/Oceans'
fnameisv = 'C_band_isv_ocean_simulation.nc'
modelstr = 'SSAlin'

# ## Input Model
# We instantiate the LUT by choosing the model we would like to use to build it, in this case linear SSA. Several models are available including:
# * SSA linear
# * KA linear
# * WCA linear
# * Hybrid model using Yan Yuan's model for the NRCS and one of the models above for the Doppler.
# (citations for all the models? Paco?)
#
# To build the look-up table, we start with precomputed model results that were produced by running simulations using one of the supported models. The simulations are run for a specific scene, illuminator-companion separation and transmitter-receiver polarisation and their results are stored in netCDF files. 
#
# The LUT initialiser takes as input a string describing the desired model. It looks for the netCDF files that correspond to the specified model, and reads the files (there might be several corresponding to different polarisations and along-track separations). Before merging them, it ensures that all the datasets have the same dimensions and that their coordinates are aligned. In addition to the NRCS, the Doppler Centroid Anomaly is also extracted from the datasets and added to the LUT. Then the different datasets are merged into one in memory and separate arrays are constructed for each physical quantity of interest, e.g., incidence angle, NRCSS, incident azimuth.
#
# In the following section we will show an example of how to constrtuct the forward model using data from the SSA model at an angle of incidence of 35° and along-track separation of 350 km.

fwdm = FwdModel(data_dir, data_dir / fnameisv, dspd=2, duvec=0.25, model=modelstr)
fwdm.at_distance = 350e3
fwdm.inc = 35

# ### A note on the coordinates
# After instantiating the forward model we can access the NRCS for different satellites. The data from the model are in a polar coordinate basis. The wind is described in terms of speed magnitude and azimuth. To make the processing easier and on a regular grid we convert the LUT to a cartesian grid of cross and along-track wind velocities. The conversion is done by first creating a cartesian grid that fits all the points of the polar system and then by mapping values of NRCS and Doppler from polar to cartesian using cubic spline interpolation.

u_min = fwdm.w_u.min()
u_max = fwdm.w_u.max()
# set indicent angle
fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(15, 5))
im1 = ax[0].imshow(drtls.db(fwdm.nrcs_lut(1, cart=True)[0]), vmin=-20, vmax=-7,
                   origin='lower', extent=(u_min, u_max, u_min, u_max))
#fig.colorbar(im1, ax=ax[0],fraction=0.046, pad=0.04)
ax[0].set_title("$\sigma_{0,VV}$ Sentinel-1")
im2 = ax[1].imshow(drtls.db(fwdm.nrcs_lut(1, cart=True)[1]), vmin=-20, vmax=-7,
                   origin='lower', extent=(u_min, u_max, u_min, u_max))
#fig.colorbar(im2, ax=ax[1], fraction=0.046, pad=0.01, label="[dB]")
fig.colorbar(im2, ax=ax.ravel().tolist(), label="[dB]")
ax[1].set_title("$\sigma_{0,VP_s}$ Hrmny-A")
for ind in range(2):
    ax[ind].set_xlabel("$U_u [m/s]$")
    if ind == 0:
        ax[ind].set_ylabel("$U_v [m/s]$")
    ax[ind].set_aspect("equal")

# # Jacobian
# The link between the L1 nrcs and L2 surface stress uncertainty is established _mostly_ through the Jacobian of the multi-dimensional GMF, for example, for a single polarization and in Stereo configuration, the Jabobian of the GMF is given by
# $$J = \begin{pmatrix} \frac{\partial \sigma_{0,S1}}{\partial U_u} & \frac{\partial \sigma_{0,S1}}{\partial U_v} \\ \frac{\partial \sigma_{0,H-A}}{\partial U_u} & \frac{\partial \sigma_{0,H-A}}{\partial U_v} \\ 
# \frac{\partial \sigma_{0,H-B}}{\partial U_u} & \frac{\partial \sigma_{0,H-B}}{\partial U_v} \end{pmatrix}$$

## Jacobian of NRCS GMF and CDCA GMF for the incident angle specified
jac_n, jac_d = fwdm.fwd_jacobian(incm)
# the result is a n_sat x 2 pol x 2-D space x n_wind_v x n_wind_u matrix
# we can visualize a few gradients
fig, ax = plt.subplots(nrows=3, ncols=2, figsize=(12,14))
sats = ("S1", "H-A", "H-B")
wcmps = ("U_u", "U_v")
for ind1 in range(3):
    for ind2 in range(2):
        im = ax[ind1, ind2].imshow(jac_n[ind1, 1, ind2], origin='lower', extent=(u_min, u_max, u_min, u_max),
                                   cmap='bwr', vmin=-np.nanmax(np.abs(jac_n[0, 1, ind2])),
                                   vmax=np.nanmax(np.abs(jac_n[0, 1, ind2])))
        fig.colorbar(im, ax=ax[ind1, ind2],fraction=0.046, pad=0.04)
        ax[ind1, ind2].set_title("$\partial\sigma_{0,%s}/\partial %s$" % (sats[ind1], wcmps[ind2]))
        ax[ind1, ind2].set_xlabel("$U_u [m/s]$")
        ax[ind1, ind2].set_ylabel("$U_v [m/s]$")

# In addition to the NRCS from the geophysical model, we expect the data to have noise that will give rise to uncertainty. We assume that the covariance matrix of the level-1 uncertainty is the following sum
# $$
#   \Sigma_\sigma = \Sigma_{\sigma,n} + \Sigma_{\sigma, g} + \Sigma_{\sigma, s}
# $$
# of three terms:
# 1. random measurement noise, which is quickly suppressed if enough looks are available;
# 2. geophysical noise;
# 3. systematic errors, such as calibration errors.
#
# ## Measurement noise
#
# For the first two terms we follow [1]. The measurement noise will be described by
# $$
# \Sigma_{\sigma,n} = \begin{pmatrix} k_{n,S1}^2 \sigma_{0,S1}^2 & 0 & 0 \\
#                                      0 & k_{n,H-A}^2\sigma_{0,H-A}^2 & 0 \\
#                                      0 & 0 & k_{n,H-B}^2\sigma_{0,H-B}^2\end{pmatrix}.
# $$
# with 
# $$
#   k^2_{n} = \frac{1}{N_\mathrm{looks}}\left(1+\frac{1}{\mathrm{SNR}} \right)^2 + \frac{1}{N_\mathrm{noise-looks}\mathrm{SNR}^2}
# $$
#
# [1] Lin, Chung-Chi, Maurizio Betto, Maria Belmonte Rivas, Ad Stoffelen, and Jos de Kloe. “EPS-SG Windscatterometer Concept Tradeoffs and Wind Retrieval Performance Assessment.” IEEE Transactions on Geoscience and Remote Sensing 50, no. 7 (July 2012): 2458–72. https://doi.org/10.1109/TGRS.2011.2180393.
#

# To compute the mesurement noise we need to first calculate the SNR of the receivers. In the interest of time. the NESZ is precomputed. We can load it into memory and use it compute the SNR.

# +
incm = 35 # Monostatic incident angle
along_track_separation = 350e3
swth_bst = sargeo.SingleSwathBistatic(par_file=paths["par"] / "Hrmny_2020_1.cfg", dau=along_track_separation) 
obsgeo = ObsGeo.from_swath_geo(incm, swth_bst, ascending=True)
mode = 'IWS'  # Set observation mode
az_res_dct = {"WM":5, "IWS":20}
az_res = az_res_dct[mode]
rx_ati_name = 'tud_2020_half'  # name of system in parameter file
rx_dual_name = 'tud_2020_dual6m' # full system, will have 3 dB better NESZ, etc.

# read computed performance
main_dir = paths["main"]
par_dir = paths["par"]
run_id = "2020_1"
fstr_dual = strsarperf.sarperf_files(main_dir, rx_dual_name, mode=mode, runid=run_id, pardir=par_dir)
fstr_ati = strsarperf.sarperf_files(main_dir, rx_ati_name, mode=mode, runid=run_id, pardir=par_dir)
fstr_s1 = strsarperf.sarperf_files(main_dir, 'sentinel', is_bistatic=False, mode=mode, runid=run_id, pardir=par_dir)
radarm = RadarModel(obsgeo, fstr_s1, fstr_dual, fstr_ati, az_res=az_res, prod_res=2e3, b_ati=9) 
# -

# Now that we have loaded the NESZ of the receivers we can calculate the SNR and compute the diagonal entries of $\Sigma_{\sigma, n}$.

# +
# SNR determination
incind = np.abs(radarm.dual_nesz.inc_v - incm).argmin()
nesz_hrmny = np.mean(10**(radarm.dual_nesz.nesz[:, incind]/10))
nesz_S1 = np.mean(10**(radarm.s1_nesz.nesz[:, incind]/10))

snrs = (np.transpose(fwdm.nrcs_lut(1, cart=True),[1, 2, 0]) /
        np.array([nesz_S1, nesz_hrmny, nesz_hrmny]).reshape((1, 1, 3)))
wind_prod_res = 1e3
n_looks = wind_prod_res**2 / az_res_dct[mode] / 5
alpha_p = np.sqrt(1/n_looks * ((1+1 / snrs)**2 + 1 / snrs**2))
fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 5))
plt.subplots_adjust(bottom=0.25)
for ind1 in range(3):
    im = ax[ind1].imshow(alpha_p[:,:, ind1], origin='lower', extent=(u_min, u_max, u_min, u_max),
                         vmax = 0.015, vmin=0.005) # cmap='inferno') , vmin=0, vmax=3)
    if ind1 == 2:
        fig.colorbar(im, ax=ax[ind1],fraction=0.046, pad=0.04)
    #ax[ind1, ind2].set_title(titles[ind1][ind2])
    ax[ind1].set_xlabel("$U_u [m/s]$")
    if ind1 == 0:
        ax[ind1].set_ylabel("$U_v [m/s]$")
    
figtitle = r"Diagonal elements of $k_{n, i}$. "
plt.figtext(0.1, 0.01,figtitle, wrap=True, fontsize='large')
# -

j = jac_n[:, 1]   # n_sat x 2-D space x n_wind_v x n_wind_u matrix
# JˆH \cdot J
# we also transpose while we are at it
jhj = np.einsum("jimn,jkmn->mnik", j, j)
# Now pseudo inverse
jhi_i = np.linalg.inv(jhj)
j_pi = np.einsum("mnik,jkmn->mnij", jhi_i, j)

cov_s = np.zeros((j.shape[2], j.shape[3], j.shape[0], j.shape[0]))
fwdm.inc = incm
#alpha = (10**(0.25/10) - 1)
for ind in range(3):
    cov_s[:, :, ind, ind] = alpha_p[:,:, ind]**2 * fwdm.nrcs_lut(1, cart=True)[ind]**2
cov_w = np.einsum("mnik,mnkj,mnlj->mnil", j_pi, cov_s, j_pi)

# +
fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 5))
plt.subplots_adjust(bottom=0.25)
for ind1 in range(3):
    im = ax[ind1].imshow(cov_s[:,:, ind1, ind1], origin='lower', extent=(u_min, u_max, u_min, u_max)) # cmap='inferno') , vmin=0, vmax=3)
    if ind1 == 2:
        fig.colorbar(im, ax=ax[ind1],fraction=0.046, pad=0.04)
    #ax[ind1, ind2].set_title(titles[ind1][ind2])
    ax[ind1].set_xlabel("$U_u [m/s]$")
    if ind1 == 0:
        ax[ind1].set_ylabel("$U_v [m/s]$")
    
figtitle = r"Diagonal elements of $\Sigma_{\sigma,n}$. "
plt.figtext(0.1, 0.01,figtitle, wrap=True, fontsize='large')


# -

# ## Geophysical noise
#
# The geophysical noise is more complicated to model. In [1] the authors follow [2], where a model for the geophysical noise was derived, giving a NRCS dispersion approximated by 
# $$
#   k_g = 0.12 \cdot e^{-U/12}.
# $$
# at ASCAT resolutions.
# As discussed in the literature, several factors contribute to this geophysical noise:
# 1. Variability of the stress equivalent wind within the resolution cell of the product. Considering the wind as a random field, the instantaneous mean over an area is itself a random variable. The effect of this variability will depend on the product resolution (or size of the footprint, thinking in scatterometric terms), and on the scales over which the wind field varies. Since the spectral peak of the wind wavenumber spectrum is typically O(1km), at higher resolutions than that the effect of this variability drops.
# 2. Imperfect overlap between the NRCS measurements for different beams (colocation errors). In the case of Hrmony, given the high resolution of the SLC products, this effect should be small.
# 3. Temporal lag between observations of different views. This temporal lag is absent in the case of Harmony.
#
# Other effects would include, for example, modulations due to surfactants.
#
# In general, we will assume that for high resolution products, this geo-physical noise contributions are small enough to be ignored. An alternative take is to assume that all observed variability can be interpreted as a variability of the stress equivalent wind vector. 
#
# ### Geophysical noise correlation
#
# If some level of gephysial noise where to be considered, it is worth noting that this should be expected to be correlated for the different views (Sentinel-1, and Harmony A and B), as the observations are simultaneous and the azimuth diversity is limited. We postulate that the covariance matrix would have a form
# $$
# \Sigma_{\sigma,gn} = k_g^2 \begin{pmatrix}  \sigma_{0,S1}^2 & \cos \alpha_a\sigma_{0,S1}\sigma_{0,H-A} & \cos \alpha_a\sigma_{0,S1}\sigma_{0,H-B}  \\
#                                      \cos \alpha_a\sigma_{0,S1}\sigma_{0,H-A} & \sigma_{0,H-A}^2 & \cos (2\alpha_a)\sigma_{0,H-A}\sigma_{0,H-B} \\
#                                      \cos \alpha_a\sigma_{0,S1}\sigma_{0,H-B} & \cos (2\alpha_a)\sigma_{0,H-A}\sigma_{0,H-B} & \sigma_{0,H-B}^2\end{pmatrix}
# $$
#
# with $\alpha_a$ the effective ground-projected azimuh angle between the observations taken as half the azimuth-projected bistatic angle. This model makes the error fully correlated if the observation geometries are identical, as it should, and fully uncorrelated if the azimuth viewing directions are orthogonal. The latter is unrealistic, but relatively unimportant as the effective angles conseidered are smal, less than 25 degree.
#
# To illustrate the impact of geophysical noise under this assumption we have assumed 
# $$
#   k_g = 0.06 \cdot e^{-U/12},
# $$
# i.e. we assume that the disperssion of the NRCS is half that of ASCAT.
#
# [1] Lin, Chung-Chi, Maurizio Betto, Maria Belmonte Rivas, Ad Stoffelen, and Jos de Kloe. “EPS-SG Windscatterometer Concept Tradeoffs and Wind Retrieval Performance Assessment.” IEEE Transactions on Geoscience and Remote Sensing 50, no. 7 (July 2012): 2458–72. https://doi.org/10.1109/TGRS.2011.2180393.
#
# [2] Portabella, M., and A. Stoffelen. “Scatterometer Backscatter Uncertainty Due to Wind Variability.” IEEE Transactions on Geoscience and Remote Sensing 44, no. 11 (November 2006): 3356–62. https://doi.org/10.1109/TGRS.2006.877952.

# + jupyter={"source_hidden": true}
def plot_cov_wind(u_u, u_v, cov_w, vmax1=0.5):
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(12,10))
    wcmps = ("U_u", "U_v")
    vmax= [[vmax1, 1],[1, vmax1]]
    vmin = [[0, -1], [-1,0]]
    levels = [[np.linspace(0,vmax1,21), np.linspace(-1,1,41)], [np.linspace(-1,1,41), np.linspace(0,vmax1,21)]]
    extnd = [['max', 'neither'], ['neither', 'max']]
    
    cmps = [['Spectral','seismic'],['seismic', 'Spectral']]
    titles = [["$\sigma_{U_u}$", r"$\rho_{U_uU_v}$"], [r"$\rho_{U_vU_u}$", "$\sigma_{U_v}$"]]
    pdat = [[np.sqrt(cov_w[:, :, 0, 0]),  
             cov_w[:, :, 0, 1]/np.sqrt(cov_w[:, :, 0, 0]*cov_w[:, :, 1, 1])],
            [cov_w[:, :, 1, 0]/np.sqrt(cov_w[:, :, 0, 0]*cov_w[:, :, 1, 1]),
             np.sqrt(cov_w[:, :, 1, 1])]]

    for ind1 in range(2):
        for ind2 in range(2):
            #im = ax[ind1, ind2].imshow(pdat[ind1][ind2], origin='lower', extent=(u_min, u_max, u_min, u_max),
            #                           vmax=vmax[ind1][ind2], vmin=vmin[ind1][ind2], cmap=cmps[ind1][ind2])
            im = ax[ind1, ind2].contourf(u_u, u_v, pdat[ind1][ind2],
                                         cmap=cmps[ind1][ind2], levels=levels[ind1][ind2],
                                         vmax=vmax[ind1][ind2], vmin=vmin[ind1][ind2], 
                                         extend=extnd[ind1][ind2])
            ax[ind1, ind2].set_aspect("equal")
            fig.colorbar(im, ax=ax[ind1, ind2],fraction=0.046, pad=0.04)
            ax[ind1, ind2].set_title(titles[ind1][ind2])
            ax[ind1, ind2].set_xlabel("$U_u [m/s]$")
            ax[ind1, ind2].set_ylabel("$U_v [m/s]$")
    return fig


# +
alpha_a = np.radians(obsgeo.bist_ang/2)
cov_g = np.zeros((j.shape[2], j.shape[3], j.shape[0], j.shape[0]))
fwdm.inc = incm
u_u = fwdm.w_u.reshape((1, fwdm.w_u.size))
u_v = fwdm.w_v.reshape((fwdm.w_v.size, 1))
u_mag = np.sqrt(u_v**2 + u_u**2)
k_g = 0.04 * np.exp(-u_mag/12)
for ind in range(3):
    cov_g[:, :, ind, ind] = fwdm.nrcs_lut(1, cart=True)[ind]**2
cov_g[:, :, 0, 1] = fwdm.nrcs_lut(1, cart=True)[0] * fwdm.nrcs_lut(1, cart=True)[1] * np.cos(alpha_a)
cov_g[:, :, 1, 0] = cov_g[:, :, 0, 1]
cov_g[:, :, 0, 2] = fwdm.nrcs_lut(1, cart=True)[0] * fwdm.nrcs_lut(1, cart=True)[2] * np.cos(alpha_a)
cov_g[:, :, 2, 0] = cov_g[:, :, 0, 2]
cov_g[:, :, 1, 2] = fwdm.nrcs_lut(1, cart=True)[1] * fwdm.nrcs_lut(1, cart=True)[2] * np.cos(2*alpha_a)
cov_g[:, :, 2, 1] = cov_g[:, :, 1, 2] 
cov_g = k_g.reshape(u_mag.shape + (1,1))**2 * cov_g
cov_wg = np.einsum("mnik,mnkj,mnlj->mnil", j_pi, cov_g, j_pi)

cov_w_fig = plot_cov_wind(fwdm.w_u, fwdm.w_v, cov_wg, vmax1=1)
figtitle = "Diagonal panels: standard deviation of retrieved wind component uncertainties due to geophysical noise. \
 \nOff-diagonal: correlation coefficients between the errors."

plt.figtext(0.1, 0.03,figtitle, wrap=True, fontsize='large')

# -

# We can now sum together the two covariance matrices to compute the total covariance.

total_cov = cov_wg + cov_w
cov_w_fig = plot_cov_wind(fwdm.w_u, fwdm.w_v, total_cov, vmax1=1)


