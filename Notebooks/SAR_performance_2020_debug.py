# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] ein.tags="worksheet-0" slideshow={"slide_type": "-"}
# # Calculate and visualize Harmony's imaging performance

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": false} slideshow={"slide_type": "-"}
import os

import numpy as np
import matplotlib.pyplot as plt

from drama.io import cfg
from drama.performance.sar import calc_aasr, calc_nesz, RASR, RASRdata, pattern, AASRdata, NESZdata, SARModeFromCfg
from drama.performance.sar.azimuth_performance import mode_from_conf
import drama.geo as geo
from drama.geo.geo_history import GeoHistory
import stereoid.oceans as strocs
import stereoid.utils.config as st_config

# + [markdown] ein.tags="worksheet-0" slideshow={"slide_type": "-"}
# ## Set up the directories and simulation parametrs
# main_dir should point to the root directory of the stereoid package. Possible values for mode are "stripmap", "IWS","sliding_spotlight", "EW" and "WM".

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": false} slideshow={"slide_type": "-"}
# General setup
paths = st_config.parse(section="Paths")
main_dir = paths["main"]

datadir = paths["data"]
patterdir = datadir / "PATTERNS"

rx_ati_name = 'tud_2020_half'
rx_dual_name = 'tud_2020_dual6m'
rxname = rx_ati_name
txname = 'sentinel'
# Next two lines are for Sentinel-1 performance
# rxname = 'sentinel' 
# For companions, is_bistatic should be True
is_bistatic = True
runid = '2020_1'

pardir = paths["par"]
pltdirr = paths["results"] / "Activation" / runid
parfile = pardir / 'Hrmny_2020_1.cfg'
conf = cfg.ConfigFile(parfile)
# extract relevant info from conf
rxcnf = getattr(conf, rxname)
txcnf = getattr(conf, txname)
if is_bistatic:
    dau_km = int(conf.formation_primary.dau[0] / 1e3)
else:
    dau_km = int(0)
dau_str = ("%ikm" % dau_km)
indstr = rxname
sysid = indstr # ("%s_%3.2fm" % (indstr, b_at))
if rxcnf.DBF:
    sysid = sysid + "_DBF"
    
if rxcnf.SCORE:
    sysid = sysid + "_SCORE"

savedirr = paths["results"] / "SARPERF" / sysid
savedirr = savedirr / dau_str
mode = "IWS"
Nswth = 3
n_az_pts = 11
inc_range=[10,45]
(incs, PRFs, proc_bw,
 steering_rates,
 burst_lengths, short_name, proc_tap, tau_p, bw_p) = mode_from_conf(conf, mode)

# + [markdown] ein.hycell=false ein.tags="worksheet-0" slideshow={"slide_type": "-"}
# # Set up Sentinel-1 parameters

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": false} slideshow={"slide_type": "-"}
# S1 setup
rx_S1 = "sentinel"
tx_S1 = "sentinel"
# extract relevant info from conf
rxcnf = getattr(conf, rx_S1)
txcnf = getattr(conf, tx_S1)
dau_km_S1 = int(0)
dau_str = ("%ikm" % dau_km_S1)
indstr = rx_S1
sysid = indstr
if rxcnf.DBF:
    sysid = sysid + "_DBF"
    
if rxcnf.SCORE:
    sysid = sysid + "_SCORE"

savedirr_S1 = paths["results"] / "SARPERF" / sysid / dau_str


do_neszs = True
neszs = []
nesz_files = []
for swth in range(Nswth):
    modeandswth = ("%s_sw%i" % (short_name, swth + 1))
    modedir = os.path.join(savedirr, modeandswth)
    nesz_files.append(os.path.join(modedir,'nesz.p'))
    if do_neszs:
        nesz_ = calc_nesz(conf, mode, swth, txname='sentinel',
                          rxname=rx_dual_name,
                          savedirr=savedirr,
                          t_in_bs=None,
                          n_az_pts=n_az_pts,
                          extra_losses=rxcnf.L,
                          Tanalysis=10,
                          az_sampling=200, bistatic=is_bistatic)
        nesz_.save(os.path.join(modedir,'nesz.p'))
    else:
        nesz_ = NESZdata.from_file(os.path.join(modedir,'nesz.p'))
    neszs.append(nesz_)

nesz = NESZdata.from_filelist(nesz_files)
nesz.save(os.path.join(savedirr, "%s_NESZ.p" % short_name))
nesz = NESZdata.from_file(os.path.join(savedirr, "%s_NESZ.p" % short_name))

# + [markdown] ein.tags="worksheet-0" slideshow={"slide_type": "-"}
# # RASR

# + [markdown] ein.hycell=false ein.tags="worksheet-0" slideshow={"slide_type": "-"}
# ## Sentinel-1

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": true} slideshow={"slide_type": "-"}
do_rasrs_S1 = True
rasrs_S1 = []
rasrs_files_S1 = []
for swth in range(0, Nswth):
    modeandswth = ("%s_sw%i" % (short_name, swth + 1))
    modedir_S1 = os.path.join(savedirr_S1, modeandswth)
    rasrs_files_S1.append(os.path.join(modedir_S1,'rasr.p'))
    if do_rasrs_S1:
        rasr_ = RASR(conf, mode, swth, txname=tx_S1,
                     rxname=rx_S1,
                     savedirr=savedirr_S1,
                     t_in_bs=None,
                     n_az_pts=n_az_pts, n_amb_az=1, Tanalysis=5,
                     az_sampling=100, bistatic=False, verbosity=1)
        rasr_.save(os.path.join(modedir_S1,'rasr.p'))
    else:
        rasr_ = RASRdata.from_file(os.path.join(modedir_S1,'rasr.p'))
    rasrs_S1.append(rasr_)

rasr_S1 = RASRdata.from_filelist(rasrs_files_S1)
rasr_S1.save(os.path.join(savedirr_S1, "%s_RASR.p" % short_name))
rasr_S1 = RASRdata.from_file(os.path.join(savedirr_S1, "%s_RASR.p" % short_name))

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": true} slideshow={"slide_type": "-"}
rasr_S1.calc_rasr()
print(rasr_S1.rasr_parcial.shape)
plt.figure()
plt.plot(rasr_S1.inc_v, 10*np.log10(rasr_S1.rasr_total[0]))
plt.grid(True)

# + [markdown] ein.hycell=false ein.tags="worksheet-0" slideshow={"slide_type": "-"}
# ## Harmony

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": true} slideshow={"slide_type": "-"}
do_rasrs = True
rasrs = []
rasr_files = []
for swth in range(0, Nswth):
    modeandswth = ("%s_sw%i" % (short_name, swth + 1))
    modedir = os.path.join(savedirr, modeandswth)
    rasr_files.append(os.path.join(modedir,'rasr.p'))
    if do_rasrs:
        rasr_ = RASR(conf, mode, swth, txname='sentinel',
                     rxname=rxname,
                     savedirr=savedirr,
                     t_in_bs=None,
                     Namb=1,
                     n_az_pts=n_az_pts, n_amb_az=5, Tanalysis=10,
                     az_sampling=100, bistatic=is_bistatic, verbosity=1)
        rasr_.save(os.path.join(modedir,'rasr.p'))
    else:
        rasr_ = RASRdata.from_file(os.path.join(modedir,'rasr.p'))
    rasrs.append(rasr_)

rasr = RASRdata.from_filelist(rasr_files)
rasr.save(os.path.join(savedirr, "%s_RASR.p" % short_name))
rasr = RASRdata.from_file(os.path.join(savedirr, "%s_RASR.p" % short_name))

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": true} slideshow={"slide_type": "-"}
print(rasr_files)
rasr.calc_rasr()
print(rasr.rasr_parcial.shape)
plt.figure()
plt.plot(rasr.inc_v, 10*np.log10(np.transpose(rasr.rasr_parcial[5,:,0,:])))
plt.grid(True)

# + [markdown] ein.tags="worksheet-0" slideshow={"slide_type": "-"}
# # Doppler uncertainties
# The following cells do not really belong in this Notebook other than as an example of use of the SAR performances.

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": true} slideshow={"slide_type": "-"}
ati_perf = strocs.ATIPerf(nesz, 6, 2e3)

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": true} slideshow={"slide_type": "-"}
nesz.plot_nesz()
inc = nesz.inc_v[1:]
plt.figure()
plt.plot(inc, ati_perf.sigma_vdop(inc, -20))
plt.grid(True)

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": true} slideshow={"slide_type": "-"}
dca_perf = strocs.DCAPerf(nesz, 6, 1e3, tx_name='sentinel', rx_name='sentinel')

# + ein.hycell=false ein.tags="worksheet-0" pycharm={"is_executing": true} slideshow={"slide_type": "-"}
plt.figure()
plt.plot(inc, dca_perf.sigma_vdop(inc, -10 + np.zeros_like(inc)))
plt.grid(True)
