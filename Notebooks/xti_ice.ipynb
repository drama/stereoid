{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Cross-track interferometry\n",
    "\n",
    "This notebook discusses the performance of Harmony's across-track interferometry. Height sensitivity in SAR is achieved by using acquisitions of the same scene separate by an across-track distance. In side-looking monostatic systems, the two-way range difference between can directly be coupled to surface elevation. For Harmony the same is true, except that it only depends on a single-way range difference as there is only one transmitter and two receivers orbit in a cross-track formation. The interferometric phase difference between two images of the same scene obtained from antennas $A_1$ and $A_2$ is\n",
    "$$\n",
    "\\phi = \\frac{2 \\pi p}{\\lambda} ({r_2} - {r_1}),\n",
    "$$\n",
    "where $p = 2$ for \"ping-pong\" systems and $p = 1$ for standard systems, and $r$ is the range from the target pixel to the antenna and the subscripts refer to the antenna number. Let $\\vec{B}$ denote the baseline vector from antenna 1 to antenna 2, and vector $\\hat{l}_i$ denote the line of sight unit vector for $i = 1,2$. The expression for the phase difference can be simplified if $\\|\\vec{B}\\| \\ll \\rho$\n",
    "$$\n",
    "\\phi \\approx - \\frac{2 \\pi p}{\\lambda} \\left< \\hat{l}_1, \\vec{B} \\right>.\n",
    "$$\n",
    "The equation relating the scatterer position vector $\\vec{T}$, a reference position for the platform $\\vec{P}$, and the look vector is\n",
    "$$\n",
    "\\vec{T} = \\vec{P} + r \\hat{l}.\n",
    "$$\n",
    "Given knowledge of $\\vec{P}$ and $r$, heigh retrieval amounts to determining the unit vector $\\hat{l}$ from the measurement of the interferometric phase. In the two-dimensional case, where the baselines lies in the plane of the line-of-sight vector and the nadir, $\\vec{B} = \\left(B \\cos\\alpha, B \\sin\\alpha \\right)^T$, where $\\alpha$, is the angle between the baseline and the horizontal plane and the phase gives the familiar\n",
    "$$\n",
    "\\phi = - \\frac{2 \\pi p}{\\lambda} B \\sin(\\theta - \\alpha),\n",
    "$$\n",
    "where $\\theta$ is the angle the line-of-sight vector makes with the nadir. Thus, measurement of $\\phi$ allows the computation of $\\theta$ which can be used to solve for $\\vec{T}$. The height of ambiguity $h_a$ is\n",
    "$$\n",
    "h_a = \\frac{\\lambda r \\sin \\theta}{p \\|\\vec{B}\\| \\cos(\\theta - \\alpha)}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import the required packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "\n",
    "from pathlib import Path\n",
    "from collections import namedtuple\n",
    "\n",
    "import matplotlib\n",
    "from matplotlib import animation\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from IPython.display import HTML\n",
    "\n",
    "\n",
    "from drama.io import cfg\n",
    "from drama.mission.timeline import FormationTimeline\n",
    "from stereoid.utils.plot_helpers import format_func, contour_figure, create_plot_title\n",
    "\n",
    "matplotlib.rc(\"text\", usetex=True)\n",
    "params = {\n",
    "    \"text.latex.preamble\": [r\"\\usepackage{siunitx}\"],\n",
    "    \"font.size\": 14,\n",
    "    \"legend.fontsize\": \"x-small\",\n",
    "    \"legend.handlelength\": 2,\n",
    "    \"savefig.dpi\": 200,\n",
    "}\n",
    "plt.rcParams.update(params)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set up paths, read configuration file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stereoid_dir = Path(\"~/Code/stereoid\")\n",
    "stereoid_dir = stereoid_dir.expanduser()\n",
    "runid = \"XTI_model\"\n",
    "pardir = stereoid_dir.joinpath(\"PAR\")\n",
    "parfile = pardir.joinpath(\"Hrmny_%s.cfg\" % runid)\n",
    "conf = cfg.ConfigFile(parfile)\n",
    "form_id = conf.formation.id\n",
    "print(\"formation id: %s\" % form_id)\n",
    "savedir = stereoid_dir / \"RESULTS\" / \"xti_sea_ice\" / runid / form_id\n",
    "savedir.mkdir(parents=True, exist_ok=True)\n",
    "save_plots = True\n",
    "fontsize = 12\n",
    "colormap = \"Spectral\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initialise the formation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ftl = FormationTimeline(parfile, secondary=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualise the formation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(10, 8))\n",
    "lw = 1\n",
    "ax.plot(ftl.t, ftl.dae, \"-\", label=r\"$a\\cdot\\Delta e$\", lw=lw)\n",
    "ax.plot(\n",
    "    ftl.t, np.radians(ftl.domega) * ftl.a, \"--\", label=r\"$a\\cdot\\Delta \\Omega$\", lw=lw\n",
    ")\n",
    "ax.plot(ftl.t, np.radians(ftl.du * ftl.a), \"k--*\", label=r\"$a\\cdot\\Delta u$\", lw=lw)\n",
    "ax.plot(ftl.t, np.radians(ftl.di * ftl.a), \"r--\", label=r\"$a\\cdot\\Delta i$\", lw=lw)\n",
    "ax.set_xlabel(\"Time [days]\", fontsize=fontsize)\n",
    "ax.set_ylabel(\"Formation parameters / m\", fontsize=fontsize)\n",
    "ax.legend(loc=\"best\", fontsize=fontsize)\n",
    "ax.set_xlim((0, 365))\n",
    "ax.grid(True)\n",
    "plt.tight_layout()\n",
    "if save_plots:\n",
    "    fig.savefig(savedir.joinpath(\"Hrmny_formation_conf.png\"))\n",
    "    ftl.view_delta_v(savefile=savedir.joinpath(\"delta_v.png\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Consider baseline for a given time as a function of incidence angle\n",
    "### Compute the relative orbit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rel_orbit = ftl.baseline3d(all_orbit=True)\n",
    "print(ftl.track.incident.shape)\n",
    "print(ftl.track.LoS_satcoord.shape)\n",
    "print(rel_orbit.dr_r.shape)\n",
    "print(rel_orbit.u.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get incedence angle for specific time instants and corresponding LoS vector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Incident angles have dimensions of [t, l], where t is the time instants and l look angles\n",
    "n_time_instants = ftl.track.incident.shape[0]\n",
    "time_indices = range(0, n_time_instants, 10)\n",
    "xtis = np.empty(\n",
    "    (len(time_indices), ftl.track.LoS_satcoord.shape[1], rel_orbit.dr_r.shape[0])\n",
    ")\n",
    "atis = np.empty(\n",
    "    (len(time_indices), ftl.track.LoS_satcoord.shape[1], rel_orbit.dr_r.shape[0])\n",
    ")\n",
    "h_amb = np.empty(\n",
    "    (len(time_indices), ftl.track.LoS_satcoord.shape[1], rel_orbit.dr_r.shape[0])\n",
    ")\n",
    "inc = np.empty((len(time_indices), ftl.track.incident.shape[1]))\n",
    "separation_vector_components = (\"dr_n\", \"dr_r\", \"dr_t\")\n",
    "for i, time_ind in enumerate(time_indices):\n",
    "    LoSslv = ftl.track.LoS_satcoord[time_ind, :, :]\n",
    "    LoSmst = ftl.track_prim.LoS_satcoord[time_ind, :, :]\n",
    "    # Mean LoS for monostatic equivalent\n",
    "    LoS = (LoSslv + LoSmst) / 2\n",
    "    separation_elements = (\n",
    "        getattr(rel_orbit, vec_component)\n",
    "        for vec_component in separation_vector_components\n",
    "    )\n",
    "    dr_n_trans, dr_r_trans, dr_t_trans = (\n",
    "        element.transpose()[np.newaxis, time_ind, :] for element in separation_elements\n",
    "    )\n",
    "    atis[i, :, :] = ftl.ati_baseline(dr_r_trans, dr_n_trans, dr_t_trans, LoS)\n",
    "    # since we are providing a LoS the inc is not used in the computation of xbaseline, however the function still expects an incident angle. This should be fixed\n",
    "    xtis[i, :, :] = ftl.xbaseline(dr_r_trans, dr_n_trans, dr_t_trans, LoS=LoS)\n",
    "    inc[i, :] = np.degrees(ftl.track.incident[time_ind, :])\n",
    "    h_amb[i, :, :] = ftl.h_amb(\n",
    "        dr_r_trans, dr_n_trans, dr_t_trans, inc[i, :, np.newaxis], LoS\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "incorrectly_encoded_metadata": "{\\m}$\"",
    "lines_to_next_cell": 0,
    "title": "r\"$\\si"
   },
   "outputs": [],
   "source": [
    "mean_latitude = np.degrees(rel_orbit.u[time_indices]) - 90\n",
    "# NOTE the incident angle varies with time. This variation is small so it can be assumed to be constant to allow the plot of the 2d image\n",
    "inc_0 = inc[0, :]\n",
    "\n",
    "Formation_Data = namedtuple(\"Formation_Data\", [\"dae\", \"domega\", \"a\"])\n",
    "formation_data = Formation_Data(dae=ftl.dae, domega=ftl.domega, a=ftl.a)\n",
    "\n",
    "contour_params = {\n",
    "    \"cmap\": colormap,\n",
    "}\n",
    "axes_params = {\n",
    "    \"add_title\": True,\n",
    "    \"title\": None,\n",
    "    \"add_xlabel\": False,\n",
    "    \"xlabel\": \"Incident angle /°\",\n",
    "    \"add_ylabel\": True,\n",
    "    \"ylabel\": \"Mean argument of latitude /°\",\n",
    "    \"y_major_locator\": plt.MultipleLocator(180 / 2),\n",
    "    \"y_minor_locator\": plt.MultipleLocator(180 / 2),\n",
    "}\n",
    "colorbar_params = {\"label\": \"Distance /m\"}\n",
    "plot_params = {\n",
    "    \"contour\": contour_params,\n",
    "    \"axes\": axes_params,\n",
    "    \"colorbar\": colorbar_params,\n",
    "    \"yaxis_formatter\": None,\n",
    "}\n",
    "fig_params = {\"figsize\": (12, 10), \"sharex\": True, \"sharey\": True}\n",
    "\n",
    "fig = contour_figure(\n",
    "    formation_data,\n",
    "    (0, 36, 84, 168),\n",
    "    (inc_0, mean_latitude, np.abs(atis)),\n",
    "    fig_params,\n",
    "    plot_params,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = contour_figure(\n",
    "    formation_data,\n",
    "    (0, 36, 84, 168),\n",
    "    (inc_0, mean_latitude, np.abs(xtis)),\n",
    "    fig_params,\n",
    "    plot_params,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#axes_params[\"y_major_locator\"] = matplotlib.ticker.MaxNLocator(5)\n",
    "#axes_params[\"y_minor_locator\"] = matplotlib.ticker.MaxNLocator(10)\n",
    "axes_params[\"y_major_locator\"] = matplotlib.ticker.FixedLocator(np.arange(160, 195, 5))\n",
    "axes_params.pop(\"y_minor_locator\")\n",
    "plot_params[\"axes\"] = axes_params\n",
    "plot_params[\"yaxis_formatter\"] = None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "polar_index = np.logical_and(mean_latitude <= 190, mean_latitude>=160)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "figs = [] # add figures to be saved in a list\n",
    "descriptions = [] # add descriptions to be used as filenames in a list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_xb = contour_figure(\n",
    "    formation_data,\n",
    "    (0, 36, 84, 168),\n",
    "    (inc_0, mean_latitude[polar_index], np.abs(xtis)[polar_index, :, :]),\n",
    "    fig_params,\n",
    "    plot_params,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_xb.suptitle(\"Cross-track baseline\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if save_plots:\n",
    "    descriptor = f\"xt_baseline_plot_N\"\n",
    "    fig_xb.savefig(savedir.joinpath(descriptor + \".png\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Error in interferometric measurements\n",
    "\n",
    "The interferometric phase measured by Harmony will have contributions both from the topography of the scene and from movement in the direction of the line of sight due to the cross-track baseline and along-track baseline respectively\n",
    "$$\n",
    "\\phi = \\phi_{\\mathrm{topo}} + \\phi_{\\mathrm{ATI}} + \\phi_n.\n",
    "$$\n",
    "The phase due to the along-track baseline $\\phi_{\\mathrm{ATI}}$ can be estimated, $\\hat{\\phi}_{\\mathrm{ATI}}$, by the two-channel receiver system on-board each of the Harmony satellites. The two channels have a physical separation along the track, making them sensitive to movement in the direction parallel to the line of sight. The estimate of this phase can be subtracted from the measured phase to remove the undesired ATI component, giving an estimate of the phase due to the topography of the scene $\\hat{\\phi}_{\\mathrm{topo}}$. The estimation error, $\\epsilon_{\\mathrm{ATI}}$, will add to the total error of the measurement\n",
    "\\begin{align}\n",
    "\\hat{\\phi}_{\\mathrm{topo}} &= \\phi_{\\mathrm{topo}} + \\epsilon_{\\mathrm{ATI}} + \\phi_n,\\\\\n",
    "\\epsilon_{\\mathrm{ATI}} &= \\phi_{\\mathrm{ATI}} - \\frac{B_\\parallel}{B_{\\parallel s}} \\hat{\\phi}_{\\mathrm{ATI}}, \\\\\n",
    "\\end{align}\n",
    "where the estimate of along-track phase contribution acquired using the short baseline is scaled by the ratio of the formation along-track baseline to the along-track baseline of the oboard phase centres. The scaling factor arises from the estimate of the radial velocity of the scaterrer returning the signal, obtained from the interferometric measurement of the on-board system scaled by $\\frac{1}{B_{\\parallel s}}$. This velocity causes $\\phi_{\\mathrm{ATI}}$ in the interferometric phase measurement of the system. Thus, to convert the estimate of the radial velocity to a phase, the velocity estimate is multiplied by $B_\\parallel$. The standard deviation of the phase is defined as\n",
    "$$\n",
    "\\sigma_\\phi = \\sqrt{\\int_{- \\pi}^{+ \\pi} \\phi^2 p_\\Phi(\\phi) \\ \\mathrm{d} \\phi}\n",
    "$$\n",
    "where $p_\\Phi(\\phi)$ is the probability density function of the phase difference between the two interferometric channels. Instead of derriving the expressions for the variance, the Cramer-Rao lower bound is used\n",
    "$$\n",
    "\\sigma_\\phi = \\sqrt{\\frac{1 - \\gamma^2}{2 N_l \\gamma^2}},\n",
    "$$\n",
    "where $\\gamma$ is the coherence and $N_l$ is the number of independent looks. The coherence is the product of\n",
    "$$\n",
    "\\gamma \\approx \\gamma_{\\mathrm{SNR}} \\gamma_t \\gamma_{\\mathrm{Quant}} \\gamma_{\\mathrm{Amb}} \\gamma_{\\mathrm{Vol}},\n",
    "$$\n",
    "where the right-hand side of the equation describes the contributions to the error due to noise ($\\gamma_{\\mathrm{SNR}}$), temporal decorrelation ($\\gamma_t$), quantisation ($\\gamma_{\\mathrm{Quant}}$), ambiguities ($\\gamma_{\\mathrm{Quant}}$), and volume decorrelation ($\\gamma_{\\mathrm{Vol}}$). The errors due to quantisation, ambiguites and volume decorrelation are not considered.\n",
    "\n",
    "The coherence due to noise depends on the $\\mathrm{SNR}$ , which is a function of the $\\mathrm{NESZ}$ and the backscatter coefficient of the mapped pixel\n",
    "$$\n",
    "\\gamma_{\\mathrm{SNR}} = \\frac{1}{1 + \\mathrm{SNR}\\left(\\sigma^0, \\mathrm{NESZ} \\right)^{-1}}.\n",
    "$$\n",
    "In the following analysis a value of $\\mathrm{SNR} = 5 \\ \\mathrm{dB}$ is used. The coherence due to temporal decorrelation consists of one component due to the cross-track baseline and another one due to the time-lag in the along-track direction\n",
    "\\begin{align}\n",
    "\\gamma_t &= \\gamma_{\\mathrm{XTI}} \\gamma_{\\mathrm{ATI}},\\\\\n",
    "\\gamma_{\\mathrm{XTI}} &= 1 - \\frac{B_{\\perp}}{B_{\\perp,c}},\\\\\n",
    "\\gamma_{\\mathrm{ATI}} &= \\mathrm{e}^{-\\tau^2 / \\tau_c^2},\\\\\n",
    "\\end{align}\n",
    "where $B_{\\perp,c} = \\frac{\\lambda r}{m \\rho_y \\cos^2(\\theta - \\alpha)}$ is the crictical across-track baseline at range resolution $\\rho_y$, $\\tau = \\frac{B_\\parallel}{2v}$ is the time lag between acquisitions due to the effective along-track baseline $\\frac{B_\\parallel}{2}$ and platform velocity $v$, and $\\tau_c \\approx 3.29 \\lambda / U$ is the coherence time at wind speed $U$.\n",
    "\n",
    "The number of independent looks is the ratio of the product resolution $\\rho_{\\mathrm{L2}}$ and nominal geometric resolution $\\rho_{\\mathrm{2D}}$\n",
    "$$\n",
    "N_l = \\frac{\\rho_{\\mathrm{L2}}}{\\rho_{\\mathrm{2D}}}.\n",
    "$$\n",
    "For Sentinel-1 in IWS mode the spatial resolution is $5 \\times 20 \\ \\mathrm{m}^2$; Setting the level-2 resolution of the product Harmony will produce is dependent on the accuracy we want to achieve. For relative elevation measurements with accuracy $\\mathcal{O}(10 \\ \\mathrm{cm})$, $8 \\times 8 \\ \\mathrm{km}^2$ is sufficient, producing $N_l = 64\\times 10^3$.\n",
    "\n",
    "Once the coherence factors have been computed, the standard deviation of the phase difference can be calculated. This method is used to calculate the error in the interferometric phase measurement $\\phi_n$ and the estimation error $\\epsilon_{\\mathrm{ATI}}$. Then the standard deviations due to these two sources of error are combined to give the total standard deviation of the measurement."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Standard deviation due to noise and decorrelation in the interferometric measurement\n",
    "#### Coherence due to SNR"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "snr = 10 ** (5 / 10)\n",
    "coh_snr = snr / (snr + 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Coherence due to temporal decorrelation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wavelength = 55e-3\n",
    "res_range = 5.0\n",
    "wind_speed = 5.0  # m/s\n",
    "print(ftl.track.R.shape)\n",
    "# print(ftl.track.incident.shape)\n",
    "b_perp_critical = (\n",
    "    wavelength * ftl.track.R / (res_range * np.cos(ftl.track.incident) ** 2)\n",
    ")\n",
    "# print(xtis.shape)\n",
    "# print(b_perp_critical.shape)\n",
    "coh_xti = 1 - xtis / b_perp_critical[time_indices, :, np.newaxis]\n",
    "vel_magnitude = np.linalg.norm(ftl.track.v_ecef, axis=-1)\n",
    "time_lag_ati = np.absolute(atis) / (\n",
    "    2 * vel_magnitude[time_indices, np.newaxis, np.newaxis]\n",
    ")\n",
    "time_coherence = 3.29 * wavelength / wind_speed\n",
    "coh_ati = np.exp(-(time_lag_ati ** 2) / time_coherence ** 2)\n",
    "#coh_ati = 1\n",
    "coh_total = coh_snr * coh_xti * coh_ati\n",
    "# print(atis.shape)\n",
    "# print(coh_total.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_independent_looks = 10e3\n",
    "std_phase = np.sqrt((1 - coh_total ** 2) / (2 * n_independent_looks * coh_total ** 2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "height_error_phase = h_amb * std_phase / (2 * np.pi)\n",
    "levels = np.linspace(0.0, 2, 21)\n",
    "# levels = np.concatenate((levels, [0.05]))\n",
    "# levels = np.sort(levels)\n",
    "#ticks = np.concatenate((np.linspace(0, 3, 6), [0.1]))\n",
    "ticks = np.linspace(0, 2, 9)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coh_total_no_ati = coh_snr * coh_xti * 1\n",
    "std_phase_no_ati = np.sqrt((1 - coh_total_no_ati ** 2) / (2 * n_independent_looks * coh_total_no_ati ** 2))\n",
    "height_error_phase_no_ati = h_amb * std_phase_no_ati / (2 * np.pi)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_keys = (\"levels\", \"vmax\", \"extend\")\n",
    "new_values = (levels, 2, \"max\")\n",
    "plot_params[\"contour\"].update(zip(new_keys, new_values))\n",
    "plot_params[\"colorbar\"].update({\"ticks\": ticks})\n",
    "fig_h = contour_figure(\n",
    "    formation_data,\n",
    "    (0, 36, 84, 168),\n",
    "    (inc_0, mean_latitude[polar_index], height_error_phase[polar_index, :, :]),\n",
    "    fig_params,\n",
    "    plot_params,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_h.suptitle(\"Uncertainty due to noise\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if save_plots:\n",
    "    descriptor = (\n",
    "        f\"std_height_noise_phase_nlooks_{n_independent_looks :.0f}_snr_{snr :.0f}_windspeed_{wind_speed}_N\"\n",
    "    )\n",
    "    fig_h.savefig(savedir.joinpath(descriptor + \".png\"), transparent=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Standard deviation of estimator\n",
    "Each Harmony companion has a two-channel receiver system made of two small antennas with an along-track physical separation. The two channels are used as an across-track interferometric system with a short baseline to estimate $\\phi_{\\mathrm{ATI}}$. For the measurements of the elevation the two channels are used as a single receiver to improve the performance. Thus, in the case of $\\hat{\\phi}_{\\mathrm{ATI}}$ the SNR of each channel is $3 \\ \\mathrm{dB}$ lower than the SNR of the full system. On the other hand, there is no loss in coherence due to across-track separation because the two phase centres are physically constrained to lie in the same along-track-radial plane. Furthermore, the loss in cohere due to temporal decorrelation is small as the physical separation is $4.5$ to $10 \\ \\mathrm{m}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "snr_short_baseline = snr / 2\n",
    "coh_snr_short_baseline = snr_short_baseline / (snr_short_baseline + 1)\n",
    "\n",
    "ati_short = 10.0\n",
    "time_lag_short_baseline = ati_short / (2 * vel_magnitude[time_indices])\n",
    "time_coherence = 3.29 * wavelength / wind_speed\n",
    "coh_temporal_short_baseline = np.exp(\n",
    "    -(time_lag_short_baseline ** 2) / time_coherence ** 2\n",
    ")\n",
    "coh_short_baseline = coh_snr_short_baseline * coh_temporal_short_baseline\n",
    "std_phase_short_baseline = np.sqrt(\n",
    "    (1 - coh_short_baseline ** 2) / (2 * n_independent_looks * coh_short_baseline ** 2)\n",
    ")\n",
    "print(atis[np.where(np.absolute(atis) < 0)])\n",
    "print(atis.shape)\n",
    "print(np.absolute(atis).shape)\n",
    "print(std_phase_short_baseline[np.where(std_phase_short_baseline < 0)])\n",
    "std_phase_short_baseline_scaled = (\n",
    "    np.absolute(atis) / ati_short * std_phase_short_baseline[:, np.newaxis, np.newaxis]\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "height_error_estimate = h_amb * std_phase_short_baseline_scaled / (2 * np.pi)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_h_ati = contour_figure(\n",
    "    formation_data,\n",
    "    (0, 36, 84, 168),\n",
    "    (inc_0, mean_latitude[polar_index], height_error_estimate[polar_index, :, :]),\n",
    "    fig_params,\n",
    "    plot_params,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_h_ati.suptitle(\"Uncertainty due to ATI phase correction\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if save_plots:\n",
    "    descriptor = (\n",
    "        f\"std_height_error_estimate_nlooks_{n_independent_looks :.0f}_snr_{snr :.0f}_windspeed_{wind_speed}_N\"\n",
    "    )\n",
    "    fig_h_ati.savefig(savedir.joinpath(descriptor + \".png\"), transparent=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Total standard deviation\n",
    "The variance in the measured interferometric phase is the sum of the variance due to the SNR and temporal decorrelation of the system and the variance of the estimator $\\hat{\\phi}_{\\mathrm{ATI}}$\n",
    "$$\n",
    "\\sigma_{\\phi} = \\sqrt{ \\sigma_{\\phi_n}^2 + \\left(\\frac{B_\\parallel}{B_{\\parallel s}} \\right)^2 \\sigma_{\\hat{\\phi}_{\\mathrm{ATI}}}^2 }.\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "std_phase_total = np.sqrt(std_phase ** 2 + std_phase_short_baseline_scaled ** 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Relative Height Accuracy\n",
    "\n",
    "The relative height errors can be derrived from\n",
    "$$\n",
    "\\Delta h = h_a \\frac{\\sigma_\\phi}{2 \\pi}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "height_error = h_amb * std_phase_total / (2 * np.pi)\n",
    "print(height_error.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_h_tot = contour_figure(\n",
    "    formation_data,\n",
    "    (0, 36, 84, 168),\n",
    "    (inc_0, mean_latitude[polar_index], height_error[polar_index, :, :]),\n",
    "    fig_params,\n",
    "    plot_params,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_h_tot.suptitle(\"Total uncertainty\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if save_plots:\n",
    "    descriptor = f\"std_height_{n_independent_looks :.0f}_snr_{snr :.0f}_snr_short_{snr_short_baseline :.0f}_windspeed_{wind_speed}_N\"\n",
    "    fig_h_tot.savefig(savedir.joinpath(descriptor + \".png\"), transparent=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
